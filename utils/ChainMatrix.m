function [ A, B, C, D, Tf ] = ChainMatrix(af, lf, freq, param, meth,  Tf)
% [ A, B, C, D, Tf ] = ChainMatrix(af, lf, freq, param, meth,  Tf)
% Computes the chain matrix for the TMM method
%
% Input arguments:
% - af: area function
% - lf: length function
% - freq: frequency vector
% - param: some physical parameters
% - meth: method of computation, TMM or CMP
% - Tf: Previous matrix
% 
% Output arguments:
% - A: upper left component of the output matrix chain
% - B: upper right component of the output matrix chain
% - C: lower left component of the output matrix chain
% - D: lower right component of the output matrix chain
% - Tf: output matrix chain
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide, VT_Network, ProdMat3D, computetransferfunction
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    lw = length(freq);    
    if nargin < 5;  meth = 'tmm'; end
    if nargin < 6;  Tf = repmat(eye(2),[1 1 lw]); end
    A = zeros(1,1,lw); 
    B = zeros(1,1,lw); 
    C = zeros(1,1,lw); 
    Zo = af/(param.rho*param.c);

    if ~strcmpi(meth, 'cmp') 
        om = param.freq*2*pi;    
        S = 2*sqrt(af*pi);        
        L = param.rho./af;
        Celem = Zo/param.c;        
    end
    
    for k = length(af):-1:1 % Computation of the chain matrix
        if strcmpi(meth, 'cmp')
            if param.loss
                argh = param.sig*lf(k)/param.c;
            else
                argh = 1i*om/param.c*lf(k);
                param.gam = 1;
            end
            
            A(1,1,:) = cosh(argh);
            B(1,1,:) = -1/Zo(k)*param.gam.*sinh(argh);
            C(1,1,:) = -Zo(k)./param.gam.*sinh(argh);
        else
            R = S(k)*sqrt(param.rho*param.mu*om)/(2*sqrt(2)*af(k)^2);
            G = (param.adiabatic-1)*S(k)/(param.rho*param.c^2)*sqrt(param.heat_cond*om/(2*param.specific_heat*param.rho));
            if param.loss
                invZw = 1./(param.wr/S(k)^2+1i*om*param.wm/S(k)^2+1./(1i*om*S(k)^2/param.wc)) * param.wallyield;
                gam = sqrt((R+1i*om*L(k)).*(G+1i*om*Celem(k)+invZw));
            else
                gam = 1i*om/param.c;
            end
            A(1,1,:) = cosh(gam*lf(k));
            B(1,1,:) = -sinh(gam*lf(k))/Zo(k);
            C(1,1,:) = -Zo(k)*sinh(gam*lf(k));            
        end
        Tn = [ A B; C A ];
        Tf = ProdMat3D( Tf, Tn );
    end % for k = 1:length(af)
    
    A = squeeze(Tf(1,1,:)); A = A(:);
    B = squeeze(Tf(1,2,:)); B = B(:);
    C = squeeze(Tf(2,1,:)); C = C(:);
    D = squeeze(Tf(2,2,:)); D = D(:);

end