function [af, lf, obj_out] = importjson(jsonFile, seq)
% [af, lf] = importjson(jsonFile, seq)
% import area function from JSON files
%
% Input arguments:
% - jsonFile: name of the JSON file
% - seq: number of file sequences to import
% 
% Output arguments:
% - af: area function
% - lf: length function
% - obj_out: VT_Waveguide object
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; seq = []; end

if isempty(seq)
    idtmp = jsondecode(fileread(jsonFile));
    af = arrayfun(@(x)x.area*1e-4,idtmp.tubes);
    lf = arrayfun(@(x)x.x*1e-2,idtmp.tubes);
    vArea = arrayfun(@(x)x.velumArea*1e-4,idtmp.tubes);
    vpo = (idtmp.VelumOpeningInCm*1e-2)^2;
else
    nFrame = length(seq);
    for kF = 1:nFrame
        fTmp = ([jsonFile, '_', num2str(seq(kF), '%.4d'), '.json']);
        idtmp = jsondecode(fileread(fTmp));
        afTmp = arrayfun(@(x)x.area*1e-4,idtmp.tubes);
        lfTmp = arrayfun(@(x)x.x*1e-2,idtmp.tubes);   
        vpoTmp = (idtmp.VelumOpeningInCm*1e-2)^2;
        vAreaTmp = arrayfun(@(x)x.velumArea*1e-4,idtmp.tubes);
        if kF == 1
            nTubes = length(afTmp);
            af = zeros(nTubes, nFrame);
            lf = zeros(nTubes, nFrame);
            vpo = zeros(nFrame, 1);
            vAreaTmp = zeros(nTubes, nFrame);
        end
        af(:,kF) = afTmp;
        lf(:,kF) = lfTmp;
        vpo(kF) = vpoTmp;
        vArea(:,kF) = vAreaTmp;
    end
end

if (sum(vpo) == 0) && (sum(vArea(:)) == 0)
    obj_out = VT_Waveguide('area_function', af, ...
        'length_function', lf);
else
   obj_out = MainOralTract('area_function', af, ...
        'length_function', lf, ...
        'velopharyngeal_port', vpo, ...
        'velum_area', vArea); 
end

end

