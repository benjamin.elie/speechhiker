function fk = relocatePartials( pks, x, fs, f0, param )
% warning off
%pks = abs(pks);
nfft = 2^nextpow2(length(x)*10);
fkm=zeros(1,param.nh);    
w = hann(length(x)+2);
w = w(2:end-1);
f = fs*(0:nfft-1)/nfft;
idxf0=find(f>=f0,1,'first');
fkm(1)=f0;
idxfi=idxf0;

yg = (fft(x(:).*w(:), nfft));
yg = yg(1:ceil(nfft/2)+1);
f = f(1:ceil(nfft/2)+1);
pks = pks(1:ceil(nfft/2)+1);
nzrow = 0;
prng = ceil(param.searchRange*fs/nfft);

for ki=2:param.nh
    idxfi=idxf0+idxfi;
%     idxfi=ki*idxf0;%+idxfi;
    if idxfi<=length(pks)
        [valeur, idxf] = max(abs(pks(max(idxfi-prng,1):min(idxfi+prng,length(pks)))));
        if valeur ~= 0
            idxfi=idxfi+idxf-prng-1; idxtt = idxfi;
            [~, idxf] = max(abs(yg(max(idxfi-prng,1):min(idxfi+prng,length(yg)))));
            idxfi=idxfi+idxf-prng-1;
            ir = max(idxfi-2,1):min(idxfi+2,length(yg));
            if max(ir)<=length(f)
                wos = f(ir); wos = wos(:);
                los = log(abs(yg(ir))); los = los(:);
                p = poly2(wos, los);
                fkm(ki) = -p(2)/2/p(1);
                if fkm(ki) < 0 || fkm(ki) > fs/2 || abs(fkm(ki)-f(idxfi)) > prng ; fkm(ki) = f(idxtt); idxfi = idxtt; end
            else
                fkm(ki) = ki*f0;
            end
        end
    else
        break        
    end
    if isnan(fkm(ki)); fkm(ki) = 0; end
    if fkm(ki)
        nzrow = 0;
    else
        nzrow = nzrow+1;
        if param.nzero && nzrow >= param.nzero
            fk = fkm;
            return;
        end
    end  
end
fk = fkm;
end

function p = poly2(x,y)
z = x(:);
N = 3;
n=(N-1:-1:0);
V = exp(log(z)*n);
p = V\y(:);
end
