function fk = f0reeval(f0, x, sr, param)
% fk = f0reeval(f0, x, sr, param)
% correction of f0/2 and f4/2 using the derivative criterion. Used for
% X-GLOS
%
% Input arguments:
% - f0: groos estimate of the fundamental frequency
% - x: signal
% - sr : sampling frequency
% - param: some parameters
% 
% Output argument:
% - fk: output fundamental frequency
%
% Benjamin Elie and Gilles Chardon, 2020
%
% See also SpeechAudio, xglos
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    for ki = 1:2
        if size(f0,1) > 1; f0 = reshape(f0,[1,length(f0)]); end
        derrive=[0  diff(f0)];
        derrive(f0==0)=0;
        f0_=[0 f0(1:end-1)];
        derrive(f0_ == 0)=0;
        detection = derrive./(f0+1e-5)*100;
%         detection(f0 > param.fmax/2) = 0;
        f0(detection <-50) = 0;% f0(detection <-50) * 2;   
        f0 = medfilt1(f0,3);   
        f0(f0<param.fomin | f0>param.fomax) = 0;
    end

%     f0 = medfilt1(f0,3);    
    fk = zeros(length(f0), param.nh);
 
    kit = param.hop+ceil(param.lw/2);
    param2 = param;    
    param2.thrs = -1;

    for cmpt = 1:length(f0)
        if f0(cmpt) >= param.fomin
            tow = floor(1/f0(cmpt)*sr);
            np = ceil(param.lw/tow);
            if abs((np-1)*tow-param.lw) < abs(np*tow-param.lw); np = np-1; end
            nw = np*tow;
            iw = max(1,kit-floor(nw/2)):min(kit+floor(nw/2)-1+mod(nw,2),length(x));
            xw = x(iw);
            w = hann(length(xw)+2); w = w(2:end-1);
%             param2.lfmed = ceil(f0(cmpt)*1.5);            
            pks = findpartials(xw, sr, w, param2);
            fk(cmpt,:) = relocatePartials( pks, xw, sr, f0(cmpt), param2);    
        end            
        kit = kit+param.hop; 
    end    
end