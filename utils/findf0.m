function [ f0, cumf0 ] = findf0(pks, sr, param)
% [ f0, cumf0 ] = findf0(pks, sr, param)
% Estimation of the fundamental frequency of the windowed signal S. Used
% for X-GLOS
%
% Input arguments:
%
% pks: cleaned periodogram
% sr: sampling frequency (in Hz)
% param : structure containing various parameters
% 
% Output arguments:
% - f0: gross estimate of the fundamental frequency
% - cumulative periodogram
%
% Benjamin Elie and Gilles Chardon, 2020
%
% See also SpeechAudio, xglos
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

f = (0:param.nfft-1) / param.nfft * sr;
idx1 = find(f >= param.fomin, 1, 'first');
idx2 = find(f <= param.fomax, 1, 'last');

% cumulative periodogram
cum_per = zeros(length(pks), 1);

Npartials = param.np;  

for ki = idx1:idx2%idx1:idx2
%     if pks(ki)
        for kj = 1:min(floor(length(pks)/ki), Npartials)
            cum_per(ki) = cum_per(ki) + pks((ki-1) * kj + 1);
%             cum_per(ki) = cum_per(ki) + pks(ki * kj);
        end
%     end
end

% keep only if fundamental is present
cum_per(pks == 0) = 0;
% first estimation of f0
[maxcumul, idxf0] = max(cum_per);
% if idxf0 < idx1
%     f0 = 0;    
%     cumf0 = 0;
%     return
% end

% find values of cum_per close to the maximum
idxfindf0 = find(cum_per > 0.95 * maxcumul);

% !?
idxf0 = max(idxfindf0(mod(idxfindf0,idxf0)==0));
% solve the problem when f0/2 occurs in the cumulated periodog

if idxf0 > ceil(param.searchRange*sr/param.nfft)
    [maxpks, idxf] = max(pks(idxf0-ceil(param.searchRange*sr/param.nfft):idxf0+ceil(param.searchRange*sr/param.nfft)));  
else
    [maxpks, idxf] = max(pks(1:idxf0+ceil(param.searchRange*sr/param.nfft)));
end

if ((isempty(maxpks)) || (maxpks/sum(pks) < 1e-3))% || (abs(param.searchRange+1-idxf) < param.partialLength))
    f0 = 0;
    cumf0 = 0;
    return
end

% If empty, no voicing, the signal contains only noise

idxf0=idxf0+idxf-ceil(param.searchRange*sr/param.nfft)-1;

if (isempty(idxf0))
    f0 = 0;    
    cumf0 = 0;
    return
end

% idxh1 = 2*idxf0;
% [maxpks, idxf] = max(pks(idxh1-param.searchRange:idxh1+param.searchRange));
% if ~maxpks
%     f0 = 0;
%     return
% end    
% %%% Get the detected harmonics
f0 = f(idxf0);
cumf0 = cum_per(idxf0);

end
