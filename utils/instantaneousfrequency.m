function [ foi, ai ] = instantaneousfrequency(x, sr, fo, meth)
% [ foi, ai ] = instantaneousfrequency(x, sr, fo, meth)
% returns the instantaneous frequency and amplitude
%
% Input arguments:
% - x: signal
% - sr : sampling frequency
% - fo: gross estimate of the fundamental frequency
% - meth: choice of the method (zcr for zero-crossing-rate, or hilbert for
% hilbert transform method)
% 
% Output arguments:
% - foi: instantaneouns fundamental frequency
% - ai: instantaneous amplitude
%
% Benjamin Elie and Gilles Chardon, 2020
%
% See also SpeechAudio, xglos
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
[ foi, ai ] = instprop( x, sr, 160, 100 );

if strcmp(meth,'zcr')
    tow = floor(1/fo*sr);
    x = x-mean(x);
    nx = length(x);
    txi = 1:0.01:nx;
    xi = interp1(1:nx, x, txi);
    px = xi(1:end-1).*xi(2:end);
    zc = find(px <= 0);
    zct = txi(zc(1:2:end));
    foi = zeros(size(x));
    % ai = foi;
    
%     [ hmax, ~ ] = findpeaks(xi);
%     [ ~, imin ] = findpeaks(-xi);
%     hmin = xi(imin);
%     lmax = length(hmax); lmin = length(hmin);
%     lm = min(lmax,lmin);
%     hmax = hmax(1:lm);
%     hmin = hmin(1:lm);
%     
%     hmax = hmax(:);
%     hmin = hmin(:);
    
    % damp = hmax-hmin;
    fo = sr./diff(zct);
    perloc = 1/2*(zct(1:end-1)+zct(2:end));
    
    if length(perloc) >= 2
        if perloc(1) > 2*tow; fo = [ 0, fo ]; perloc = [ 1 perloc ]; end
        if nx-perloc(end) > 2*tow; fo = [ fo, 0 ]; perloc = [ perloc nx ]; end
        foi = interp1(perloc, fo, txi,'pchip');
        foi = foi(1:100:end);
        foi = foi(:);
    end
end
end

function [ ifq, upenv, lowenv ] = instprop( x, sr, ordfir, tw )
% find instantaneous frequency and amplitude using Hilbert transform
if nargin < 2; sr = 1; end
if nargin < 3; ordfir = 160; end
if nargin < 4; tw = 100; end

upenv = [];
lowenv = [];
%%%% Remove the mean
xm = mean(x);

%%%% design hilbert filter
d = designfilt('hilbertfir','FilterOrder',ordfir,'TransitionWidth',tw,'SampleRate',sr); 
grd = ordfir/2;

xmm = x-xm;
xmm = [ zeros(grd,1); xmm(:); zeros(grd,1) ];
hb = filter(d,xmm);
   
x_anal = xmm(1:end-grd) + 1j*hb(grd+1:end)+xm;
x_anal = x_anal(grd:length(x)+grd-1);
phix = unwrap(angle(x_anal));
ifq = 1/2/pi*gradient(phix,1/sr);
if nargout >  1; upenv = abs(x_anal); end

if nargout > 2
    x = -x;
    xm = mean(x); 
    xmm = x-xm;
    xmm = [ zeros(grd,1); xmm(:); zeros(grd,1) ];
    hb = filter(d,xmm);    
    x_anal = xmm(1:end-grd) + 1j*hb(grd+1:end)+xm;
    x_anal = x_anal(grd:length(x)+grd-1);
    lowenv = -abs(x_anal);
end

end

