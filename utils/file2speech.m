function SpeechObject = file2speech(fileName, chn, sro)
% SpeechObject = file2speech(fileName)
% create SpeechAudio object from audio file
%
% Input arguments:
% - fileName: name of the audio file
% - chn: channel where to extract the audio signal
% - sro: desired sampling frequency
% 
% Output argument:
% - SpeechObject: created SpeechAudio object
%
% Benjamin Elie, 2020
%
% See also SpeechAudio
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; chn = 1; end
if nargin < 3; sro = -10; end

if exist(fileName, 'file')
    [ y, sr ] = audioread(fileName);
    if min(size(y)) == 2
        [ ~, idx ] = max(size(y));
        if idx == 1
            y = y(:, chn);
        else
            y = y(chn, :);
        end
    end
    SpeechObject = SpeechAudio('signal', y, 'sampling_frequency', sr);
    if sro > 0 && sr ~= sro
        SpeechObject.speechresample(sro);
    end
else
    error("File does not exist");
end

end

