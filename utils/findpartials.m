function pks = findpartials(x, sr, win, param) 
% pks = findpartials(x, sr, win, param) 
% returns a cleaned version of the periodogram
% keeps interval of the periodogram above peaks that are above the noise
% floor (estimated by a median filtering of the periodogram). Used fo
% X-GLOS
%
% Input arguments:
% - x: signal
% - sr : sampling frequency
% - win: window
% - param: some parameters
% 
% Output argument:
% - pks: detected peaks in the spectrogram
%
% Benjamin Elie and Gilles Chardon, 2020
%
% See also SpeechAudio, xglos
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

F0min = param.fomin;
Nfft = 2^nextpow2(length(x)*10); 

f = sr * (0:Nfft-1) / Nfft;
P = abs(fft(x .* win, Nfft)) .^ 2;
P = P/sum(P);
% Power above the noise floor
Pt = P; 
nw = param.lfmed * ceil(Nfft/param.nfft);
if ~mod(nw,2); nw = nw+1; end
if param.thrs > 0
    pth = running_percentile(P, nw, param.pct);
    Pt(Pt<pth) = 0;
end

Pt(f < F0min) = 0;
pks = zeros(length(Pt), 1);
m = (Pt(2:end-1) > Pt(3:end)) & (Pt(2:end-1) > Pt(1:end-2)); locs = find(m) + 1; % Find peaks
prng = ceil(param.partialLength*sr/Nfft);

% keep the periodogram around the peaks above the noise floor
for ki = 1:length(locs)
    if (locs(ki) > prng) && (locs(ki)+prng < length(pks))
        partial_range = locs(ki)-prng : locs(ki)+prng;
        pks(partial_range) = Pt(partial_range);
    end
end

end

