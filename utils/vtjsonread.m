function VT = vtjsonread(jsonfile, pathaf, prm)
% VT = vtjsonread(jsonfile, pathaf, prm)
% Read JSON file for VT_Network configuration
%
% Input arguments:
% - jsonfile: name of the JSON file
% - pathaf: path to find the area function files
% - prm: some parameters
% 
% Output argument:
% - VT: output VT_Network object
%
% Benjamin Elie, 2020
%
% See also VT_Network, VT_Waveguide, MainOralTract, Glottis
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 3; prm = []; end
if ~isfield(prm,'fsim'); prm.fsim = 6e4; end
if ~isfield(prm,'amin'); prm.amin = 1e-11; end
if ~isfield(prm,'agmax'); prm.agmax = 0.4e-4; end
if ~isfield(prm,'psubmax'); prm.psubmax = 700; end

fsim = prm.fsim;
amin = prm.amin;

VT = VT_Network;
idjson = jsondecode(fileread(jsonfile));
gs = idjson.globalScenario;
nTubes = gs.numberOfTubes;

%%%%% Area function
af = gs.keyAreaFunctions;
afID = arrayfun(@(x)x.mainAreaFunctionFile,af,'uniformOutput',false);
naf = length(afID);
afTp = arrayfun(@(x)x.synthesisTime,af)*1e-3;
afTrans = arrayfun(@(x)x.transitionFromPreviousAreaFunction,af,'uniformOutput',false);
tEnd = max(afTp);
tVec = 0:1/fsim:tEnd;
nPt = length(tVec);

af0 = zeros(nTubes,naf);
lf0 = af0;
varea0 = af0; 
vpo0 = zeros(naf,1);

afOut = zeros(nTubes, nPt);
lfOut = zeros(nTubes, nPt);
vpoOut = zeros(nPt,1);
vareaOut = zeros(nTubes, nPt);

for kaf = 1:naf      
    tmpFile = afID{kaf};
    idx = strfind(tmpFile,'.');
    tmpList = dir([fullfile(pathaf,tmpFile(1:idx-1)),'*']);
    ftmp = fullfile(pathaf,tmpList(1).name);
    idtmp = jsondecode(fileread(ftmp));
    vpo0(kaf) = (idtmp.VelumOpeningInCm/1e2).^2; 
    af0(:,kaf) = arrayfun(@(x)x.area*1e-4,idtmp.tubes);
    lf0(:,kaf) = arrayfun(@(x)x.x*1e-2,idtmp.tubes);
    varea0(:,kaf) = arrayfun(@(x)x.velumArea*1e-4,idtmp.tubes);
    
    if (kaf > 1) && (kaf < naf)
        ind = find(tVec>= afTp(kaf-1) & tVec <= afTp(kaf));
        switch afTrans{kaf+1}
            case 'COS'
                afOut(:,ind) = Cosine_interp( [ afTp(kaf-1), afTp(kaf) ], [af0(:,kaf-1), af0(:,kaf) ], tVec(ind) );
                lfOut(:,ind) = Cosine_interp( [ afTp(kaf-1), afTp(kaf) ], [lf0(:,kaf-1), lf0(:,kaf) ], tVec(ind) );
                vareaOut(:,ind) = Cosine_interp( [ afTp(kaf-1), afTp(kaf) ], [varea0(:,kaf-1), varea0(:,kaf) ], tVec(ind) );
                vpoOut(ind) = Cosine_interp( [ afTp(kaf-1), afTp(kaf) ], [vpo0(kaf-1), vpo0(kaf) ], tVec(ind) );
            case 'LIN'
                afOut(:,ind) = interp1( [ afTp(kaf-1), afTp(kaf) ], [ af0(:,kaf-1), af0(:,kaf) ].', tVec(ind) ).';
                lfOut(:,ind) = interp1( [ afTp(kaf-1), afTp(kaf) ], [ lf0(:,kaf-1), lf0(:,kaf) ].', tVec(ind) ).';
                vareaOut(:,ind) = interp1( [ afTp(kaf-1), afTp(kaf) ], [ varea0(:,kaf-1), varea0(:,kaf) ].', tVec(ind) ).';
                vpoOut(ind) = interp1( [ afTp(kaf-1), afTp(kaf) ], [ vpo0(kaf-1), varea0(kaf) ], tVec(ind) );
        end    
    elseif(kaf == naf)
        ind = find(tVec>= afTp(kaf-1) & tVec <= afTp(kaf));
        switch afTrans{kaf}
            case 'COS'
                afOut(:,ind) = Cosine_interp( [ afTp(kaf-1), afTp(kaf) ], [af0(:,kaf-1), af0(:,kaf) ], tVec(ind) );
                lfOut(:,ind) = Cosine_interp( [ afTp(kaf-1), afTp(kaf) ], [lf0(:,kaf-1), lf0(:,kaf) ], tVec(ind) );
                vareaOut(:,ind) = Cosine_interp( [ afTp(kaf-1), afTp(kaf) ], [varea0(:,kaf-1), varea0(:,kaf) ], tVec(ind) );
                vpoOut(ind) = Cosine_interp( [ afTp(kaf-1), afTp(kaf) ], [vpo0(kaf-1), vpo0(kaf) ], tVec(ind) );
            case 'LIN'
                afOut(:,ind) = interp1( [ afTp(kaf-1), afTp(kaf) ], [ af0(:,kaf-1), af0(:,kaf) ].', tVec(ind) ).';
                lfOut(:,ind) = interp1( [ afTp(kaf-1), afTp(kaf) ], [ lf0(:,kaf-1), lf0(:,kaf) ].', tVec(ind) ).';
                vareaOut(:,ind) = interp1( [ afTp(kaf-1), afTp(kaf) ], [ varea0(:,kaf-1), varea0(:,kaf) ].', tVec(ind) ).';
                vpoOut(ind) = interp1( [ afTp(kaf-1), afTp(kaf) ], [ vpo0(kaf-1), varea0(kaf) ], tVec(ind) );
        end  
    end
end

afOut = afOut-vareaOut;
%%%%% Check null values
afOut(afOut < amin) = amin;
lfOut(lfOut < amin) = amin;
vareaOut(vareaOut < amin) = amin;
vpoOut(vpoOut < amin) = amin;


MOT = MainOralTract('area_function',afOut,'length_function',lfOut);
VT.waveguide = MOT;
%%%%%% Check for nasal coupling and build nasal tract if necessary
if sum(vpo0) > 0
    kn = zeros(nPt,1);
    for k = 1:nPt
        idx = find(vareaOut(:,k),1,'first');
        if ~isempty(idx)
            kn(k) = idx;
        end
    end
    kn = ceil(median(kn));
end

%%%% F0
F0 = gs.F0Commands;

F0Hz = arrayfun(@(x)x.FOInHz,F0);
F0tp = arrayfun(@(x)x.timeInMs*1e-3,F0);
idx0 = find(F0Hz>=0);
F0Hz = F0Hz(idx0);
F0tp = F0tp(idx0);

F0Out = interp1(F0tp,F0Hz,tVec,'linear',0);
 
%%%%% Ag
ag = gs.keyAG0;
ag0 = arrayfun(@(x)x.Ag0Opening,ag);
agPhon = arrayfun(@(x)x.phoneticLabel,ag,'uniformOutput',false);
agTp = arrayfun(@(x)x.synthesisTime*1e-3,ag);
agTrans = arrayfun(@(x)x.transitionFromPreviousAreaFunction,ag,'uniformOutput',false);
nag = length(ag0);

agOut = zeros(nPt,1);

for kag = 1:nag
    if (kag > 1) %&& (kag < nag)
        ind = find(tVec>= agTp(kag-1) & tVec <= agTp(kag));
        switch agTrans{kag}
            case 'COS'
                agOut(ind) = Cosine_interp( [ agTp(kag-1), agTp(kag) ], [ag0(kag-1), ag0(kag) ], tVec(ind) );
            case 'LIN'
                agOut(ind) = interp1( [ agTp(kag-1), agTp(kag) ], [ ag0(kag-1), af0(kag) ], tVec(ind) );
        end    
    end
end    

Gl = Glottis;
Gl.fundamental_frequency = F0Out;
lch = sqrt(agOut);%.^2; % adimensional length of the chink opening
lch(lch <= amin) = amin; % if length = 0 => set to the minimal value
lch(lch >= 1) = 1-amin; %  if length = 1 => set to the maximal value (1-vmin)
Gl.partial_abduction = lch;

%%%%% Check existence of glottal opening and build glottal chink if necessary
if sum(agOut) > 0
    agOut = agOut*prm.agmax;
    agOut(agOut<amin) = amin;
    Chink = GlottalChink('parent_wvg',MOT,'area_function', reshape(agOut(:),size(tVec)),'length_function',Gl.x_position(end)*ones(size(tVec)));
    VT.waveguide = {VT.waveguide, Chink};
end

VT.oscillators = {Gl};

%%%% Segmentation
seg = gs.segmentation;
segStart = arrayfun(@(x)x.beginsAtInMs*1e-3,seg);
segEnd = arrayfun(@(x)x.endsAtInMs*1e-3,seg);
segPhon = arrayfun(@(x)x.sampaCode,seg,'uniformOutput',false);

%%%% Build subglottal pressure
psubOut = zeros(nPt,1);
ind = find(tVec > segStart(1) & tVec <= segStart(2));
psubOut(ind) = Cosine_interp( [ segStart(1), segStart(2) ], [ 0, prm.psubmax ], tVec(ind) );

if ~strcmp(segPhon{end},'#')    
    ind = find( tVec > segStart(end) );
    psubOut(ind) = Cosine_interp( [ segStart(end), segEnd(end) ], [ prm.psubmax, 0 ], tVec(ind) );
    psubOut(tVec > segStart(2) & tVec <= segStart(end)) = prm.psubmax;
else
    fallTime = segStart(end)-0.05;
    ind = find( tVec > fallTime );
    psubOut(ind) = Cosine_interp( [ fallTime, segEnd(end) ], [ prm.psubmax, 0 ], tVec(ind) );
    psubOut(tVec > segStart(2) & tVec <= fallTime) = prm.psubmax;
end

VT.subglottal_control = psubOut;
VT.phonetic_label = segPhon;
VT.phonetic_instant = [ segStart(:), segEnd(:) ];
VT.simulation_frequency = fsim;

end

function yo = Cosine_interp( xi, yi, xo )
% Cosine interpolation at points xo
xa = xi(1); 
xb = xi(end); 
[ nTubes, nPt ] = size(yi);
if min(nTubes, nPt) == 1
    ya = yi(1);
    yb = yi(:,end);
else
    ya = yi(:,1); 
    yb = yi(:,end);
end
dy = yb-ya;
Xo = (xo-xa)./(xb-xa)*pi+pi;
yo = ya+1/2*(1+cos(Xo)).*dy;

end

