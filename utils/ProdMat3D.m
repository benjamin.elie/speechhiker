function C = ProdMat3D( A, B )
% C = ProdMat3D( A, B )
% Compute the product AxB of 3D matrices along a single dimension

% Input arguments:
% - A: left matrix
% - B: right matrix
%
% Output argument:
% - C: product matrix
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide, VT_Network, ChainMatrix, computetransferfunction
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    [ l1, l2, l3 ] = size(A);
    C = zeros(size(A));
    if l1 == 2 && l2 == 2    
        C11 = zeros(1,1,l3); 
        C12 = zeros(1,1,l3); 
        C21 = zeros(1,1,l3); 
        C22 = zeros(1,1,l3); 
        
        a11 = A(1,1,:); 
        a12 = A(1,2,:); 
        a21 = A(2,1,:); 
        a22 = A(2,2,:);
        b11 = B(1,1,:); 
        b12 = B(1,2,:); 
        b21 = B(2,1,:); 
        b22 = B(2,2,:);
        
        C11(1,1,:) = a11(:).*b11(:) + a12(:).*b21(:); 
        C12(1,1,:) = a11(:).*b12(:) + a12(:).*b22(:); 
        C21(1,1,:) = a21(:).*b11(:) + a22(:).*b21(:);        
        C22(1,1,:) = a21(:).*b12(:) + a22(:).*b22(:);

        C = vertcat(horzcat(C11,C12),horzcat(C21,C22));
    else
        for k = 1:l3        
            C(:,:,k) = A(:,:,k)*B(:,:,k);        
        end    
    end
end