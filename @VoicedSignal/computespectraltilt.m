function tilt = computespectraltilt(obj)
% tilt = computespectraltilt(obj)
% compute spectral tilt of the VoicedSignal object
% 
% Input argument:
% - obj: VoicedSignal object
%
% Output argument:
% - tilt: spectral tilt
% 
% Benjamin Elie, 2020
%
% See also VoicedSignal, SpeechAudio, xglos
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

bk = obj.amplitudes;
harm = obj.harmonics;
tilt = nan(size(bk, 1), 1);

for k = 1:size(bk, 1)
    bktmp = abs(bk(k,:));
    fktmp = harm(k,:);
    bktmp(isnan(fktmp)) = [];
    fktmp(isnan(fktmp)) = [];

    if length(fktmp) >= 3
        p_obs = polyfit(fktmp(:), log(abs(bktmp(:))), 1);
        tilt(k) = p_obs(1);
    end
end

obj.spectral_tilt = tilt;

end

