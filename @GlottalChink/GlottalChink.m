classdef GlottalChink < VT_Waveguide
% GlottalChink: class inherited from VT_Waveguide. Describes the glottal opening
% 
% Specific properties of GlottalChink objects are:
% - chink_length: length of the glottal opening
% - glottal_flow_vector: vector containing the values of the glottal flow
% passing through the chink at each time instant
% - glottal_flow_inst: instantaneous glottal flow through the chink.
%
% The parent_point property is 1 (it is always connected at the oral tract
% input), and the radiation is false (does not radiate).
%
% GlottalChink objects have no specific methods at the moment
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide, VT_Network, MainOralTract, SubGlottalTract
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    
    properties         
        chink_length = [];%                
    end
    
    properties (Hidden = true)
        glottal_flow_vector = [];
        glottal_flow_inst = [];
    end
    
    methods
         % Constructor method
        function obj = GlottalChink(varargin)            
            for k = 1:2:length(varargin)
                field = varargin{k};
                argvalue = varargin{k+1};
                if isprop(obj,field)
                    obj.(field) = argvalue;
                end % if isprop
            end % for k ...
            obj.parent_point = 1;
            obj.radiation = false;
        end % end function
    end
end

