classdef Phoneme < SpeechAudio
% Phoneme: class used for operations on phonemes of an oral corpus
% 
% Properties of Phoneme objects are:
% - phoneme_label: name of the phoneme
% - phonetic_class: phonetic class in which belongs the phoneme
% - voiced: true if voiced phoneme
% - previous_phoneme: preceeding Phoneme object in the oral corpus
% - following_phoneme: following Phoneme object in the oral corpus
% - context: linguistic context
% - mean_formants
% - mean_fundamental_frequency
% - mean_hnr: mean harmonic-to-noise ratio, in dB
% - mean_vq: mean voicing quotient, in %
% - dispersion_formants
% - dispersion_fundamental_frequency 
% - dispersion_hnr
% - dispersion_vq
% - jitter
% - shimmer
% - devoing_index: difference in % between the VQ of the phoneme and that
% of the adjacent phonemes
%
% Methods of Phoneme objects are
% - getStats: compute the mean values and dispersion of formants, f0, VQ and HNR, using
% the specified method (mean, median, central)
% - devoicingIndex: compute the devoicing
% - getPhonemeInformation: get information about the phonetic class and the
% voicing from the phoneme label. Modifications allowing the user to add
% its own dictionary is expected for the next future...
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, Speaker
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    properties
        phoneme_label = [];
        phonetic_class = [];
        voiced = [];
        previous_phoneme = [];
        following_phoneme = [];
        context = [];
        mean_formants = [];
        mean_fundamental_frequency = [];  
    end
    
    properties (Hidden=true)
        mean_hnr = [];
        mean_vq = [];
        dispersion_formants = [];
        dispersion_fundamental_frequency = [];
        dispersion_hnr = [];
        dispersion_vq = [];
        jitter = [];
        shimmer = [];  
        devoing_index = [];
    end
    
    methods
        % Constructor method
        function obj = Phoneme(varargin)
            for k = 1:2:length(varargin)
                field = varargin{k};
                argvalue = varargin{k+1};
                if isprop(obj,field)
                    obj.(field) = argvalue;
                end % if isprop
            end % for k ...
        end % end function
        
        getStats(obj, meth);
        devoicingIndex = getDevoicing(obj);
        getPhonemeInformation(obj)
    end
    
end

