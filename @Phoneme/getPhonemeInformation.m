function getPhonemeInformation(obj)
% getPhonemeInformation(obj)
% get the phonetic information oabout the Phoneme object. The possibility
% of loading its own dictionary is planned in the next future
% 
% Input arguments:
% - obj: Phoneme object
%
% 
% Benjamin Elie, 2020
%
% See also Phoneme
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    ph = obj.phoneme_label;
    
    oral_vowel_dict = {'a','e','i','o','u','y','A','E','O','I','U', 'Y', '}', ...
        '2', '@','6', '7', '8', '9', 'V', '&', '3', 'Q', '{', ...
        'a/','e/','i/','o/','u/','y/','A/','E/','O/','I/','U/', 'Y/', '}/', ...
        '2/', '@/','6/', '7/', '8/', '9/', 'V/', '&/', '3/', 'Q/', '{/'};
    nasal_vowel_dict = {'a~','e~','i~','o~','u~','y~','A~','E~','O~', ...
        'I~','U~', 'Y~', '}~', '2~', '@~', '8~', '7~', 'V~', '&~', ...
        '6~', '3~', '3\~', 'Q~', '{~', '@\~'};
    voiced_fricative = {'z', 'Z','v'};
    voiceless_fricative = {'s', 'S','f', 'C', 'X', 'x'};
    voiced_plosive = {'b', 'd','g', '_b', 'b_', '_d', 'd_', 'g_', '_g'};
    voiceless_plosive = {'p', '_p', 'p_', 't', 't_', '_t', 'k', 'k_', '_k'};
    liquids = {'l', 'r', 'L', 'R'};
    nasal_consonants = {'m', 'n' };
    
    switch ph
        case oral_vowel_dict
            obj.phonetic_class = 'oral vowel';
            obj.voiced = true;
        case nasal_vowel_dict
            obj.phonetic_class = 'nasal vowel';
            obj.voiced = true;           
        case voiced_fricative
            obj.phonetic_class = 'fricative';
            obj.voiced = true;
        case voiceless_fricative
            obj.phonetic_class = 'fricative';
            obj.voiced = false;
        case voiced_plosive
            obj.phonetic_class = 'plosive';
            obj.voiced = true;
        case voiceless_plosive
            obj.phonetic_class = 'plosive';
            obj.voiced = false;
        case liquids
            obj.phonetic_class = 'liquid';
            obj.voiced = true;
        case nasal_consonants
            obj.phonetic_class = 'nasal consonant';
            obj.voiced = true;            
    end
end