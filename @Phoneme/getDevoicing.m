function devoicingIndex = getDevoicing(obj)
% devoicingIndex = getDevoicing(obj)
% Compute the devoicing index of the phoneme object
% 
% Input arguments:
% - obj: Phoneme object
% 
% Benjamin Elie, 2020
%
% See also Phoneme
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

phm1 = obj.previous_phoneme;
if isempty(phm1)
    phm1 = Phoneme('voicing_quotient',0);
end
if phm1.label == '#'
    phm1.voicing_quotient = 0;
end

php1 = obj.following_phoneme;
if isempty(php1)
    php1 = Phoneme('voicing_quotient',0);
end
if php1.label == '#'
    php1.voicing_quotient = 0;
end

if (isempty(obj.voicing_quotient) || ...
        isempty(phm1.voicing_quotient) || ...
        isempty(php1.voicing_quotient))
    error('Phonemes has no voicing_quotient, please compute the voicing quotient');
end

maxm1 = max(phm1.voicing_quotient);
maxp1 = max(php1.voicing_quotient);
minvq = min(obj.voicing_quotient);
maxadj = max(maxm1, maxp1);
devoicingIndex = maxadj-minvq;
obj.devoicing_index = devoicingIndex;

end

