function getStats(obj, meth)
% getStats(obj, meth)
% compute the first order and second order stat of the phoneme object obj.
% 
% Input arguments:
% - obj: Phoneme object
% - meth: choice of the method (mean, median, or central)
% 
% Benjamin Elie, 2020
%
% See also Phoneme
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    if nargin < 2; meth = 'central'; end

    switch meth
        case 'mean'
            obj.mean_formants = mean(obj.formants,2);
            obj.dispersion_formants = std(obj.formants, [], 2);
            obj.mean_fundamental_frequency = mean(obj.gross_fundamental_frequency);
            obj.dispersion_fundamental_frequency = std(obj.gross_fundamental_frequency);
            obj.mean_hnr = mean(obj.hnr);
            obj.dispersion_hnr = std(obj.hnr);
            obj.mean_vq = mean(obj.voicing_quotient);
            obj.dispersion_vq = std(obj.voicing_quotient);
        case 'median'
            obj.mean_formants = median(obj.formants,2);
            obj.dispersion_formants = mad(obj.formants, 1, 2);
            obj.mean_fundamental_frequency = median(obj.gross_fundamental_frequency);
            obj.dispersion_fundamental_frequency = mad(obj.gross_fundamental_frequency, 1);
            obj.mean_hnr = median(obj.hnr);
            obj.dispersion_hnr = mad(obj.hnr, 1);
            obj.mean_vq = median(obj.voicing_quotient);
            obj.dispersion_vq = mad(obj.voicing_quotient, 1);
        case 'central'
            numFor = size(obj.formants,2);
            isOdd = mod(numFor,2);
            if isOdd
                obj.mean_formants = obj.formants(:, numFor/2+0.5);                
            else
                obj.mean_formants = mean(obj.formants(:, numFor/2:numFor/2+1), 2);
            end
            numFor = length(obj.gross_fundamental_frequency);
            isOdd = mod(numFor,2);
            if isOdd
                obj.mean_fundamental_frequency = obj.gross_fundamental_frequency(numFor/2+0.5);
            else                           
                obj.mean_fundamental_frequency = mean(obj.gross_fundamental_frequency(numFor/2:numFor/2+1));
            end
            if ~isempty(obj.hnr)
                numFor = length(obj.voicing_quotient);
                isOdd = mod(numFor,2);
                if isOdd
                    obj.mean_hnr = obj.hnr(numFor/2+0.5);
                    obj.mean_vq = obj.voicing_quotient(numFor/2+0.5);
                else
                    obj.mean_hnr = mean(obj.hnr(numFor/2:numFor/2+1));
                    obj.mean_vq = mean(obj.voicing_quotient(numFor/2:numFor/2+1));
                end
            end
        otherwise
            error('Method is not recongnized. Please choose either mean or median')
    end
end

