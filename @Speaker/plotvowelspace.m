function h = plotvowelspace(obj, vowelType, lpc_order)
% h = plotvowelspace(obj, vowelType, lpc_order)
% plot vowel space of the Speaker object 
% 
% Input arguments:
% - obj: Speaker object
% - vowelType: choice between computing only oral vowels, only nasal, or
% both
% - lpc_order: order of the LPC used to compute the formants
%
% Output argument:
% - h: figure object in which appears the vowel space
% 
% Benjamin Elie, 2020
%
% See also Speaker, Phoneme
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; vowelType = 'oral'; end
if nargin < 3; lpc_order = 10; end
ph = obj.phonemes;
switch vowelType
    case 'oral'
        idx = find(arrayfun(@(x)strcmpi(x.phonetic_class,'oral vowel'), ph));
    case 'nasal'
        idx = find(arrayfun(@(x)strcmpi(x.phonetic_class,'nasal vowel'), ph));
    case 'all'
        findOral = arrayfun(@(x)strcmpi(x.phonetic_class,'oral vowel'), ph);
        findNasal = arrayfun(@(x)strcmpi(x.phonetic_class,'nasal vowel'), ph);
        idx = find(findOral | findNasal);
end

if isempty(idx)
    error('Phonemes are not labeled, please label phonemes.')
end
preemph = [1 0.63];
fk = nan(3, length(idx));
for k = 1:length(idx)
    phTmp = ph(idx(k));
    sigTmp = phTmp.signal;
    sr = phTmp.sampling_frequency;
    if sr > 10e3
       sigTmp = resample(sigTmp, 10e3, sr, 30);
       sro = 10e3;
    else
       sro = sr;
    end
    xwin = sigTmp.*hamming(length(sigTmp));
    xwin = filter(1,preemph,xwin);
    lpc_coeff = lpc(xwin, lpc_order);
    formants = angle(roots(lpc_coeff))/2/pi*sro;   
    formants = sort(formants(formants > 0 & formants < sro-10));
    fk(:,k) = formants(1:3);       
end

H12 = convhull(fk(1:2,:).');
H13 = convhull(fk([1 3],:).');
H23 = convhull(fk(2:3,:).');
Hall = convhull(fk.');

h = figure;
subplot(221);
plot(fk(1,:), fk(2,:), 'ok', 'markerfacecolor', 'b')
hold on;
plot(fk(1,H12), fk(2,H12), 'r', 'linewidth', 2)
set(gca,'ticklabelinterpreter','latex')
ylabel('$F2$')
xlabel('$F1$')
leg1 = legend('Vowels', 'Convex hull', 'Location', 'Best');
set(leg1, 'interpreter', 'latex');
title('$F1-F2$ space', 'interpreter', 'latex')
subplot(222);
plot(fk(2,:), fk(3,:), 'ok', 'markerfacecolor', 'b')
hold on;
plot(fk(2,H23), fk(3,H23), 'r', 'linewidth', 2)
set(gca,'ticklabelinterpreter','latex')
ylabel('$F3$')
xlabel('$F2$')
title('$F2-F3$ space', 'interpreter', 'latex')
subplot(223);
plot(fk(1,:), fk(3,:), 'ok', 'markerfacecolor', 'b')
hold on;
plot(fk(1,H13), fk(3,H13), 'r', 'linewidth', 2)
set(gca,'ticklabelinterpreter','latex')
ylabel('$F3$')
xlabel('$F1$')
title('$F1-F3$ space', 'interpreter', 'latex')
subplot(224);
plot3(fk(1,:), fk(2,:), fk(3,:), 'ok', 'markerfacecolor', 'b')
hold on;
plot3(fk(1,Hall), fk(2,Hall), fk(3, Hall), 'r', 'linewidth', 2)
set(gca,'ticklabelinterpreter','latex')
zlabel('$F3$')
ylabel('$F2$')
xlabel('$F1$')
title('$F1-F2-F3$ space', 'interpreter', 'latex')
set(gcf, 'units','normalized', 'position', [ 0 0 1 1 ])
end

