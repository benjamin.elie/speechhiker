classdef Speaker < handle
% Speaker: class used to store acoustic and articulatory data of a speaker
% from corpus
% 
% Properties of Speaker objects are:
% - mother_tongue: mother tongue of the speaker
% - id: identification number or label of the speaker in the corpus
% - L2: other languages spoken by the speaker
% - L2_level: level of other languages
% - sex: sex of the speaker
% - age: age of the speaker
% - utterance: cell array conataining all of the utterances of the speaker
% stored as SpeechAudio objects
% - phonemes: cell array conataining all of the phonemes uttered by the speaker
% stored as Phoneme objects
%
% Methods of Speaker objects are
% - plotvowelspace: plot the vowel space of the speaker using the phonemes
% stored in the phonemes properties
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, Phoneme
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    
    properties
        %%% Input  
        mother_tongue = [];
        id = [];   
        L2 = [];
        L2_level = [];
        sex = [];
        age = [];
    end
    
    properties (Hidden = true)
        utterance = [];
        phonemes = [];      
    end
    
    methods
         % Constructor method
        function obj = Speaker(varargin)
            for k = 1:2:length(varargin)
                field = varargin{k};
                argvalue = varargin{k+1};
                if isprop(obj,field)
                    obj.(field) = argvalue;
                end % if isprop
            end % for k ...
        end % end function
        
        h = plotvowelspace(obj, vowelType, meth)
    end
    
    
end

