classdef Glottis < Oscillator
% Glottis: class inherited from Oscillator. Describes the oscillating portion
% of the glottis
% 
% Specific properties of Glottis objects are:
% - subglottal_pressure: instantaneous pressure upstream the glottal
% constriction
% - supraglottal_pressure: pressure downstream the glottal constriction
% - DC_flow: low frequency component of the flow in the vocal tract at the
% current simulation step
% - max_opening: opening area of the fully open glottis
% - area: glottal opening area
% - DC_vector: low frequency component of the flow in the vocal tract
%
% When creating Glottis objects, defaults values for oscillator properties
% are assigned. Check them in the constructor method.
%
% Glottis objects have no specific methods at the moment
%
% Benjamin Elie, 2020
%
% See also Oscillator, Tongue
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    
    properties(Hidden=true)
        % Input
        subglottal_pressure = [];
        supraglottal_pressure = [];        
        % Output
        
        %%%% Instantaneous
        DC_flow = 0;
        max_opening = 4e-5;
        area = [];
        
        %%%% Time evolution
        DC_vector = [];
    end
    
    methods
          % Constructor method
        function obj = Glottis(varargin)
            if isempty(varargin)
                
                Qam = 1;
                Qak = 1;
                Qam2 = 0.2; 
                Qak2 = 0.2;
                
                rho_folds = 1020;
                hfolds = 1e-2*(0.065+0.05+0.115);%3e-3;
                kspring = 10;
                
                obj.rest_position = 5e-5*ones(2);
                obj.mass_position = 5e-5*ones(2);
                obj.width = 0.003;
                x1 = obj.width/(1+Qam2);  % 1st mass
                x2 = obj.width;   % 2nd mass
                x3 = x2+x1*1e-11;  % end of constriction
                obj.x_position = [ 0 x1 x2 x3 ];
                obj.length = 0.016;
                
                obj.Qr = 0.2;
                obj.Qrc = 0.5;
                                
                % Mechanical parameters
                m1ui = rho_folds*obj.length*obj.width*hfolds/(1+Qam2)/2*1.2; % upper mass 1 (kg)
                k1ui = kspring; % upper spring stiffness 1 (N/m)
                r1ui = obj.Qr*sqrt(k1ui*m1ui/2); % damping of the upper mass 1
                m2ui = m1ui*Qam2; % upper mass 2 (kg)
                k2ui = k1ui*Qak2; % upper spring stiffness 2 (N/m)
                kcui = k1ui*obj.Qrc; % upper coupling spring (N/m)
                r2ui = obj.Qr*sqrt(k2ui*m2ui/2); % damping of upper mass 2
                m1di = m1ui*Qam ; % lower mass 1 (kg)
                k1di = k1ui*Qak ; % lower spring stiffness 1 (N/m)
                m2di = m2ui*Qam ; % lower mass 2 (kg)
                r1di = obj.Qr*sqrt(k1di*m1di/2); % damping of lower mass 1
                k2di = k2ui*Qak; % lower spring stiffness 2 (N/m)
                kcdi = kcui*Qak ; % lower coupling stiffness (N/m)
                r2di = obj.Qr*sqrt(k2di*m2di/2); % damping of lower mass 2
                
                obj.initial_mass = [ m1ui, m2ui; m1di, m2di ]; % mass matrix
                obj.initial_stiffness = [ k1ui, k2ui; k1di, k2di ]; % stiffness matrix
                obj.initial_damping = [ r1ui, r2ui; r1di, r2di ]; % damping matrix
                obj.initial_coupling_stiffness = [ kcui; kcdi ]; % coupling stiffness
                clear m1ui k1ui r1ui m2ui k2ui kcui r2ui m1di k1di m2di r1di k2di kcdi r2di     
                
                obj.etha_k1 = 100;
                obj.etha_k2 = 100;
                obj.etha_h1 = 500;
                obj.etha_h2 = 500;
                obj.contact_stiffness = 4;
                obj.contact_damping = 2.2;
           
            else
                for k = 1:2:length(varargin)
                    field = varargin{k};
                    argvalue = varargin{k+1};
                    if isprop(obj,field)
                        obj.(field) = argvalue;
                    end % if isprop
                end % for k ...
            end
        end % end function
    end
end
