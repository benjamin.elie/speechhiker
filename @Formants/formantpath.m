function obj_out = formantpath(obj)
% obj_out = formantpath(obj)
% search true formant traectory from raw estimate
% 
% Input argument:
% - obj: Formant object
%
% Output argument:
% - obj_out: modified Formant object
%
% Benjamin Elie, 2020
%
% See also Formants
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

freq = obj.frequency;
bw = obj.bandwidth;
damp = obj.damping;
ampl = obj.amplitude;
tvec = obj.time_vector;
dT = median(diff(tvec));

[ nForm, nFrame ] = size(freq);
smooth_ord = ceil(0.2/dT);

% forward search
form_out_tmp = nan(size(freq));
form_out_tmp(:,1) = freq(:,1);
bwTmp = bw;
dampTmp = damp;
amplTmp = ampl;
for k = 2:nFrame
   fTmp = freq(:,k);
   fTmpm1 = freq(:,k-1);
   Idx = knnsearch(fTmp,fTmpm1); 
   newF = freq(Idx,k);
   C = unique(newF);
   if length(C) < length(newF)
       dF = abs(diff(freq(:,k-1:k), 1, 2));
       dup = find(hist(newF,unique(newF))>1);
       for kd1 = 1:length(dup)
           dup_ind = find(newF==newF(dup(kd1)));
           for kd2 = 1:length(dup_ind)-1
               [ ~, idxMax ] = max(dF(dup_ind));
               newF(dup_ind(idxMax)) = nan;
           end
       end
   end
   form_out_tmp(:,k) = newF;  
   bwTmp(:,k) = bw(Idx,k);
   bwTmp(isnan(newF)) = nan;
   dampTmp(:,k) = damp(Idx,k);
   dampTmp(isnan(newF)) = nan;
   amplTmp(:,k) = ampl(Idx,k);
   amplTmp(isnan(newF)) = nan;
end

% backward search
form_out_tmp_bw = nan(size(freq));
form_out_tmp_bw(:,end) = freq(:,end);
bwTmp_bw = bw;
dampTmp_bw = damp;
amplTmp_bw = ampl;
for k = nFrame-1:-1:1
   fTmp = freq(:,k);
   fTmpm1 = freq(:,k+1);
   Idx = knnsearch(fTmp,fTmpm1); 
   newF = freq(Idx,k);
   C = unique(newF);
   if length(C) < length(newF)
       dF = abs(diff(freq(:,k:k+1), 1, 2));
       dup = find(hist(newF,unique(newF))>1);
       for kd1 = 1:length(dup)
           dup_ind = find(newF==newF(dup(kd1)));
           for kd2 = 1:length(dup_ind)-1
               [ ~, idxMax ] = max(dF(dup_ind));
               newF(dup_ind(idxMax)) = nan;
           end
       end
   end
   form_out_tmp_bw(:,k) = newF;  
   bwTmp_bw(:,k) = bw(Idx,k);
   bwTmp_bw(isnan(newF)) = nan;
   dampTmp_bw(:,k) = damp(Idx,k);
   dampTmp_bw(isnan(newF)) = nan;
   amplTmp_bw(:,k) = ampl(Idx,k);
   amplTmp_bw(isnan(newF)) = nan;
end

freq_out = zeros(size(freq));
bw_out = bwTmp;
damp_out = dampTmp;
ampl_out = amplTmp;
for kF = 1:nForm
%     disp(['Smoothing F',num2str(kF),' out of ',num2str(nForm)])
    FnTmp = zeros(nFrame,1);    
    Fn_fw = form_out_tmp(kF,:);
    Fn_bw = form_out_tmp_bw(kF,:);
    FnTmp(1) = Fn_fw(1);
    for kFr = 2:nFrame
        dF = [ Fn_bw(kFr)-FnTmp(kFr-1); Fn_fw(kFr)-FnTmp(kFr-1) ];
        [ ~, idxMin ] = min(abs(dF));
        if idxMin == 1
            FnTmp(kFr) = Fn_bw(kFr);
            bw_out(kF, kFr) = bwTmp_bw(kFr);
            damp_out(kF, kFr) = dampTmp_bw(kFr);
            ampl_out(kF, kFr) = amplTmp_bw(kFr);            
        elseif idxMin == 2
            FnTmp(kFr) = Fn_fw(kFr);
            bw_out(kF, kFr) = bwTmp(kFr);
            damp_out(kF, kFr) = dampTmp(kFr);
            ampl_out(kF, kFr) = amplTmp(kFr); 
        end
    end

    freqTmp = smoothdata(FnTmp, smooth_ord, 'gaussian','omitnan');
    maxFreq = max(freqTmp);
    minFreq = min(freqTmp);
    idxN = find(~isnan(freqTmp));
    freqTmp = interp1(idxN, freqTmp(idxN), 1:length(freqTmp), 'linear', 'extrap');
    freqTmp(freqTmp <= minFreq) = minFreq;
    freqTmp(freqTmp >= maxFreq) = maxFreq;
    freq_out(kF, :) = freqTmp;
% %     bw_out(kF, :) = smooth(bw_out(kF, :), smooth_ord, 'loess');
%     damp_out(kF, :) = smoothdata(damp_out(kF, :), smooth_ord, 'gaussian','omitnan');
% %     ampl_out(kF, :) = smooth(ampl_out(kF, :), smooth_ord, 'loess');    
end

if nargout == 1
    obj_out = Formants('frequency', freq_out, ...
        'bandwidth', bw_out, ...
        'damping', damp_out, ...
        'amplitude', ampl_out);
else
    obj.frequency = freq_out;
    obj.bandwidth = bw_out;
    obj.damping = damp_out;
    obj.amplitude = ampl_out;
end

end

