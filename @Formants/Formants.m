classdef Formants < handle
% Formants: class used to store formant informations
% 
% Properties of Formants objects are:
% - frequency
% - bandwidth
% - damping
% - amplitude
% - time_vector
% - parent: SpeechAudio object on which the formants have been estimated
% - isForm: specify if formants or antiformants
%
% Methods of Formants objects are
% - formantpath: search true formant traectory from raw estimate
% - interpolate: interpolation for resampling
% - smoothing: smooth formant data
%
% Benjamin Elie, 2020
%
% See also SpeechAudio
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    
    properties        
        frequency = [];
        bandwidth = [];
        damping = [];
        amplitude = [];    
        isForm = [];
    end
    
    properties (Hidden = true)
        time_vector = [];      
        parent = [];
    end
    
    methods
         % Constructor method
        function obj = Formants(varargin)
            for k = 1:2:length(varargin)
                field = varargin{k};
                argvalue = varargin{k+1};
                if isprop(obj,field)
                    obj.(field) = argvalue;
                end % if isprop
            end % for k ...
        end % end function
        
        obj_out = formantpath(obj) 
        obj_out = transform(obj, varargin)
        interpolate(obj, txx)
    end
end

