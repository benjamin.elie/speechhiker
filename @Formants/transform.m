function obj_out = transform(obj, sr, varargin)
% obj_out = transform(obj, varargin)
% change formant parameters
% 
% Input argument:
% - obj: Formant object
% - sr: sampling frequency
% - varargin: should specify which parameter and the corresponding
% transform operation
%
% Output argument:
% - obj_out: modified Formant object
%
% Benjamin Elie, 2020
%
% See also Formants
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

freq = obj.frequency;
damp = obj.damping;
tVec = obj.time_vector;

[ nForm, nFrame ] = size(freq);
z = exp(2*pi*1i*freq/sr).*exp(-damp/sr);
z_module = abs(z);
z_angle = exp(1i*angle(z));
dampcoeff = 1;
freqcoeff = 1;

for k = 1:2:length(varargin)
    field = varargin{k};
    argvalue = varargin{k+1};
    switch lower(field)
        case 'frequency'
            freqcoeff = argvalue;
        case 'damping'
            dampcoeff = argvalue;
        otherwise
            error('Atttribute to modify not recognized. Please choose either frequency or damping');
    end
end
    
if max(size(freqcoeff)) == 1
    if max(size(dampcoeff)) == 1
        z_new = (z_module.^dampcoeff).*(z_angle.^argvalue);        
    else
        nForm2ch = size(dampcoeff, 1);  
        if nForm2ch > nForm
            nForm2ch = nForm;            
            dampcoeff = dampcoeff(1:nForm, [1:nForm size(dampcoeff, 2)]);
        end
        z_new = z;
        for kF = 1:nFrame                
            f_pos = damp(1:nForm2ch, kF);
            damp_vec = [ f_pos(:); 1 ];
            new_damp = dampcoeff*damp_vec;
            ratio_damp = new_damp./f_pos;
            z_module_tmp = z_module(1:nForm2ch, kF);
            z_angle_tmp = z_angle(1:nForm2ch, kF);
            z_new(1:nForm2ch, kF) = (z_module_tmp.^ratio_damp).*(z_angle_tmp.^freqcoeff);  
        end        
    end
else
    nForm2ch = size(freqcoeff, 1);  
    if nForm2ch > nForm
        nForm2ch = nForm;            
        freqcoeff = freqcoeff(1:nForm, [1:nForm size(freqcoeff, 2)]);
    end
    
    if max(size(dampcoeff)) == 1
        ratio_damp = dampcoeff;
        isVec = false;
    else
        nDamp2ch = size(dampcoeff, 1);  
        if nDamp2ch > nForm
            nDamp2ch = nForm;            
            dampcoeff = dampcoeff(1:nForm, [1:nForm size(dampcoeff, 2)]);
        end
        isVec = true;
    end
    z_new = z;
    for kF = 1:nFrame                
        f_pos = freq(1:nForm2ch, kF);
        freq_vec = [ f_pos(:); 1 ];
        new_freq = freqcoeff*freq_vec;
        ratio_freq = new_freq./f_pos;
        z_module_tmp = z_module(1:nForm2ch, kF);
        z_angle_tmp = z_angle(1:nForm2ch, kF);
        if isVec
            disp('Change all damp coeff')
            damp_pos = damp(1:nForm2ch, kF);
            damp_vec = [ f_pos(:); 1 ];
            new_damp = dampcoeff*damp_vec;
            ratio_damp = new_damp./damp_pos;
        end
        z_new(1:nForm2ch, kF) = (z_module_tmp.^ratio_damp).*(z_angle_tmp.^ratio_freq);         
    end
    
end

freq_new = angle(z_new)/2/pi*sr;
damp_new = -log(abs(z_new))*sr;
bw_new = damp_new./(pi);

if nargout == 1
    obj_out = Formants('frequency', freq_new, ...
        'damping', damp_new, ...
        'bandwidth', bw_new, ...
        'amplitude', obj.amplitude, ...
        'time_vector', tVec, ...
        'parent', obj.parent, ...
        'isForm', obj.isForm);
else
    obj.frequency = freq_new;
    obj.damping = damp_new;  
end

end

