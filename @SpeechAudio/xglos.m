function [obj_voiced, obj_unvoiced, F0, harm, mvfint] = xglos(obj, param)
% [obj_voiced, obj_unvoiced, F0, harm, mvfint] = xglos(obj, param)
% performs X-GLOS for a voiced/noise separation of the SpeechAudio object
%
% Input arguments:
% - obj: SpeechAudio object
% - param: tunable parameters for X-GLOS 
%
% Output argument:
% - obj_voiced: VoicedSignal object containing only the voiced
% contributions of the SpeechAudio object
% - obj_unvoiced: UnvoicedSignal object containing only the noise
% contributions of the SpeechAudio object
% - F0: fundamental frequency
% - harm: detected harmonic frequencies at each temporal frame
% - mvfint: estimates of the maximal voiced frequency
%
% Benjamin Elie, 2020
%
% See also SpeechAudio
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

S = obj.signal(:);
sr = obj.sampling_frequency;

if isempty(S)
    error('Signal is empty')
end

F0 = struct('f',[],'t',[]);

Lsec = 0.04; % default length of the window, in s
warning off
%%%%% Default values of param
if nargin < 2; param = []; end
if ~isfield(param,'lw'); param.lw = 2^nextpow2(Lsec * sr); end
if ~isfield(param,'hopfactor'); param.hopfactor = 4; end
if ~isfield(param,'thrs'); param.thrs = 3.5; end
if ~isfield(param,'Nfft'); param.nfft = 2^nextpow2(param.lw*10); end
if ~isfield(param,'lfmed'); param.lfmed = 250; end
if ~isfield(param,'nper'); param.nper = 4; end
if ~isfield(param,'fomin'); param.fomin = 80; end
if ~isfield(param,'fomax'); param.fomax = 300; end
if ~isfield(param,'nh'); param.nh = floor(sr/2/param.fomin); end
if ~isfield(param,'partialLength'); param.partialLength = 7; end
if ~isfield(param,'searchRange'); param.searchRange = 7; end
if ~isfield(param,'np'); param.np = 6; end
if ~isfield(param,'nzero'); param.nzero = 1; end
if ~isfield(param,'pct'); param.pct = 90; end
if ~isfield(param,'meth'); param.meth = 'LSE'; end
if ~isfield(param,'stdmeth'); param.stdmeth = 'std'; end
if ~strcmp(param.meth,'LSE') && ~strcmp(param.meth,'WLSE'); warning on; warning('param.meth is badly defined. It has been set to LSE (default)'); param.meth = 'LSE'; warning off; end
if ~isfield(param,'jitt'); param.jitt = false; end
if ~isfield(param,'shim'); param.shim = false; end
if (param.jitt || param.shim) && (~isfield(param,'ord')); param.ord = ceil(min(param.lw/3,501)); end
if param.jitt || param.shim; param.fcoef = fir1(param.ord,2*param.partialLength/(sr/2),'low'); end
if (param.jitt || param.shim) && (~isfield(param,'methfreq')); param.methfreq = 'zcr'; end
if ~isfield(param,'verb'); param.verb = 0; end

%%%% Build sliding window for the time signal
Lw = param.lw;
w = hann(Lw+1);
w = w(2:end);
param.hop = param.lw/param.hopfactor;
a = param.hop;

% Initialization
%%%% zero-padding of the signal at both extremities
npad = Lw;
Spad = [zeros(npad, 1) ; S ; zeros(npad, 1)];
Nframes = ceil((length(Spad)-2*Lw)/a);

f0 = NaN(Nframes, 1);
cumf0 = f0;

t = (1:Nframes) * (a/sr)+Lw/2/sr;
param.t = t;

if isempty(obj.gross_fundamental_frequency)
    pks = zeros(param.nfft, Nframes);
    x = zeros(Lw,Nframes);
    param.lfmed = ceil(param.fomin*2.5);
    for u = 1:Nframes
        x(:,u) = Spad(u*a:u*a+Lw-1);  
        % pks is the cleaned version of the periodogram
        pks(:,u) = findpartials(x(:,u), sr, ones(length(x(:,u)),1),  param);    
        [ f0(u), cumf0(u) ] = findf0(pks(:,u), sr, param);% Get pitch and detected harmonics
        % adapts the length of the median filter
        if f0(u); param.lfmed = ceil(f0(u)*2.5); end
    end
else
    f0_gross = obj.gross_fundamental_frequency;
    tfo = obj.gross_f0_time_vector;
    f0 = interp1(tfo, f0_gross, t, 'linear','extrap');
end

% octave correction
fk = f0reeval(f0, Spad, sr, param);
f0 = fk(:,1);
mvf = max(fk,[],2);

% interpolation of f0 and MVF
tfo = (0:length(Spad)-1)/sr;
foint = interp1(t,f0,tfo,'pchip',0);
mvfint = interp1(t,mvf,tfo,'pchip',0);

Sh = zeros(size(Spad));
Sh2 = Sh;
 
harm = fk;
harm(~harm) = NaN;

if param.jitt || param.shim
    sfilt = zeros(length(Spad),Nframes);
    fkcmat = zeros(Nframes,1);
    fojmat = nan(length(Spad),Nframes);
    aimat = fojmat;
    kfk = 1;  
    for u = 1:Nframes
        idx = u*a:u*a+Lw-1;
        if fk(u,1)>0 
            fkc = round(fk(u,1));
            if ~any(fkcmat == fkc)            
                bb = param.fcoef.*cos(2*pi*fkc/sr*(0:length(param.fcoef)-1));
                xwf = filtfilt(bb,1,Spad)*2;
                sfilt(:,kfk) = xwf;
                fkcmat(kfk) = fkc;
                xwf = xwf(idx);
                kfk = kfk + 1;
            else
                xwf = sfilt(idx,fkcmat == fkc);
            end   
            [ fojitt, aijitt ] = instantaneousfrequency(xwf,sr, fk(u,1), param.methfreq);
            aijitt(fojitt < param.fomin | fojitt > param.fomax) = 0;
            fojitt(fojitt < param.fomin | fojitt > param.fomax) = 0;
            aimat(idx,u) = aijitt;
            fojmat(idx,u) = fojitt;
        end
    end
    foj = nanmedian(fojmat,2);
    aij = nanmedian(aimat,2);
    aij(isnan(foj)) = 0;
    aij(foj < param.fomin | foj > param.fomax) = 0;
    aij = smooth(aij, 101,'lowess');   
    foj(isnan(foj)) = 0;
    foj(foj < param.fomin | foj > param.fomax) = 0;
    foj = smooth(foj, 101,'lowess');    
    foint = foj;
end

amplitudes = nan(size(harm));

for u = 1:Nframes
    idx = u*a:u*a+Lw-1;
    wh = hann(length(idx)+1); wh = wh(2:end);       
	s = Spad(u*a:u*a+Lw-1).*wh; 
        
    if fk(u,1)>0 
        nk = fk(u,:)/fk(u,1);
        nk(nk==0) = [];        
        if param.shim
            aijitt = aij(idx);
        else
            aijitt = 1;
        end
        hann_window = repmat(wh(:).*aijitt,[1,length(nk)]); 
        if param.jitt 
            fojitt = foj(idx);
            alp = exp(1i*2*pi/sr*((cumsum(fojitt))*nk));
        else
            alp = exp(1i*2*pi/sr*(0:length(s)-1).'*(nk*fk(u,1)));
        end         
        E = alp(1:length(s),:).*hann_window; 
        if strcmp(param.meth,'LSE') 
            bk = E\s;
        elseif strcmp(param.meth,'WLSE')
            bk = wlse(s, exp(2*1i*pi*(nk*fk(u,1))/sr), ceil(length(s)/4), wh,'apesk');
        end        
        amplitudes(u, 1:length(bk)) = bk;
        Sp = real(E*bk(:))*2;
    else
        Sp = zeros(size(s));
    end
    Sh(idx) = Sh(idx)+Sp.*wh;    
    Sh2(idx) = Sh2(idx)+s.*wh;
end

frame_bound = 3/2 * param.hopfactor/4;

Sh = Sh/frame_bound;
Sh2 = Sh2/frame_bound;

F0.f = f0(2:end-1);
F0.t = t(2:end-1);
deb = npad+1; fin = deb+length(S)-1;
Sh = Sh(deb:fin);
Sh2 = Sh2(deb:fin);
foint = foint(deb:fin);
mvfint = mvfint(deb:fin);
F0.fint = foint;
F0.tint = (0:length(foint)-1)/sr;
Sn = (Sh2 - Sh);

obj_voiced = VoicedSignal('signal', Sh, 'sampling_frequency', sr, ...
'amplitudes', amplitudes(2:end-1,:), 'harmonics', harm(2:end-1,:));
obj_unvoiced = UnvoicedSignal('signal', Sn, 'sampling_frequency', sr);

obj.voiced_signal = obj_voiced;
obj.unvoiced_signal = obj_unvoiced;
obj.instantaneous_fundamental_frequency = foint;
obj.gross_fundamental_frequency = F0.f;
obj.gross_f0_time_vector = F0.t-F0.t(1); 
obj.maximal_voiced_frequency = mvfint;
obj.voiced_signal.harmonics = harm(2:end-1, :);

end


