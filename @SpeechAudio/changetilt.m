function obj_out = changetilt(obj, tilt_factor, param)
% obj_out = changetilt(obj, tilt_factor, param)
% modify the spectral tilt of the SpeechAudio object. Requires a
% voiced/noise separation.
% 
% Input arguments:
% - obj: SpeechAudio object
% - tilt_factor: factor to apply to the spectral tilt
% - param: structure containing parameters for the separation
%
% Output argument:
% - obj_out: modified SpeechAudio object
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, xglos, VoicedSignal, UnvoicedSignal
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Lsec = 0.04; % default length of the window, in s
warning off
sr = obj.sampling_frequency;

%%%%% Default values of param
if nargin < 3; param = []; end
if ~isfield(param,'lw'); param.lw = 2^nextpow2(Lsec * sr); end
if ~isfield(param,'hopfactor'); param.hopfactor = 4; end

if isempty(obj.signal)
    error('Signal is empty')
end

if isempty(obj.voiced_signal)
    obj.xglos(param);
end

S = obj.signal(:);

%%%% Build sliding window for the time signal
Lw = param.lw;
w = hann(Lw+1);
w = w(2:end);
param.hop = param.lw/param.hopfactor;
a = param.hop;

%Initialization
%%%% zero-padding of the signal at both extremities
npad = Lw;
Spad = [zeros(npad, 1) ; S ; zeros(npad, 1)];

Nframes = ceil((length(Spad)-2*Lw)/a);

t = (1:Nframes) * (a/sr)+Lw/2/sr;


harm = obj.voiced_signal.harmonics;
if size(harm, 1) ~= Nframes
    harm = interp1(obj.gross_f0_time_vector, harm, t, 'linear', 'extrap');
end

freq_out = (0:Lw/2)*sr/Lw;
Sh = zeros(size(Spad));

for u = 1:Nframes
    idx = u*a:u*a+Lw-1;
    wh = hann(length(idx)+1); wh = wh(2:end);       
	s = Spad(u*a:u*a+Lw-1).*wh; 
    fk = harm(u,:);
        
    if fk(1) > 0 
        nk = fk/fk(1);
        nk( nk==0 | isnan(nk) )= [];               
        hann_window = repmat(wh(:),[1,length(nk)]); 
        alp = exp(1i*2*pi/sr*(0:length(s)-1).'*(nk*fk(1)));
        E = alp(1:length(s),:).*hann_window; 
        bk = E\s;
        Sp = real(E*bk(:))*2;
        
        if length(bk) > 1
            bktmp = bk(~isnan(fk));
            fktmp = fk(~isnan(fk));
            fklim = max(fk)*1.2;
            p_obs = polyfit(fktmp(:), log(abs(bktmp)), 1);
            p1 = log(abs(bk(1)))-p_obs(1)*fk(1);
            line_obs = p_obs(1)*freq_out+p1;
            p2 = log(abs(bk(1)))-tilt_factor*p_obs(1)*fk(1);
            line_targ = tilt_factor*p_obs(1)*freq_out+p2;
            
            amp_ratio = exp(line_targ-line_obs);
            amp_ratio(freq_out<=fk(1) | freq_out >= fklim) = 1;
            amp_ratio = amp_ratio(:);
           
            ampfk = interp1(freq_out(:), amp_ratio(:), fktmp(:));
            bk = bk(:).*ampfk(:);
            Sp_modif = real(E*bk(:))*2;
        else
            Sp_modif = Sp;
        end
        
    else
        Sp_modif = zeros(size(s));
    end
    Sh(idx) = Sh(idx)+Sp_modif.*wh;    
end

frame_bound = 3/2 * param.hopfactor/4;

Sh = Sh/frame_bound;

deb = npad+1; fin = deb+length(S)-1;
Sh = Sh(deb:fin);
signal_out = Sh+obj.unvoiced_signal.signal(:);

if nargout < 1
    obj.signal = signal_out;
else
    obj_out = SpeechAudio('signal', signal_out, ...
        'sampling_frequency', obj.sampling_frequency);
end

end


