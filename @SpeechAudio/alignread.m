function [ labels, instants ] = alignread(obj, fileName)
% [ labels, instants ] = alignread(obj, fileName)
% read alignment file as textgrid
% 
% Input arguments:
% - obj: SpeechAudio object
% - fileName: name of the segmentation file (supports textgrid, xml and
% JSON formats)
%
% Output argument:
% - labels: segmentation labels
% - instants: segmenation time stamps
%
% Benjamin Elie, 2020
%
% See also SpeechAudio
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% check if textgrid or xml
if contains(lower(fileName), '.textgrid')
    [labels,instants] = read_textgrid_segmentation_files(fileName);
    
elseif contains(lower(fileName), '.xml')
    [labels, instants] = read_xml_segmentation_files(fileName);
   
elseif contains(lower(fileName), '.json')
    [labels, instants] = read_json_segmentation_files(fileName);  
elseif contains(lower(fileName), '.seg')
    [labels, instants] = read_seg_segmentation_files(fileName); 
elseif contains(lower(fileName), '.whc')
    [labels, instants] = read_whc_segmentation_files(fileName); 
else
    error('File format is not recognized')
end

obj.phonetic_labels = labels;
obj.phonetic_instants = instants;

end



