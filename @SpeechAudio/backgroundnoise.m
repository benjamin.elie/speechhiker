function [xOut, backNoise] = backgroundnoise(obj, Pxx, targetSNR)
% [xOut, backNoise] = backgroundnoise(obj, Pxx, targetSNR)
% add background noise to the SpeechAudio object with a specific SNR
% 
% Input arguments:
% - obj: SpeechAudio object
% - Pxx: PSD of the background noise
% - targetSNR: desired SNR
%
% Output argument:
% - xOut: output signal with background noise
% - backNoise: background noise 
%
% Benjamin Elie, 2020
%
% See also SpeechAudio
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

sig = obj.signal;
nSig = length(sig);
sr = obj.sampling_frequency;

nwin = 4096;
novlp = nwin*3/4;
nhop = nwin-novlp;
w = hann(nwin);
nfft = 2^nextpow2(sr);
L = nfft/2+1;

wNoise = randn(nSig, 1);   
backNoise = zeros(size(wNoise));
xbuff = buffer(wNoise, nwin, novlp, 'nodelay');

for kbuff = 1:size(xbuff,2)
    iStart = (kbuff-1)*nhop+1;
    iEnd = min(iStart + nwin - 1, length(backNoise));
    idx = iStart:iEnd;
    xwin = xbuff(:,kbuff).*w;
    fft_xwin = fft(xwin, nfft); 
    fft_xwin = fft_xwin(1:L);

    fft_new = fft_xwin(:).*abs(Pxx(:));
    x_new = real(ifft([fft_new; conj(flipud(fft_new(2:end))) ], nfft, 'symmetric'));
    new_end = min(nwin, length(idx));
    backNoise(iStart:iEnd) = backNoise(idx) + x_new(1:new_end);

end
backNoise = backNoise / 1.5;    
X = sqrt(var(sig)/var(backNoise)/targetSNR);
backNoise = backNoise*X;
xOut = sig(:) + backNoise;
obj.signal = xOut;

end

