function [ VQ, HNR ] = computeVQ(obj, nwin, novlp, nfft, meth)
% [ VQ, HNR ] = computeVQ(obj, nwin, novlp, nfft)
% Compute the voicing quotient (and the armonic-to-noise ratio HNR) from
% the Voiced and Unvoiced parts of the SpeechAudio object. Based on the
% spectrogram
%
% Input arguments:
% - obj: SpeechAudio object
% - nwin: length of the window
% - novlp: number of overlapping samples
% - nfft: order of the FFT
% - meth: choice of the method (either with spectrogram, or with time
% windows)
%
% Output arguments:
% - VQ: voicing quotient (in %)
% - HNR: harmonic-to-noise ratio (in dB)
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, VoicedSignal, UnvoicedSignal, xglos
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% check if separation has been done
if isempty(obj.voiced_signal) || isempty(obj.unvoiced_signal)
    error('No separation has been done. Please run X-GLOS');
end

sr = obj.sampling_frequency;
tvec = (0:length(obj.signal)-1)/sr;

if nargin < 2; nwin = 1024; end
if nargin < 3; novlp = nwin*3/4; end
if nargin < 4; nfft = 2^nextpow2(sr); end
if nargin < 5; meth = 'rms'; end

V = obj.voiced_signal;
U = obj.unvoiced_signal;

switch lower(meth)
    case 'spectro'
        [ Sxx_V, fxx, txx ] = V.computespectrogram(nwin, novlp, nfft);
        Sxx_U = U.computespectrogram(nwin, novlp, nfft);
        grossVQ = (sum(abs(Sxx_V).^2, 1))./(sum(abs(Sxx_U).^2, 1) + sum(abs(Sxx_V).^2, 1));
        grossHNR = 20*log10(abs((sum(abs(Sxx_V).^2, 1))./(sum(abs(Sxx_U).^2, 1))));
    case 'rms'
        nhop = nwin-novlp;
        Vbuff = buffer(V.signal, nwin, novlp, 'nodelay');
        Ubuff = buffer(U.signal, nwin, novlp, 'nodelay');
        nFrame = size(Vbuff,2);
        txx = (0:nhop:length(obj.signal))/sr;
        txx = txx(1:nFrame) + nwin/sr/2;

        grossVQ = zeros(nFrame, 1);
        grossHNR = zeros(nFrame, 1);
        w = hann(nwin);

        for k = 1:nFrame
            xpt = Vbuff(:,k).*w(:); 
            xpt = xpt-mean(xpt);
            xnt = Ubuff(:,k).*w; 
            xnt = xnt-mean(xnt);
            grossVQ(k) = ((norm(xpt,2).^2)/(norm(xpt,2).^2+norm(xnt,2).^2)).^2;
            grossHNR(k) = ((norm(xpt,2).^2)/(norm(xnt,2).^2)).^2;
        end
    otherwise
        error('The method to compute VQ does not exist. Please choose either spectro or rms');
end

VQ = interp1(txx,grossVQ,tvec)*100;
HNR = interp1(txx,grossHNR,tvec);

obj.voicing_quotient = VQ;
obj.hnr = HNR;

if isempty(obj.time_vector)
    obj.time_vector = tvec;
end

end

