function obj_out = formantshifting(obj, formTarget, nwin, novlp, nfft)
% obj_out = formantshifting(obj, formTarget, nwin, novlp, nfft)
% modify the SpeechAudio object so that the formant frequency match formTarget, defined as a Formant object
% 
% Input argument:
% - obj: SpeechAudio object
% - formTarget: Formant object specifying the desired result
% - nwin: window length
% - novlp: number of overlapping samples
% - nfft: length of the FFT
%
% Output argument:
% - obj_out: modified SpeechAudio object
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, computeformants, Fomrants, transform
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


if nargin < 3; nwin = 256; end
if nargin < 4; novlp = nwin*3/4; end
if nargin < 5; nfft = nwin; end

x = obj.signal;
x_out = zeros(size(x));
sr = obj.sampling_frequency;
if isempty(obj.formants)
    warning('No formants were computed, running formant estimation right now')
    obj.formantestimate(nwin, novlp, sr/1000+2, true);
end

xbuff = buffer(x, nwin, novlp, 'nodelay');
nhop = nwin-novlp;

nFrame = size(xbuff, 2);
txx = (0:nhop:length(x))/sr;
txx = txx(1:nFrame);
nF = length(formTarget);
isNewZero = false;
arrayfun(@(x)x.interpolate(txx),formTarget);
if nF == 2
    zeroTarget = formTarget(2);
    formTarget = formTarget(1);
    isNewZero = true;
end

currForm = obj.formants;
currForm.interpolate(txx);
% arrayfun(@(x)x.interpolate(txx),formTarget);

oldDamp = currForm.damping;
newDamp = formTarget.damping;
oldFreq = currForm.frequency;
newFreq = formTarget.frequency;
oldZ = exp(2*pi*1i*oldFreq/sr).*exp(-oldDamp/sr);
newZ = exp(2*pi*1i*newFreq/sr).*exp(-newDamp/sr);
oldZ = [ oldZ; conj(oldZ) ];
newZ = [ newZ; conj(newZ) ];
if isNewZero
    newZeroFreq = zeroTarget.frequency;
    newZeroDamp = zeroTarget.damping;
    newZero = exp(2*pi*1i*newZeroFreq/sr).*exp(-newZeroDamp/sr);
    newZero = [ newZero; conj(newZero)];
else
    newZero = zeros(size(newZ));
end
if ~isempty(obj.zeros)
    oldZeroFreq = obj.zeros.frequency;
    oldZeroDamp = obj.zeros.damping;
    oldZero = exp(2*pi*1i*oldZeroFreq/sr).*exp(-oldZeroDamp/sr);
    oldZero = [ oldZero; conj(oldZero) ];
else
    oldZero = zeros(size(oldZ));
end

w = hann(nwin);
L = nfft/2+1;

for k = 1:size(xbuff,2)
    deb = (k-1)*nhop+1;
    fin = deb + nwin - 1;
    if fin > length(x_out)
        x_out = [ x_out; zeros(nwin,1) ];
    end
    idx = deb:fin;
    xwin = xbuff(:,k).*w;
    fft_xwin = fft(xwin, nfft); 
    fft_xwin = fft_xwin(1:L);

    [ ma_new, ar_new ] = zp2tf(newZero(:,k), newZ(:,k), 1);
    [ ma_old, ar_old ] = zp2tf(oldZero(:,k), oldZ(:,k), 1);
    
    h_orig = freqz(ma_old, ar_old, L);
    h_new = freqz(ma_new, ar_new, L);
    
    fft_new = fft_xwin.*(abs(h_new))./(abs(h_orig));
    x_new = real(ifft([fft_new; conj(flipud(fft_new(2:end))) ], nfft, 'symmetric'));
    x_out(deb:fin) = x_out(idx) + x_new(1:nwin);
end

x_out = x_out(1:length(x)) / 1.5;

if nargout < 1
    obj.signal = x_out;
else
    obj_out = SpeechAudio('signal', x_out, ...
        'sampling_frequency', obj.sampling_frequency, ...
        'phonetic_labels', obj.phonetic_labels, ...
        'phonetic_instants', obj.phonetic_instants, ...
        'formants', formTarget, ...
        'zeros', zeroTarget);
end
