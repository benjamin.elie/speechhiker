function [ Sxx, fxx, txx ] = computecepstrumenvelope(obj, nwin, novlp, order)
% [ Sxx, fxx, txx ] = computespectrumenvelope(obj, nwin, novlp, order)
% Compute the spectral envelope of the SpeechAudio object at different time
% frames using cepstral coefficients.
%
% Input arguments:
% - obj: SpeechAudio object
% - nwin: window length
% - novlp: number of overlapping samples
% - order: order of the cepstrum. If not specified, it is automatically
% adapted from pitch value
%
% Output argument:
% - Sxx: Complex sdpectral envelope matrix
% - fxx: frequency vector
% - txx: time vector
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, plotspectralenvelope
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; nwin = 1024; end
if nargin < 3; novlp = nwin*3/4; end
if nargin < 4; order = []; end

x = obj.signal;
sr = obj.sampling_frequency;
nhop = nwin-novlp;

xbuff = buffer(x, nwin, novlp, 'nodelay');
w = hamming(nwin);
L = nwin/2+1;
fxx = (0:L-1)*sr/nwin;

nFrame = size(xbuff,2);
txx = (0:nhop:length(x))/sr;
txx = txx(1:nFrame);

Sxx = zeros(L, nFrame);

for k = 1:nFrame
    xwin = xbuff(:,k).*w;
    xRealCep = rceps(xwin); 
    
    if isempty(order)
        minrange = max(ceil(sr/nwin), 50);
        maxrange = 400;
        try
            f0 = pitch(xwin, sr, 'range', [minrange, maxrange], 'windowlength', nwin, 'overlaplength', 0);
        catch
            SpTmp = SpeechAudio('signal', xwin, ...
                'sampling_frequency', sr);
            prm = [];
            prm.win_size = nwin;
            prm.fomin = minrange;
            prm.fomax = maxrange;
            SpTmp.yin(prm);
            f0 = nanmedian(SpTmp.gross_fundamental_frequency);
        end
        if isnan(f0)
            f0 = 125;
        end
        CepOrder = 2*round(sr/f0)-4;       
    else
        CepOrder = order;
    end
    
    if floor(CepOrder/2) == CepOrder/2
        CepOrder=CepOrder-1; 
    end
    winCeps = hann(CepOrder);
    wzp = [winCeps(((CepOrder+1)/2):CepOrder); ...
        zeros(nwin-CepOrder, 1); ...
           winCeps(1:(CepOrder-1)/2)]; 
    liftCeps = wzp.*xRealCep;  
    xEnv = real(fft(liftCeps));
    Sxx(:,k) = exp(xEnv(1:L));
end

outputEnvelope = TimeFreq('parent', obj, ...
    'time_frequency_matrix', Sxx, ...
    'time_vector', txx, ...
    'frequency_vector', fxx, ...
    'window', w, ...
    'overlap', novlp, ...
    'nfft', nwin);

obj.spectral_envelope = outputEnvelope;

