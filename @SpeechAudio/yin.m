function [ f0, timeFrame ] = yin(obj, prm)
% [ f0, timeFrame ] = yin(obj, prm)
% compute the fundamental frequency of the SpeechAUdio object with YIN
%
% Input arguments:
% - obj: SpeechAudio object
% - param: tunable parameters for YIN
%
% Output argument:
% - f0: gross estimae of the fundamental frequency
% - timeFrame: time vector of the same length as f0
%
% Benjamin Elie, 2020
%
% See also SpeechAudio
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; prm = []; end
if ~isfield(prm,'win_size'); prm.win_size = 256; end
if ~isfield(prm, 'overlap'); prm.overlap = max(1,floor(prm.win_size*3/4)); end
if ~isfield(prm,'threshold'); prm.threshold = 0.1; end
if ~isfield(prm, 'fomin'); prm.fomin = 80; end
if ~isfield(prm,'fomax'); prm.fomax = 300; end

x = obj.signal;
sr = obj.sampling_frequency;
nwin = prm.win_size;
novlp = prm.overlap;
f0_max = prm.fomax;
f0_min = prm.fomin;
harm_threshold = prm.threshold;

nStep = nwin-novlp;

if sr == 1
    warning("Warning: the sampling frequency is 1");
end
tau_min = floor(sr/f0_max);
tau_max = ceil(sr/f0_min);

timeFrame = (0:nStep:(length(x) - nwin))/sr;
frames = buffer(x, nwin, novlp, 'nodelay');
numFrame = length(timeFrame);

f0 = zeros(numFrame, 1);
harmRate = zeros(numFrame, 1);
argmins = zeros(numFrame, 1);

for k = 1:numFrame
    
    currFrame = frames(:,k);

    diffFun = differenceFunction(currFrame, tau_max);
    N = min(tau_max, length(diffFun));
    cmdf = [ 1; diffFun(2:end).*(1:N-1).'./cumsum(diffFun(2:end)) ];    
    
    [ ~, idxMin ] = min(cmdf);
    
    p = getPitch(cmdf, tau_min, tau_max, harm_threshold);

    if idxMin>tau_min
        argmins(k) = sr/idxMin;
    end
    if p ~= 0
        f0(k) = sr/p;
        harmRate(k) = cmdf(p);
    else
        f0(k) = 0;
        harmRate(k) = min(cmdf);
    end
end

obj.gross_fundamental_frequency = f0;
obj.gross_f0_time_vector = timeFrame; 
end

function y = rfft(x, nfft)
    L = floor(nfft/2+1);
    y = fft(x, nfft);
    y = y(1:L);
end

function y = irfft(x)

    even = 1-mod(length(x), 2);    
    if even
        n = 2 * (length(x) - 1 );
        s = length(x) - 1;
    else
        n = 2 * (length(x) - 1 )+1;
        s = length(x);
    end
    xn = zeros(1,n);
    xn(1:length(x)) = x;
    xn(length(x)+1:n) = conj(x(s:-1:2));
    y = ifft(xn);
end

function diffFun = differenceFunction(x, tau_max)
    
    nx = length(x);
    tau_max = min(tau_max, nx);
    diffFun = [ 0; cumsum(x(:).^2) ];
    sizeFrame = nx + tau_max;
    tmp = dec2bin(floor(sizeFrame/32));
    p2 = length(tmp);
    nice_numbers = [16, 18, 20, 24, 25, 27, 30, 32];
    pad_vector = nice_numbers*(2^p2);
    size_pad = min(pad_vector(pad_vector >= sizeFrame));
    fc = rfft(x, size_pad);
    conv_tmp = irfft(fc.*conj(fc));
    conv_tmp = conv_tmp(1:tau_max);
    diffFun = diffFun(nx:-1:nx-tau_max+1)+diffFun(nx)-diffFun(1:tau_max)-2*conv_tmp(:);
    
end

function p = getPitch(cmdf, tau_min, tau_max, harm_threshold)

    p = 0;
    tau_start = tau_min;
    while tau_start < tau_max
        if cmdf(tau_start+1) < harm_threshold
            while tau_start + 1 < tau_max && cmdf(tau_start + 2) < cmdf(tau_start + 1)
                tau_start = tau_start + 1;
            end
            p = tau_start;
        end
        tau_start = tau_start + 1; 
    end
    if p ~= 0
        p = p-1;
    end
    
end

