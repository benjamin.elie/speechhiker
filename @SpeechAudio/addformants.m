function obj_out = addformants(obj, tf_add, nwin, novlp, nfft)
% obj_out = addformants(obj, tf_add, nwin, novlp, nfft)
% Modification of the spectral envelope of the SpeechAudio object.
% 
% Input arguments:
% - obj: Oscillator object
% - tf_add: transfer function of the transform linear function
% - nwin: window length
% - novlp: number of overlapping samples
% - nfft: length of the FFT
%
% Output argument:
% - obj_out: retunrs the modified SpeechAudio object
%
% Benjamin Elie, 2020
%
% See also SpeechAudio
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 3; nwin = 256; end
if nargin < 4; novlp = nwin*3/4; end
if nargin < 5; nfft = nwin; end

x = obj.signal;
x_out = zeros(size(x));

xbuff = buffer(x, nwin, novlp, 'nodelay');
nhop = nwin-novlp;
w = hann(nwin);
L = nfft/2+1;

for k = 1:size(xbuff,2)
    deb = (k-1)*nhop+1;
    fin = min(deb + nwin - 1, length(x_out));
    idx = deb:fin;
    xwin = xbuff(:,k).*w;
    fft_xwin = fft(xwin, nfft); 
    fft_xwin = fft_xwin(1:L);
 
    fft_new = fft_xwin(:).*abs(tf_add(:));
    x_new = real(ifft([fft_new; conj(flipud(fft_new(2:end))) ], nfft, 'symmetric'));
    new_end = min(nwin, length(idx));
    x_out(deb:fin) = x_out(idx) + x_new(1:new_end);

end

if nargout < 1
    obj.signal = x_out;
else
    obj_out = SpeechAudio('signal', x_out, ...
        'sampling_frequency', obj.sampling_frequency);
end

