function obj_out = cepstraltransform(obj, transOper, nwin, novlp, isSmooth)
% obj_out = cepstraltransform(obj, transOper)
% modify the SpeechAudio object by cepstral transform
% 
% Input argument:
% - obj: SpeechAudio object
% - transOper: cepstral transform operator
% - nwin: window length
% - novlp: number of overlapping samples
% - nfft: length of the FFT
%
% Output argument:
% - obj_out: modified SpeechAudio object
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, computeformants, Fomrants, transform
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 3; nwin = 1024; end
if nargin < 4; novlp = nwin*3/4; end
if nargin < 5; isSmooth = false; end

if nwin < 1024
    nwin = 1024;
    novlp = nwin*3/4;
end

sr_in = obj.sampling_frequency;
sr = 16e3;
if sr_in ~= sr
    obj.speechresample(sr);
end

x = obj.signal;
nord = size(transOper, 1);
% frequency_transform = fxx; 
% clear fxx

nfft = nwin;
L = nfft/2+1;

Sxx_in = obj.computecepstrumenvelope(nwin, novlp);
if isSmooth
    Sxx_Smooth = zeros(size(Sxx_in));
    nFrame = size(Sxx_in, 2);
    for k = 1:nFrame
        SxxTmp = Sxx_in(:,k);
        Sxx_Smooth(:,k) = 10.^smooth(log10(abs(SxxTmp)), 501);
    end
    clear SxxTmp
else
    Sxx_Smooth = 1;
end
Sxx_Red = 20*log10(abs(Sxx_in./Sxx_Smooth));
% meanRed = 10.^mean(log10(abs(Sxx_Red)), 1);
% Sxx_Red = Sxx_Red-meanRed;
Sxx_inConj = [ Sxx_Red; conj(flipud(Sxx_Red(2:end-1,:))) ];
clear Sxx_Red
ceps_coeff = real(ifft(Sxx_inConj, nfft, 1, 'symmetric'));
clear Sxx_inConj
ceps_coeff = ceps_coeff(1:nord, :);
ceps_coeff = zscore(ceps_coeff);
cepsT = transOper*ceps_coeff;
cepsT2 = zscore(cepsT);
Sxx_trans = (real(fft(cepsT2, nfft, 1)));
Sxx_trans = (10.^(Sxx_trans(1:L,:)/20).*Sxx_Smooth);

Sxx_in = (real(fft(ceps_coeff, nfft, 1)));
Sxx_in = (10.^(Sxx_in(1:L,:)/20).*Sxx_Smooth);

clear ceps_coeff cepsT cepsT2 Sxx_Smooth

x_out = zeros(size(x));
xbuff = buffer(x, nwin, novlp, 'nodelay');
nhop = nwin-novlp;
L = nfft/2+1;
w = hann(nwin);

nFrame = size(xbuff, 2);

for k = 1:nFrame
    deb = (k-1)*nhop+1;
    fin = deb + nwin - 1;
    if fin > length(x_out)
        x_out = [ x_out; zeros(nwin,1) ];
    end
    idx = deb:fin;
    xwin = xbuff(:,k).*w;
    fft_xwin = fft(xwin, nfft); 
    fft_xwin = fft_xwin(1:L);
    h_orig = Sxx_in(:,k);
    h_new = Sxx_trans(:,k);
    fft_new = fft_xwin.*(abs(h_new))./(abs(h_orig));
    x_new = real(ifft([fft_new; conj(flipud(fft_new(2:end-1))) ], nfft, 'symmetric'));
    x_out(deb:fin) = x_out(idx) + x_new(1:nwin);
end

x_out = x_out(1:length(x)) / 1.5;

if nargout == 1
    obj_out = SpeechAudio('signal', x_out, ...
        'sampling_frequency', sr);
    if sr_in ~= sr
        obj_out.speechresample(sr_in);
    end
else
    obj.signal = x_out;
    obj.sampling_frequency = sr;
    if sr_in ~= sr
        obj.speechresample(sr_in);
    end
end

end

