classdef SpeechAudio < handle
% SpeechAudio: class used to perform audio manipulation
% 
% Properties of SpeechAudio objects are:
% - signal: audio signal vector
% - sampling_frequency
% - phonetic_labels: list of phonemes corresponding to the speech signal,
% if segmented at the phone label
% - phonetic_instants: time instants of the onset and offsets of the phonemes. Must be the same size as phonetic_labels
% - time_vector: time vector of the signal
% - spectrum_frequency_vector: frequency vector associated to the spectrum
% of the speech signal
% - spectrum: spectrum of the signal
% - spectrogram_frequency_vector: frequency vector associated to the
% spectrogram of the speech signal
% spectrogram_time_vector: time vector associated to the spectrogram of the speech signal
% spectrogram: spectrogram of the speech signal
% gross_fundamental_frequency: gross estimate of the fundamental frequency
% gross_f0_time_vector: time vector associated to the gross estimate of the
% fundamental frequency
% instantaneous_fundamental_frequency
% instantaneous_periodic_amplitude
% maximal_voiced_frequency: MVF estimate (maximal frequency for which
% glottal activiy is detected)
% voiced_signal: VoicedSignal object estimated from voice/noise separation
% unvoiced_signal: UnvoicedSignal object estimated from voice/noise separation
% spectral_centroid
% spectral_spread
% skewness
% kurtosis    
% spectral_envelope
% envelope_time_vector
% envelope_frequency_vector
% formants: formant object containing the formant parameters
% zeros: formant object continaing the zeros parameters
% hnr: harmonic-to-noise ratio, in dB
% voicing_quotient: voicing quotient, in %. 
% phonemes: cell array containing the phonemes stored as Phoneme objects
%
% Methods of SpeechAudio objects are
% - computespectrogram: compute the spectrogram
% - plotspectrogram: plot the spectrogram
% - computespectrum: compute the spectrum
% - plotspectrum: plot the spectrum
% - computespectralmoments: compute the spectral moments
% - xglos: runs X-GLOS for voice/noise separation
% computespectralenvelope: compute the spectral envelope at different time
% frames
% - plotenvelope: plot the spectral envelopes as a spectrogram
% - addformants: transform the signal by applying a filter defined by
% tf_add at each time frame
% - formantshifting: modify signal so that formants match the target
% parameters
% - changetilt: change spectral tilt by modification of the sinusoidal
% amplitudes of the voiced signal
% - manualsegment: manual segmentation to divide the original SpeechAudio
% objects into several SpeechAudio objects. Segmentation is done by moue
% clicking on spectrogram
% - playspeech: play the speech signal using soundsc
% - savespeech: save speech signal into audio file
% - yin: performs yin to estimate the gross fundamental frequency
% - alignread: read alignment file (textgrid format)
% - simulmicdisto: simulates microphone distortion for particular phonemes
% (requires a phonetic segmentation)
% - segmentspeech: divides the SpeechAudio objects onto several Phonemes
% objects according to the phonetic segmentation
% - backgroundnoise: add background noise at specific SNR
%
% Benjamin Elie, 2020
%
% See also Phoneme, VoicedSignal, UnvoicedSignal, Speaker
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    
    properties
        %%% Input        
        signal = [];
        sampling_frequency = 1;
        phonetic_labels = [];
        phonetic_instants = [];
    end
    
    properties (Hidden = true)
        time_vector = [];
        spectrum_frequency_vector = [];
        spectrum = [];
        spectrogram = [];
        gross_fundamental_frequency = [];
        gross_f0_time_vector = [];
        instantaneous_fundamental_frequency = [];
        instantaneous_periodic_amplitude = [];
        maximal_voiced_frequency = [];
        voiced_signal = [];
        unvoiced_signal = [];
        spectral_centroid = [];
        spectral_spread = [];
        skewness = [];
        kurtosis = [];      
        spectral_envelope = [];
        formants = [];
        zeros = [];
        hnr = [];
        voicing_quotient = [];
        phonemes = [];
    end
    
    methods
         % Constructor method
        function obj = SpeechAudio(varargin)
            for k = 1:2:length(varargin)
                field = varargin{k};
                argvalue = varargin{k+1};
                if isprop(obj,field)
                    obj.(field) = argvalue;
                end % if isprop
            end % for k ...
        end % end function
        
        [Sxx, fxx, txx ] = computespectrogram(obj, win, novlp, nfft)
        [Sxx, fxx ] = computespectrum(obj, win, nfft, whole)
        [cgs, spread ] = computespectralmoments(obj, win, novlp, nfft)
        [Sh, Sn, F0, harm, mvfint] = xglos(obj, param)
        [ Sxx, fxx, txx ] = computespectralenvelope(obj, nwin, novlp, nfft, order)
        [ Sxx, fxx, txx ] = computecepstrumenvelope(obj, nwin, novlp, nfft, order)
        obj_out = addformants(obj, tf_add, nwin, novlp, nfft)
        obj_out = changetilt(obj, tilt_factor, param)
        signal_out = manualsegment(obj)
        playspeech(obj, varargin)
        savespeech(obj, fileName, norm)
        [ f0, timeFrame ] = yin(obj, prm)
        [ labels, instants ] = alignread(obj, fileName)
        obj_out = simulmicdisto(obj, phonemes, coeff, dist_thresh)
        phonemes = segmentspeech(obj, tiers, delay)
        [xOut, backNoise] = backgroundnoise(obj, Pxx, targetSNR)
        [ VQ, HNR ] = computeVQ(obj, nwin, novlp, nfft, meth)
        speechresample(obj, sro)    
        formantObject = formantestimate(obj, nwin, novlp, lpc_order, isSmooth)
        obj_out = formantshifting(obj, formTarget, nwin, novlp, nfft)
        obj_out = cepstraltransform(obj, transOper, nwin, novlp, isSmooth)
        [ formantObject, zeroObject ] = formantzeroestimate(obj, nwin, novlp, pole_order, zero_order, isSmooth)
        sliceplot(obj, dyn)
        exportsegment2json(obj, jsonFileName)
    end
end

