function sliceplot(obj, dyn)
% sliceplot(obj, dyn)
% plot spectrogram of SpeechAudio objects at specific slices
%
% Input arguments:
% - obj: SpeechAudio object
% - dyn: dynamic range for z-axis, in dB (default is 100 dB)
%
% Output argument:
% - fig_handle: figure object
%
% Benjamin Elie, 2020
%
% See also TimeFreq, SpeechAudio, computespectrogram, computespectralenvelope, tfplot
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; dyn = 100; end

set(0,'defaulttextinterpreter','latex')
set(0,'defaultAxesFontSize',20)

% initialization
sr = obj.sampling_frequency;
x = obj.signal;
nfft = 2^nextpow2(sr);
preemph = [1 0.63];
L = nfft/2+1;
freq_out = (0:L-1)*sr/nfft;

% narrowband specrogram
nwinNB = 64;
novlpNB = nwinNB*3/4;

% wideband specrogram
nwinWB = 1024;
w = hamming(nwinWB);

if isempty(obj.time_vector)
    obj.time_vector = (0:length(obj.signal)-1)/obj.sampling_frequency;
end

[ Sxx, fsp, tsp ] = spectrogram(x, hann(nwinNB), novlpNB, nfft, sr);

Pxx = abs(Sxx).^2;
max_Pxx = max(abs(Pxx(:)));

isContinue = true;
 
while isContinue

    fig_handle = figure;
    h(1) = subplot(3,1,[ 1 2 ]);
    imagesc(tsp, fsp, db(Pxx)); axis xy
    colormap(1-gray)
    set(gca,'ticklabelinterpreter','latex')
    xlabel('Time (s)')
    ylabel('Frequency (Hz)')
    caxis([db(max_Pxx)-dyn db(max_Pxx)])
    hold on;
    if ~isempty(obj.phonetic_labels)
        plot_phonetic_segment(obj.phonetic_labels{1}, ...
            obj.phonetic_instants{1}, fsp(end));
    end
    ylim([0 fsp(end) ])
    % colorbar
    h(2) = subplot(313);
    plot(obj.time_vector, obj.signal, 'linewidth',2);
    set(gca,'ticklabelinterpreter','latex')
    xlabel('Time (s)')
    ylabel('Signal')
    hold on;
    if ~isempty(obj.phonetic_labels)
        plot_phonetic_segment(obj.phonetic_labels{1}, ...
            obj.phonetic_instants{1}, max(obj.signal));
    end
    ylim([min(obj.signal), max(obj.signal) ])
    linkaxes(h, 'x')
    xlim([0 max(obj.time_vector)])
    set(gcf, 'units','normalized','position',[ 0 0 1 1 ])


    [ x_in, dumVar ] = ginput(1);
    idx = find(obj.time_vector >= x_in, 1, 'first');
    if ~isempty(idx)
        % narrowband
        idxStart = max(idx-nwinNB/2, 1);
        idxStop = min(idx+nwinNB/2-1, length(x));
        if idxStart == 1
            idxStop = nwinNB;
        end
        if idxStop == length(x)
            idxStart = length(x)-nwinNB+1;
        end
        xwin = x(idxStart:idxStop).*hann(nwinNB);
        nbSpectrum = fft(xwin, nfft);
        nbSpectrum = nbSpectrum(1:L);
        
        % wideband
        idxStart = max(idx-nwinWB/2, 1);
        idxStop = min(idx+nwinWB/2-1, length(x));
        if idxStart == 1
            idxStop = nwinWB;
        end
        if idxStop == length(x)
            idxStart = length(x)-nwinWB+1;
        end
        xwin = x(idxStart:idxStop).*hann(nwinWB);
        wbSpectrum = fft(xwin, nfft);
        wbSpectrum = wbSpectrum(1:L);
        xwin_filt = filter(1,preemph,xwin);
        lpc_xx = lpc(xwin_filt, floor(sr/1000+2));
        spectrEnvelope = freqz(1, lpc_xx, L);
        
        maxfigure = max([max(abs(nbSpectrum)), ...
            max(abs(wbSpectrum)), ...
            max(abs(spectrEnvelope))]);
        
        figure;
        plot(freq_out, db(wbSpectrum), 'linewidth',2); hold on;
        plot(freq_out, db(nbSpectrum), 'linewidth',2);
        plot(freq_out, db(spectrEnvelope), 'linewidth',2);
        set(gca,'ticklabelinterpreter','latex')
        ylabel('Spectrum (dB)')
        xlabel('Frequency (Hz)')
        ylim([ db(maxfigure)-dyn db(maxfigure) ])
        xlim([0 max(freq_out) ])
        leg = legend('Wide band spectrum', 'Narrow band spectrum', 'LPC spectral envelope','location', 'best');
        set(leg, 'interpreter', 'latex');
        checkCont = questdlg('Another slice?', ...
                         'Continue', ...
                         'Yes', 'No', 'Yes');
                   
        if strcmpi(checkCont, 'no')
            isContinue = false;
        end
    end
end
end

function plot_phonetic_segment(label, instant, ylim)

    for k = 1:length(label)
        lblTmp = label{k};
        lblTmp = strrep(lblTmp,'#','\#');
        lblTmp = strrep(lblTmp,'_','\_');
        lblTmp = strrep(lblTmp,'~','\~');        
        lblTmp = strrep(lblTmp,'%','\%');
        plot(instant(k,2)*ones(2,1), [ -1e10 1e10 ], '--','color',[ 0 0.498 0 ],'linewidth',1)
        text(mean(instant(k,:)), ylim, lblTmp, ...
            'color',[ 0 0.498 0 ],'fontsize',14,'fontweight','bold',...
            'horizontalalignment','center','verticalalignment','top')
    end
end