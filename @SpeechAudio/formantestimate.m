function formantObject = formantestimate(obj, nwin, novlp, lpc_order, isSmooth)
% formantObject = formantestimate(obj, nwin, novlp, lpc_order)
% Estimates formant parameters of the SpeechAudio object as LPC roots
%
% Input arguments:
% - obj: SpeechAudio object
% - nwin: window length
% - novlp: number of overlapping samples
% - lpc_order: order of the LPC
% - isSmooth: if true (default), rearrange results to get true trajectories 
%
% Output argument:
% - formantObject: output formant object
%
% Benjamin Elie, 2020
%
% See also Formants, formantpath
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

sr = obj.sampling_frequency;
x = obj.signal;

if nargin < 2; nwin = 256; end
if nargin < 3; novlp = nwin*3/4; end
if nargin < 4; lpc_order = sr/1000+2; end
if nargin < 5; isSmooth = true; end

nhop = nwin-novlp;
xbuff = buffer(x, nwin, novlp, 'nodelay');
w = hamming(nwin);
preemph = [1 0.63];

nFrame = size(xbuff,2);
txx = (0:nhop:length(x))/sr;
txx = txx(1:nFrame);

nfMax = floor(lpc_order/2-1);
form_out = nan(nfMax,nFrame);
bw_out = nan(nfMax,nFrame);

for k = 1:nFrame
    xwin = xbuff(:,k).*w;
    xwin = filter(1,preemph,xwin);
    lpc_xx = lpc(xwin, lpc_order);
    z = roots(lpc_xx); 
    fk = angle(z)/2/pi*sr;    
    bw = -1/2*(sr/(2*pi))*log(abs(z));
    [ formants, idxSort ] = sort(fk(fk > 90 & fk < sr/2-10));
    bw = bw(idxSort);
    nfTmp = length(formants);
    bw = bw(idxSort);
    if nfTmp >= nfMax
        formants = formants(1:nfMax);
        bw = bw(1:nfMax);
        nfTmp = nfMax;
    end
    form_out(1:nfTmp,k) = formants;   
    bw_out(1:nfTmp,k) = bw;    
end
damp = bw_out.*pi;

formantObject = Formants('frequency', form_out, 'bandwidth', bw_out, ...
    'damping', damp, 'amplitude', ones(size(form_out)), ...
    'time_vector', txx, ...
    'parent', obj, ...
    'isForm', true);

if isSmooth
%     disp('Smoothing formant path')
    formantObject.formantpath;
%     disp('Formant path smoothing is done')
end

obj.formants = formantObject;

end

