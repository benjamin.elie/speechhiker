function [ formantObject, zeroObject ] = formantzeroestimate(obj, nwin, novlp, pole_order, zero_order, isSmooth)
% formantObject = formantzeroestimate(obj, nwin, novlp, pole_order, zero_order, isSmooth)
% Estimates formant and zero parameters of the SpeechAudio object as roots
% of an ARMA model
%
% Input arguments:
% - obj: SpeechAudio object
% - nwin: window length
% - novlp: number of overlapping samples
% - pole_order: order of the autoregressive model
% - zero_order: order of the moving average model
% - isSmooth: if true (default), rearrange results to get true trajectories 
%
% Output arguments:
% - formantObject: output Formant object containing formant parameters
% - zeroObject: output Formant object containing antiformant parameters
%
% Benjamin Elie, 2020
%
% See also Formants, formantpath
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

sr = obj.sampling_frequency;
x = obj.signal;

if nargin < 2; nwin = 256; end
if nargin < 3; novlp = nwin*3/4; end
if nargin < 4; pole_order = sr/1000+2; end
if nargin < 5; zero_order = sr/2000+2; end
if nargin < 6; isSmooth = true; end


nhop = nwin-novlp;
xbuff = buffer(x, nwin, novlp, 'nodelay');
w = hamming(nwin);
preemph = [1 0.63];

nFrame = size(xbuff,2);
txx = (0:nhop:length(x))/sr;
txx = txx(1:nFrame);

nfMax = floor(pole_order/2-1);
nzMax = floor(zero_order/2-1);

form_out = nan(nfMax,nFrame);
pbw_out = nan(nfMax,nFrame);

zero_out = nan(nzMax,nFrame);
zbw_out = nan(nzMax,nFrame);

for k = 1:nFrame
    xwin = xbuff(:,k).*w;
    xwin = filter(1,preemph,xwin);
    xdata = iddata(xwin, [], 1./sr);
    sys = armax(xdata, [pole_order, zero_order]);
    ar_coeff = sys.A;
    ma_coeff = sys.C;
    p = roots(ar_coeff);     
    fk = angle(p)/2/pi*sr;    
    pbw = -1/2*(sr/(2*pi))*log(abs(p));
    [ formants, idxSort ] = sort(fk(fk > 0 & fk < sr-10));
    nfTmp = length(formants);
    pbw = pbw(idxSort);
    if nfTmp >= nfMax
        formants = formants(1:nfMax);
        pbw = pbw(1:nfMax);
        nfTmp = nfMax;
    end
    
    form_out(1:nfTmp,k) = formants;   
    pbw_out(1:nfTmp,k) = pbw; 
    
    z = roots(ma_coeff);     
    zfk = angle(z)/2/pi*sr;    
    zbw = -1/2*(sr/(2*pi))*log(abs(z));
    [ zeros_f, idxSort ] = sort(zfk(zfk > 0 & zfk < sr-10));
    nzTmp = length(zeros_f);
    zbw = zbw(idxSort);
    if nzTmp >= nzMax
        zeros_f = zeros_f(1:nzMax);
        zbw = zbw(1:nzMax);
        nzTmp = nzMax;        
    end    
    zero_out(1:nzTmp,k) = zeros_f;   
    zbw_out(1:nzTmp,k) = zbw;
    
end
pdamp = pbw_out.*pi;
zdamp = zbw_out.*pi;

formantObject = Formants('frequency', form_out, 'bandwidth', pbw_out, ...
    'damping', pdamp, 'amplitude', ones(size(form_out)), ...
    'time_vector', txx, ...
    'parent', obj, ...
    'isForm', true);
zeroObject = Formants('frequency', zero_out, 'bandwidth', zbw_out, ...
    'damping', zdamp, 'amplitude', ones(size(zero_out)), ...
    'time_vector', txx, ...
    'parent', obj, ...
    'isForm', false);

if isSmooth
    formantObject.formantpath;
    zeroObject.formantpath;
end

obj.formants = formantObject;
obj.zeros = zeroObject;

end

