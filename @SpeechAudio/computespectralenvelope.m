function [ Sxx, fxx, txx ] = computespectralenvelope(obj, nwin, novlp, nfft, order)
% [ Sxx, fxx, txx ] = computespectralenvelope(obj, nwin, novlp, nfft, order)
% Compute the spectral envelope of the SpeechAudio object at different time
% frames using LPC.
%
% Input arguments:
% - obj: SpeechAudio object
% - nwin: window length
% - novlp: number of overlapping samples
% - nfft: length of the FFT
% - order: order of the LPC
%
% Output argument:
% - Sxx: Complex sdpectral envelope matrix
% - fxx: frequency vector
% - txx: time vector
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, plotspectralenvelope
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; nwin = 512; end
if nargin < 3; novlp = nwin*3/4; end
if nargin < 4; nfft = nwin; end
if nargin < 5; order = obj.sampling_frequency/1000+2; end

x = obj.signal;
sr = obj.sampling_frequency;
nhop = nwin-novlp;

xbuff = buffer(x, nwin, novlp, 'nodelay');
w = hamming(nwin);
preemph = [1 0.63];
L = nfft/2+1;
fxx = (0:L-1)*sr/nfft;

nFrame = size(xbuff,2);
txx = (0:nhop:length(x))/sr;
txx = txx(1:nFrame) + (nwin/2)/sr;

Sxx = zeros(L, nFrame);

for k = 1:nFrame
    xwin = xbuff(:,k).*w;
    xwin = filter(1,preemph,xwin);
    lpc_xx = lpc(xwin, order);
    h_orig = freqz(1, lpc_xx, L);
    Sxx(:,k) = h_orig;
end

outputEnvelope = TimeFreq('parent', obj, ...
    'time_frequency_matrix', Sxx, ...
    'time_vector', txx, ...
    'frequency_vector', fxx, ...
    'window', w, ...
    'overlap', novlp, ...
    'nfft', nfft);

obj.spectral_envelope = outputEnvelope;

