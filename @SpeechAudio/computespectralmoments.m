function [cgs, spread ] = computespectralmoments(obj, win, novlp, nfft)
% [cgs, spread ] = computespectralmoments(obj, win, novlp, nfft)
% Compute the first spectral moments at different time frames using the
% spectrogram
%
% Input arguments:
% - obj: SpeechAudio object
% - nwin: window length
% - novlp: number of overlapping samples
% - nfft: length of the FFT
%
% Output argument:
% - cgs: soectral centroid
% - spread: spectral spread
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, computespectrogram
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; win = hann(256); end
if nargin < 3; novlp = len(win)*3/4; end
if nargin < 4; nfft = length(win); end

if obj.sampling_frequency == 1
    warning('Warning: the sampling frequency is 1');
end    

if isempty(obj.spectrogram)
    warning('Warning: spectrogram is empty, running computespectrogram with default values')
    obj.computespectrogram(win, novlp, nfft);
end

f = obj.spectrogram_frequency_vector;
Sxx = abs(obj.spectrogram).^2;
nFreq, nFrame = size(Sxx);
cgs = zeros(nFrame,1);
spread = zeros(nFrame, 1);
sumSxx = sum(Sxx, 1);

for k = 1:nFrame
    Sxxtmp = Sxx(:,k);
    cgs(k) = sum(Sxxtmp.*f(:))./sumSxx(k);
    spread(k) = sqrt(sum((f(:)-cgs(k)).^2.*Sxxtmp)./sumSxx(k);
end

obj.spectral_centroid = cgs;
obj.spectral_spread = spread;

end

