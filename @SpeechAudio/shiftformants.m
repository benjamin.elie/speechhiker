function obj_out = shiftformants(obj, formant_transform, nwin, novlp, nfft, lpc_order)
% obj_out = shiftformants(obj, formant_transform, nwin, novlp, nfft, lpc_order)
% move formant frequencies
% 
% Input argument:
% - obj: SpeechAudio object
% - formant_transform: if scalar, shift all formant frequencies by this
% factor. If formant_transform is a matrix, apply linear transformation
% y=Ax, where y are the new formant frequencies, A is the matrix
% formant_transform, and x are the initial formant frequencies.
% - nwin: window length
% - novlp: number of overlapping samples
% - nfft: length of the FFT
% - lpc_order: order of the LPC
%
% Output argument:
% - obj_out: modified SpeechAudio object
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, computespectralenvelope
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


if nargin < 3; nwin = 256; end
if nargin < 4; novlp = nwin*3/4; end
if nargin < 5; nfft = nwin; end
if nargin < 6; lpc_order = obj.sampling_frequency/1000 +2; end

x = obj.signal;
x_out = zeros(size(x));
sr = obj.sampling_frequency;

xbuff = buffer(x, nwin, novlp, 'nodelay');
nhop = nwin-novlp;
w = hann(nwin);
wh = hamming(nwin);
preemph = [1 0.63];
L = nfft/2+1;
dampCoef = 2;

for k = 1:size(xbuff,2)
    deb = (k-1)*nhop+1;
    fin = min(deb + nwin - 1, length(x_out));
    idx = deb:fin;
    xwin = xbuff(:,k).*w;
    fft_xwin = fft(xwin, nfft); 
    fft_xwin = fft_xwin(1:L);

    xwh = xbuff(:,k).*wh;
    x1 = filter(1,preemph,xwh);
    lpc_xx = lpc(x1, lpc_order);
    z = roots(lpc_xx); 
    fk = angle(z)/2/pi*sr;
    [ fk_sort, idx_sort ] = sort(fk);
    z = z(idx_sort);
    ind2change = find(abs(fk_sort) > 0 & abs(fk_sort) < sr/2 -10);
    
    if max(size(formant_transform)) == 1
        z_module = abs(z(ind2change));
        z_angle = exp(1i*angle(z(ind2change)));
        z(ind2change) = (z_module.^dampCoef).*(z_angle.^formant_transform);
    else
        nForm = size(formant_transform, 1);  
        if nForm > length(ind2change)/2
            nForm = length(ind2change)/2;            
            formant_transform = formant_transform(1:nForm, [1:nForm size(formant_transform, 2)]);
        end
        fk2change = abs(fk_sort(ind2change));
        [ fk2change_sort, ind2change_sort ] = sort(fk2change);
        f_pos = fk2change_sort(1:2:2*nForm);
        form_vec = [ f_pos(:); 1 ];
        new_formant = formant_transform*form_vec;
        ratio_f = new_formant./f_pos;
        ratio_f = reshape(repmat(ratio_f(:), 1, 2).', 2*numel(ratio_f), 1);
        z_module = abs(z(ind2change(ind2change_sort(1:2*nForm))));
        z_angle = exp(1i*angle(z(ind2change(ind2change_sort(1:2*nForm)))));
        z(ind2change(ind2change_sort(1:2*nForm))) = ((z_module.^dampCoef).*(z_angle.^ratio_f));  
    end
    [ ~, lpc_new ] = zp2tf(zeros(size(z)), z, 1);
    
    h_orig = freqz(1, lpc_xx, L);
    h_new = freqz(1, lpc_new, L);
    
    fft_new = fft_xwin.*abs(h_new)./abs(h_orig);
    x_new = real(ifft([fft_new; conj(flipud(fft_new(2:end))) ], nfft, 'symmetric'));
    new_end = min(nwin, length(idx));
    x_out(deb:fin) = x_out(idx) + x_new(1:new_end);

end

x_out = x_out / 1.5;

if nargout < 1
    obj.signal = x_out;
else
    obj_out = SpeechAudio('signal', x_out, ...
        'sampling_frequency', obj.sampling_frequency);
end
