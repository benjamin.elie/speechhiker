function phonemes = segmentspeech(obj, tiers, delay)
% phonemes = segmentspeech(obj)
% segment SpeechAudio object into an array of phoneme objects
% 
% Input argument:
% - obj: SpeechAudio object
% - tiers: tiers level onto which the segmentation should be done
% - delay: time interval to add before and after speech segments (optional)
%
% Output argument:
% - phonemes: array of Phoneme objects
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, Phoneme
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


if isempty(obj.phonetic_labels)
    error('Speech audio object is not segmented, please perform segmentation')
end

if nargin < 2; tiers = 1; end
if nargin < 3; delay = 0; end


ph_label = obj.phonetic_labels{tiers};
ph_ist = obj.phonetic_instants{tiers};
x = obj.signal;
sr = obj.sampling_frequency;
f0 = obj.gross_fundamental_frequency;
tf0 = obj.gross_f0_time_vector;
formants = obj.formants;

for k = 1:length(ph_label)   
    phon_tmp = Phoneme('phoneme_label',ph_label{k});
    phon_tmp.getPhonemeInformation;
    phon_tmp.sampling_frequency = sr;
    if iscell(ph_ist)
        inst_tmp = ph_ist{k};
    else
        inst_tmp = ph_ist(k,:); 
    end
    
    if delay > 0
        inst_tmp(1) = inst_tmp(1) - delay;
        inst_tmp(2) = inst_tmp(2) + delay;
    end
    % cut signal    
    idx_sig = round(inst_tmp(1)*sr):round(inst_tmp(2)*sr);
    idx_sig(idx_sig < 1) = 1;
    idx_sig(idx_sig > length(x)) = length(x);
    phon_tmp.signal = x(unique(idx_sig));
    
    if ~isempty(f0)
        % cut f0
        idx_f0 = find(tf0 >= inst_tmp(1) & tf0 <= inst_tmp(2));
        phon_tmp.gross_fundamental_frequency = f0(idx_f0);
        phon_tmp.gross_f0_time_vector = tf0(idx_f0);
    end
    
    if ~isempty(formants)
        % cut formants
        tform = formants.time_vector;
        idx_form = find(tform >= inst_tmp(1) & tform <= inst_tmp(2));
        phon_tmp.formants = Formants('frequency', formants.frequency(:,idx_form), ...
            'bandwidth', formants.bandwidth(:,idx_form), ...
            'damping', formants.bandwidth(:,idx_form), ...
            'amplitude', formants.amplitude(:,idx_form), ...
            'time_vector', formants.time_vector(idx_form));     
    end    
    phon_tmp.phonetic_labels{1} = {ph_label{k}};
    phon_tmp.phonetic_instants{1} = reshape(inst_tmp - inst_tmp(1), [], 2);
    phonemes(k) = phon_tmp;
    
end    
    
for k = 1:length(phonemes)
    switch k
        case 1
            phonemes(k).following_phoneme = phonemes(k+1);
        case length(phonemes)
            phonemes(k).previous_phoneme = phonemes(k-1);
        otherwise
            phonemes(k).previous_phoneme = phonemes(k-1);
            phonemes(k).following_phoneme = phonemes(k+1);
    end    
end

end
