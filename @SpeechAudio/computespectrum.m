function [Sxx, fxx ] = computespectrum(obj, win, nfft, whole)
% [Sxx, fxx ] = computespectrum(obj, win, nfft, whole)
% Compute the spectrum of the SpeechAudio object
%
% Input arguments:
% - obj: SpeechAudio object
% - win: window type (rect, hann, or hamming), default is rect
% - nfft: length of the FFT
% - whole: if true, returns the FFT between 0 and sr, else between 0 and
% sr/2
%
% Output argument:
% - Sxx: Complex sdpectral envelope matrix
% - fxx: frequency vector
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, plotspectrum
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; win = 'rect'; end
if nargin < 3; nfft = length(obj.signal); end
if nargin < 4; whole = false; end 

switch win
    case 'rect'
        w = ones(size(obj.signal));
    case 'hann'
        w = hann(length(obj.signal));
    case 'hamming'
        w = hamming(length(obj.signal));
    otherwise
        warning('Warning: the window type is badly defined. Spectrum is computed with default rectangular window');
        w = ones(size(obj.signal));
end


Sxx = fft(obj.signal(:).*w(:), nfft);
fxx = (0:nfft-1)*obj.sampling_frequency/nfft;

if ~whole
    L = nfft/2+1;
    fxx = fxx(1:L);
    Sxx = Sxx(1:L);
end
outputSpectrum = FreqSig('parent', obj, ...
    'frequency_bins', Sxx, ...
    'frequency_vector', fxx, ...
    'window', w, ...
    'nfft', nfft);

obj.spectrum = outputSpectrum;
end

