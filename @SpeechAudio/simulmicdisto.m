function obj_out = simulmicdisto(obj, phonemes, coeff, dist_thresh)
% obj_out = simulmicdisto(obj, phonemes, coeff, dist_thresh)
% simulates distortion due to microphone proximity
%
% Input arguments:
% - obj: SpeechAudio object
% - phonemes: labels of the phonemes in which mic distortion apply
% - coeff: coefficient by which the signal is multiplied in the modified
% phoneme segments. Must be the same length as phonemes
% - dist_thresh: threshold beyond which overload is applied
%
% Output argument:
% - obj_out: modified SpeechAudio object
%
% Benjamin Elie, 2020
%
% See also SpeechAudio
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    if isempty(obj.phonetic_labels)
        error('No phonetic segmentation. Please perform speech-text alignment')
    end
    if nargin < 4; dist_thresh = 1.2; end
    labels = obj.phonetic_labels{1};
    instants = obj.phonetic_instants{1};

    x = obj.signal;
    sr = obj.sampling_frequency;  

%     maxsig = max(abs(x))*dist_thresh;
    x_modif = x;
    
    for k = 1:length(phonemes)    
        x_modif = speechmodif(x_modif, sr, phonemes{k}, coeff(k), labels, instants);
    end
%     x_modif(x_modif>maxsig) = maxsig;
%     x_modif(x_modif<-maxsig) = -maxsig;    
    
    obj_out = SpeechAudio('signal', x_modif, ...
        'sampling_frequency', sr, ...
        'phonetic_labels', obj.phonetic_labels, ...
        'phonetic_instants', obj.phonetic_instants);

end

function y = speechmodif(x, sr, phoneme, modif_coeff, labels, instants)
% 

    envelope = ones(size(x));
    idx = find(strcmp(labels, phoneme) | strcmp(labels, [phoneme, '_']));   
    switch phoneme
        case {'p','k','t'}
            isBurst = true;
            isFric = false;
        case {'f','v'}
            isBurst = false;
            isFric = true;
        otherwise
            isBurst = false;
            isFric = false;
    end
    if isempty(idx)
        warning('No phoneme found')
    end
    
    for k = 1:length(idx)    
        idxtmp = idx(k);        
        if k ~=length(idx)
            if idx(k+1) == idxtmp+1
                idxtmp = [ idxtmp; idxtmp+1 ];
                k = k+1;
            end
        end
        p_instant = instants(idxtmp,:);
        idx_p = round(p_instant(:,1)*sr):round(p_instant(:,2)*sr);
        tr_dur = 0.01;
        tr_size = floor(tr_dur*sr);
        if mod(tr_size,2)
            tr_size = tr_size-1;
        end
        win = hann(tr_size);
        winup = win(1:tr_size/2);
        windown = win(tr_size/2:end);
        if isBurst
%             try
%                 disp('Burst enhancement working')
%                 yTmp = modifburst(x(idx_p), sr, modif_coeff);
%                 x(idx_p) = yTmp;
%             catch
%                 disp('Burst enhancement did not work')
                envelope(idx_p) = modif_coeff*ones(size(idx_p));
                rise_start = idx_p(1)-tr_size/4;
                rise_end = idx_p(1)-tr_size/4+length(winup)-1;
                envelope(rise_start:rise_end) = 1+(modif_coeff-1)*winup;

                fall_start = idx_p(end)-tr_size/4;
                fall_end = idx_p(end)-tr_size/4+length(windown)-1;
                envelope(fall_start:fall_end) = 1+(modif_coeff-1)*windown;
%             end
        else
            envelope(idx_p) = modif_coeff*ones(size(idx_p));
            rise_start = idx_p(1)-tr_size/4;
            rise_end = idx_p(1)-tr_size/4+length(winup)-1;
            envelope(rise_start:rise_end) = 1+(modif_coeff-1)*winup;

            fall_start = idx_p(end)-tr_size/4;
            fall_end = idx_p(end)-tr_size/4+length(windown)-1;
            envelope(fall_start:fall_end) = 1+(modif_coeff-1)*windown;
            if isFric  
                nwin = 512;
                xTmp = x(rise_start:fall_end); 
                xTmp2 = x(:);
                xTmp2(rise_start:fall_end) = 0;
                xTmp = [ zeros(nwin/2,1); xTmp(:); zeros(nwin/2, 1) ];                
                yTmp = modiffric(xTmp, nwin, nwin*3/4, [ 1 -0.9 ]);                
                x(rise_start-nwin/2:fall_end+nwin/2) = xTmp2(rise_start-nwin/2:fall_end+nwin/2) + yTmp(:);
            end
        end
    end    
    y = x(:).*envelope(:);
end

function y = modifburst(x, sr, modif_coeff)

[~, idxMax] = (max(abs(x)));
xres = Esprit(x(idxMax:end), 20);
figure; plot(x(idxMax:end)); hold on; plot(xres)
xres = xres*modif_coeff;
tRise = 0.001;
nRise = round(tRise*sr);
if idxMax-nRise < 1
    nRise = idxMax-1;
end
w = hann(2*nRise);
w = w(1:end/2);
nW = length(w);
xM = xres(1);
xM1 = x(idxMax-1);
rapp = abs(xM/xM1);
y = x;
idxStart = max(idxMax-nW, 1);
idxStop = idxMax-1;
y(idxStart:idxStop) = x(idxStart:idxStop).*(1+rapp*w);
y(idxMax:end) = xres;

end

function xres = Esprit( x, K )
% estimate the frequency of the damped sinusoids that model x
M = min(100, floor(length(x)/2));
% [ ~, R ] = corrmtx(x, M, 'covariance');
R = hankel4esprit( x, M ); % Hankel matrix
[U,~,~] = svd(R); % Singular value decomposition
Up = U(2:end, 1:K);
Um = U(1:end-1, 1:K);
Phi = pinv(Um) * Up; % Spectral matrix
z = eig(Phi);   % Eigenvalue decomposition, z are the poles that model the signal
z(abs(z) > 1) = [];
Vm = exp((0:length(x)-1).'*log(z(:)).');
amp = Vm\x(:);
xres = 2*real(Vm*amp(:));
end

function Hkl = hankel4esprit( sp, Ntot )
% Create Hankel matrix for Esprit
Nl = length(sp) - Ntot +1 ;
Nt = fix(Nl/Ntot);
Hkl = zeros(Ntot);

for k=1:Nt
    deb = (k-1)*Ntot+1;
    fin = deb+2*Ntot-1;
    if fin > length(sp)
        sp = [ sp(:); zeros(fin-length(sp),1) ];
    end
    x = sp(deb:fin);
    H = hankel(x(1:Ntot),x(Ntot:end));
    Hkl = Hkl + H*H';
end

deb = Nt*Ntot+1;
x = sp(deb:end);
H = hankel(x(1:Ntot),x(Ntot:end));
Hkl = Hkl + H*H';

end

function y = modiffric(x, nwin, novlp, filt_coeff)

xbuff = buffer(x, nwin, novlp, 'nodelay');
nhop = nwin-novlp;

nFrame = size(xbuff, 2);
y = zeros(size(x));
w = hann(nwin);

for k = 1:nFrame
    deb = (k-1)*nhop+1;
    fin = deb + nwin - 1;
    if fin > length(y)
        y = [ y; zeros(nwin,1) ];
    end
    idx = deb:fin;
    xwin = xbuff(:,k).*w;
    x_new = filter(1,filt_coeff, xwin);
    x_new = x_new*(std(xwin)/std(x_new));
	y(idx) = y(idx) + x_new;
end
y = y(1:length(x))/1.5;
end



