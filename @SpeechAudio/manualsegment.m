function signal_out = manualsegment(obj)
% signal_out = manualsegment(obj)
% manual segmentation of the SpeechAudio object. Divide the SpeechAudio
% onto several SpeechAudio objects
%
% Input argument:
% - obj: SpeechAudio object

% Output argument:
% - signal_out: array containing the segmented SpeechAudio objects
%
% Benjamin Elie, 2020
%
% See also SpeechAudio
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

sig = obj.signal;
sr = obj.sampling_frequency;

signal_out = SpeechAudio;


htmp = figure; 
plot(sig);
set(gcf, 'units', 'normalized', 'position', [ 0 0 1 1 ])

[x, ~] = ginput;
x = floor(x);
x(x<1) = 1;
x(x>length(sig)) = length(sig);
x = sort(x, 'ascend');

if mod(length(x), 2)
    if x(end) < length(sig)
        x(end+1) = length(sig);
    else
        x(end) = [];
    end
end

numIter = 1;
for k = 1:2:length(x)
   xtmp = sig(x(k):x(k+1));
   signal_out(numIter) = SpeechAudio('signal', xtmp, ...
       'sampling_frequency', sr);
   numIter = numIter + 1;
end

close (htmp)

end

