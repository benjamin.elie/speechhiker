function exportsegment2json(obj, jsonFileName)
% exportsegment2json(obj, jsonFileName)
% export segmentation into JSON file
%
% Input arguments:
% - obj: VT_Network object
% - jsonFileName: name of the segmentation JSON file (.json extension not
% required)
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, alignread
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if isempty(obj.phonetic_labels)
    error('No segmentation: cannot export empty data');
end

nTiers = length(obj.phonetic_labels);
tiers(nTiers) = struct('events', struct('segment_name',[], ...
    'segment_start', [], 'segment_stop', []));

for k = 1:nTiers
    lbl = obj.phonetic_labels{k};
    inst = obj.phonetic_instants{k};
    nEvents = length(lbl);
    events = struct('segment_name', lbl, 'segment_start', [], 'segment_stop', []);
    for kE = 1:nEvents
        if iscell(inst)
            inst_tmp = inst{kE};
        else
            inst_tmp = inst(kE, :);
        end
        events(kE).segment_start = inst_tmp(1);
        events(kE).segment_stop = inst_tmp(2);
    end
    tiers(k).events = events;        
end
id = struct('tiers', tiers);
txt = jsonencode(id);
fileJSON = [ jsonFileName, '.json' ];
fid = fopen(fileJSON,'w');
fwrite(fid,txt,'char');
fclose(fid);

end

