function [ transFun, freq ] = computetransferfunction(obj, param, meth, loc)
% [ transFun, freq ] = computetransferfunction(obj, param, loc)
% Compute the transfer function and the impulse response of the vocal tract
% defined by area function Avt and lvt. The resonance frequencies are
% estimated via ESPRIT. They are the pole frequencies of the impulse
% response of the vocal tract. The constant values are stocked in
% param.
% The transfer function is computed using the chain paradigm by Sondhi and
% Schroeter (A hybrid time-frequency domain articulatory speech
% synthesizer, IEEE TASSP, 1987).
%
%
% Input arguments:
% - obj: VT_Waveguide object to which we want to get the transfer function
% - param : acoustic constant terms
% - loc: location of the source (if 1: glottis-to-lips transfer function)
%
% Output arguments:
% - transFun : complex transfer function of the waveguide object
% - freq : vector containing the frequency points, in Hz, for which the TF has been computed
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide, computeformants, formants2af
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    if nargin < 2; param = []; end
    if ~isfield(param,'rho'); param.rho = 1.204/1000; end %Volumic Mass of the air; %Volumic Mass of the air
    if ~isfield(param,'c'); param.c = 343.4; end %Sound celerity in the air
    if ~isfield(param,'mu'); param.mu = 1.9831e-5; end
    if ~isfield(param,'a'); param.a = 130*pi; end
    if ~isfield(param,'b'); param.b = (30*pi)^2; end
    if ~isfield(param,'c1'); param.c1 = 4; end
    if ~isfield(param,'wo2'); param.wo2 = (406*pi)^2; end
    if ~isfield(param,'c1n'); param.c1n = 72; end
    if ~isfield(param,'heat_cond'); param.heat_cond = 0.0034; end
    if ~isfield(param,'specific_heat'); param.specific_heat = 160; end
    if ~isfield(param,'adiabatic'); param.adiabatic = 1.4; end
    if ~isfield(param,'wallyield'); param.wallyield = true; end
    if ~isfield(param,'loss'); param.loss = true; end
    if ~isfield(param,'freq'); param.freq = 0:50:5000; end %Vector of frequencies
    if nargin < 3; meth = 'tmm'; end
    if nargin < 4; loc = 1; end

    param.freq(param.freq<=1e-11) = 1e-11;
    freq = param.freq(:);
    af = obj.area_function;
    lf = obj.length_function;
    w = 2*pi*freq; % angular frequency
    lw = length(w);
    
    if strcmpi(meth, 'cmp')
        param.alp = sqrt(1i*w*param.c1);
        param.bet = 1i*w*param.wo2./((1i*w+param.a)*1i.*w+param.b)+param.alp;
        param.gam = sqrt((param.alp+1i*w)./(param.bet+1i*w));
        param.sig = param.gam.*(param.bet+1i*w);    
    end
  
    nframe = size(af,2);
    transFun = zeros(lw, nframe);
    Hf_cstr = zeros(lw, nframe); 
    
    if length(loc) < 1 && length(loc) > 1
        error('constriction location does not match the number of frame')
    end
    if length(loc) == 1 && nframe > 1
        loc = loc * ones(nframe, 1);
    end
    
    for kf = 1:nframe
        Avt = af(:,kf);
        lvt = lf(:,kf);
        
        % Radiation impedance (Flanagan, Speech Analysis, Synthesis and Perception, 2nd edition, 1972)
        Zrad = (param.rho*(2*pi*freq).^2/2/pi/param.c + ...
            1i*8*param.rho*(2*pi*freq)/3/pi^(3/2)*Avt(end)^(-1/2));
        [ A, ~, C ] = ChainMatrix(Avt, lvt, freq, param, meth);        

        Hf = 1./(-C.*Zrad(:)+A);
%         Hfm1 = Hf.*(A1-C1.*Zrad(:));
        transFun(:,kf) = Hf;
        
        if loc(kf) > 1 % Transfer function at the constriction point
            if ~isfield(param,'lg'); param.lg = 0.03; end
            if ~isfield(param,'Ag0'); param.Ag0 = 0.4e-4; end
            Zg = 12*param.mu*param.lg^3/param.Ag0^3+0.875*param.rho/2/param.Ag0^2+1i*freq*2*pi*param.rho*param.lg/param.Ag0;
            k_tmp = loc(kf);
            aup = Avt(k_tmp:-1:1);
            adown = Avt(k_tmp+1:end);            
            lup = lvt(k_tmp:-1:1);
            ldown = lvt(k_tmp+1:end);
            [ Aup, Bup, Cup, Dup ] = ChainMatrix(aup, lup, freq, param, meth);
            [ Adown, Bdown, Cdown, Ddown ] = ChainMatrix(adown, ldown, freq, param, meth);
            Tf_front = 1./(Cdown.*Zrad(:)+Ddown);
            Z_front = (Adown.*Zrad(:)+Bdown)./(Cdown.*Zrad(:)+Ddown);
            Z_back = (Aup.*Zg(:)+Bup)./(Cup.*Zg(:)+Dup);
            Hf_cstr(:,kf) = Tf_front(:).*Z_back(:)./(Z_front(:)+Z_back(:));
        end
        
    end % for kf = 1:nframe
    obj.transfer_function = transFun;
    if norm(Hf_cstr(:)) > 0
        obj.transfer_function_constriction = Hf_cstr;
    end
%     obj.transfer_functionm1 = Hfm1;
    obj.radiation_impedance = Zrad;
    obj.freq = freq;
%     obj.input_impedance = 
end % end function