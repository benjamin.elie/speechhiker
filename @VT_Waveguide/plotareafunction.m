function h_out = plotareafunction(obj, sl, hf)
% h_out = plotareafunction(obj, sl, hf)
% plot the area function of the VT_Waveguide object, at specific time
% frames, and in the specified figure object
% 
% Input arguments:
% - obj: VT_Waveguide object
% - sl: time frames for which we want to plot the area function
% - hf: figure object in which we want to plot
%
% Output argument:
% - figure object in which we plotted the area function
% 
% Benjamin Elie, 2020
%
% See also VT_Waveguide, setareafunction
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    if nargin < 2; sl = []; end
    if nargin < 3; hf = []; end
    a = obj.area_function;
    l = obj.length_function;
    if ~isempty(sl)
        a = a(:,sl);
        l = l(:,sl);
    end
    if isempty(hf)
        h_out = figure;
    else
        h_out = figure(hf);
    end
    plot([0; cumsum(l)], [a(1)*1e4; a*1e4],'linewidth',2); hold on;
    set(gca,'fontsize',20)
    set(gca, 'ticklabelinterpreter','latex')
    xlabel('Distance from glottis (m)','interpreter','latex')
    ylabel('Area (cm\textsuperscript{2})','interpreter','latex')
    set(gcf, 'units','normalized','position', [0 0 1 1 ])
end       