function h_out = plottransferfunction(obj, sl, hf)
% h_out = plottransferfunction(obj, sl, hf)
% plot the transfer function of the Waveguide object, at specific time
% frames, and in the specified figure object
% 
% Input arguments:
% - obj: VT_Waveguide object
% - sl: time frames for which we want to plot the transfer function
% - hf: figure object in which we want to plot
%
% Output argument:
% - figure object in which we plotted the transfer function
% 
% Benjamin Elie, 2020
%
% See also VT_Waveguide, computetransferfunction
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    if nargin < 2; sl = []; end
    if nargin < 3; hf = []; end
    if isempty(obj.transfer_function) 
            warning('No transfer function computed for this waveguide! Please compute the transfer function first');
            return        
    else
        tf = obj.transfer_function;
        freq = obj.freq;
    end % if there is a TF

    if isempty(hf)
        h_out = figure;
    else
        h_out = figure(hf);
    end % if new figure
    
    if ~isempty(sl) || size(tf,2) < 2
        if ~isempty(sl); tf = tf(:,sl); end
        semilogy(freq, abs(tf),'linewidth',2); hold on;
        set(gca,'fontsize',20)
        set(gca, 'ticklabelinterpreter','latex')
        xlabel('Frequency (Hz)','interpreter','latex')
        ylabel('Transfer function','interpreter','latex')
        set(gcf, 'units','normalized','position', [0 0 1 1 ])
    else
        imagesc(1:size(tf,2), freq, abs(tf)); hold on; axis xy
        set(gca,'fontsize',20)
        set(gca, 'ticklabelinterpreter','latex')
        ylabel('Frequency (Hz)','interpreter','latex')
        xlabel('Frame','interpreter','latex')
        set(gcf, 'units','normalized','position', [0 0 1 1 ])
    end % if slice
end % end function