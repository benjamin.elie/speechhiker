function exportaf2json(obj, fileName, vpo, vArea)
% exportaf2json(obj, fileName)
% export the area function of the VT_Waveguide object into a JSON file
%
% Input arguments:
% - obj: VT_Waveguide object
% - fileName: name of the JSON file 
% - vpo: velopharyngeal port area
% - vArea: velum area
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 3; vpo = 0; end
if nargin < 4; vArea = 0; end

af = obj.area_function;
lf = obj.length_function;

[ nTubes, nFrame ] = size(af);

% Check file extension
idx = strfind(fileName, '.json');
if ~isempty(idx)
    fileName = fileName(1:idx-1);
end

if length(vpo) == 1
    vpo = vpo*ones(nFrame, 1);
end

if length(vArea) == 1
    vArea = vArea*ones(nTubes, nFrame);
end

if min(size(vArea)) == 1
    vArea = repmat(vArea(:), 1, nFrame);
end

if size(vpo) ~= size(af)
    error('vpo has not the same size as the area function')
end

for kF = 1:nFrame
    if nFrame > 1
        fTmp = [ fileName, '_', num2str(kF,'%.5d'), '.json' ];
    else
        fTmp = [ fileName,'.json' ];
    end
    id = struct('VelumOpeningInCm',vpo(kF)*1e2,'frameId',fTmp,'tubes',[]);
    for kT = 1:nTubes
        id.tubes(kT).area = af(kT, kF)*1e4;
        id.tubes(kT).velumArea = vArea(kT, kF)*1e4;
        id.tubes(kT).x = lf(kT, kF)*1e2;
    end
    txt = jsonencode(id);
    fid = fopen(fTmp,'w');
    fwrite(fid,txt,'char');
    fclose(fid);
end

end

