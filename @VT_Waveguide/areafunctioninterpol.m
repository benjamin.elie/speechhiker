function [ x_out, y_out ] = areafunctioninterpol( obj, nTubes )
% [ x_out, y_out ] = areafunctioninterpol( obj, nTubes )
% Interpolate area function into a vector y_out of nTubes evenly spaced area
% function.
% 
% Input arguments:
% - obj: VT_Waveguide object
% - nTubes : number of desired subsections
%
% Output arguments:
% - x_out : output length function
% - y_out : output area function
% 
% Benjamin Elie, 2020
%
% See also VT_Waveguide, setareafunction
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

af = obj.area_function;
lf = obj.length_function;

nFrame = size(af,2);

cumx_in = cumsum(lf,1);
x_out = zeros(nTubes, nFrame);
y_out = zeros(nTubes, nFrame);
for kframe = 1:nFrame
    y_in = af(:,kframe);
    x_in = cumx_in(:,kframe);

    x_out1 = (1:nTubes)*x_in(end)/nTubes;
    y_out(:,kframe) = interp1(x_in, y_in, x_out1, 'nearest','extrap');
    x_out(:,kframe) = [ x_out1(1), diff(x_out1) ];
    
end

obj.area_function = y_out;
obj.length_function = x_out;

end

