function [newaf, newlf, isCont] = changejawopening(obj, newangle, orig)
% [newaf, newlf] = changejawopening(obj, newangle, orig)
% modify the area function of obj to simulate a modification of the jaw
% opening
%
% Input arguments:
% - obj: VT_Waveguide object
% - newangle: angle of the desired jaw opening
% - orig: location of the tubelet around which we operate the rotation
%
% Output arguments:
% - newaf: modified area function
% - newlf: modified length function
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

[ nTubes, nFrames ] = size(obj.area_function);
[lf, af ] = obj.areafunctioninterpol(nTubes);
% 
% af = obj.area_function;
% lf = obj.length_function;

xf = cumsum(lf, 1);

af2modif = af(orig:end,:);
xf2modif = xf(orig:end,:);

orientation = sign(af(end,:)-af(orig,:));
d_wall = sqrt((af(end,:)-af(orig,:)).^2 + (xf(end,:)-xf(orig,:)).^2);
d_tract = xf(end,:)- xf(orig,:);
current_angle = orientation*acos(d_tract./d_wall);

anglechange = newangle-current_angle;
x_orig = xf2modif(1);
y_orig = af2modif(1);

newxf2change = (xf2modif-x_orig)*cos(anglechange) - (af2modif-y_orig)*sin(anglechange);
newaf2change = (xf2modif-x_orig)*sin(anglechange) + (af2modif-y_orig)*cos(anglechange);

af_unintp = af;
xf_unintp = xf;

af_unintp(orig:end,:) = newaf2change+y_orig;
xf_unintp(orig:end,:) = newxf2change+x_orig;
newlength = xf_unintp(end,:);
newlf = repmat(newlength/nTubes, nTubes, nFrames);
newx = cumsum(newlf, 1);

newaf = interp1(xf_unintp, af_unintp, newx, 'linear', 'extrap');
isCont = false;

if any(newaf <= 1e-11)
    warning('Contact detected!')
    newaf(newaf<=1e-11) = 1e-11;
    isCont = true;
end

obj.area_function = newaf;
obj.length_function = newlf;

end

