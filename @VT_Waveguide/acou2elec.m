function [ Lj, Rj, Cj, Gw, wl, wc, Udj, bj, Hj, Rcm, Rcp ] = acou2elec( obj, Const, ujm1, aglot )
% [ Lj, Rj, Cj, Gw, wl, wc, Udj, bj, Hj, Rcm, Rcp ] = acou2elec( obj, Const, ujm1, aglot )
% Computes the electric components values of the VT_Waveguide object
%
% Input arguments:
% obj: VT_Waveguide object
% Const: various physical constants and parameters
% ujm1: acoustic flow in the waveguide at the previous iteration
% aglot: glottal area
% 
% Output arguments:
% Electric components of the waveguide
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide, VT_Network, SubGlottalTract, MainOralTract 
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 3; ujm1 = 0; end
if nargin < 4; aglot = 0; end

Xj = obj.length_function;
Aj = obj.area_function;

if isfield(Const,'k_iter')
    Xj = Xj(:,Const.k_iter);
    Aj = Aj(:,Const.k_iter);
end

[ nTube, nFrame ] = size(Aj);
A_lip = Aj(end,:);
rho = Const.rho;
co_mu = Const.mu;
c = Const.c;
T = Const.T;
rmlt = Const.rmlt;
wall_resi = Const.wr;
wall_mass = Const.wm;
wall_comp = Const.wc;
rad = sqrt(Aj/pi);
aprec = diff([aglot;Aj(:)]);
asucc = sign(diff([Aj(:);1e10]));

Gw = zeros(size(Xj));
Udj = zeros(size(Xj));
Rcm = zeros(size(Xj));
Rcp = zeros(size(Xj));
if Const.dynterm
    Rcm(aprec<0) = abs(ujm1(aprec<0))*Const.rho./(2*Aj(aprec<0).^2);
    Rcp(asucc<0) = -abs(ujm1(find(asucc<0)+1))*Const.rho./(2*Aj(asucc<0).^2);
end

Lj = rho*Xj/2./Aj;
Rj = rmlt*pi*co_mu.*Xj./Aj.^2;
Cj = Xj.*Aj/(rho*c^2);
Srad = T/2*Const.S*pi*sqrt(pi)/rho;
Grad = Const.G*pi^2/rho/c;
if ~isa(obj,'SubGlottalTract')
    wr = wall_resi/(2*sqrt(pi))./Xj./sqrt(Aj);
    wl = 2/T*wall_mass/(2*sqrt(pi))./Xj./sqrt(Aj);
    wc = T/2./(wall_comp/(2*sqrt(pi))./Xj./sqrt(Aj));
else
    ltrach = sum(Aj);
    xtrach = cumsum(Aj)/ltrach;
    wr = 0.33./(1.30.^(xtrach/4))*1.6e3./(2*pi*(rad.^3).*Xj);
    wl = 0.33./(1.30.^(xtrach/4))*1.1e-3./(2*pi*rad.*Xj);
    wc = (2*pi*(rad.^3).*Xj)./(0.33./(1.30.^(xtrach/4))*0.392e6);
end

if Const.wallyield == 1; Gw = 1./(wr+wl+wc); end

%%%% Compute linear system coefficients
bj = zeros(nTube+1, nFrame);
Hj = zeros(nTube+1, nFrame);
bj(1:nTube,:) = 1./(2*Cj/T+Gw);
Hj(2:nTube,:) = -2*(Lj(1:nTube-1,:)+Lj(2:nTube,:))/T ... 
    -Rj(1:nTube-1,:)-Rj(2:nTube,:)-Rcm(1:nTube-1,:)-Rcp(2:nTube,:) ... 
    -bj(1:nTube-1,:)-bj(2:nTube,:);
bj(nTube+1,:) = 1./(Srad*sqrt(A_lip)+Grad*A_lip);
Hj(nTube+1,:) = -(2/T*Lj(nTube,:)+Rj(nTube,:)+bj(nTube,:)+bj(nTube+1,:)+Rcp(nTube));

obj.Lj = Lj;
obj.Rj = Rj;
obj.Cj = Cj;
obj.Gw = Gw;
obj.wl = wl;
obj.wc = wc;
obj.Udj = Udj;
obj.bj = bj;
obj.Hj = Hj;
obj.Rcm = Rcm;
obj.Rcp = Rcp;
obj.Ns = zeros(nTube+1,nFrame);

end