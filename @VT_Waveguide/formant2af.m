function [ akmat, lkmat, err_form, titer ] = formant2af( obj, formant_target, param )
% [ akmat, lkmat, fnest, Hnest, err_form, titer ] = formant2af( obj, formant_target, param )
% method of VT_Waveguide objects that performs dynamic acoustic-to-area-function inversion from
% formant estimates
% 
% Input arguments:
% obj: VT_Waveguide object to which we want to estimate its true area function
% formant_target: frequencies for which the waveguide should have as
% resonance frequencies. If formant_target is a Matrix, each column
% corresponds to a time frame
% param: various parameters used for the inversion
%
% Output arguments:
% akmat: estimated area function
% lkmat: estimated length function
% fnest: resonance frequencies of the output waveguide
% err_form: vector containing the error in formant frequencies at each
% iteration
% titer: vector containing the computation time at each iteration
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide, computeformants, computetransferfunction
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 3; param = []; end
if ~isfield(param,'rho'); param.rho = 1.204/1000; end % Volumic Mass of the air; %Volumic Mass of the air
if ~isfield(param,'c'); param.c = 343.4; end % Sound celerity in the air
if ~isfield(param,'mu'); param.mu = 1.9831e-5; end
if ~isfield(param,'a'); param.a = 130*pi; end
if ~isfield(param,'b'); param.b = (30*pi)^2; end
if ~isfield(param,'c1'); param.c1 = 4; end
if ~isfield(param,'wo2'); param.wo2 = (406*pi)^2; end
if ~isfield(param,'c1n'); param.c1n = 72; end
if ~isfield(param,'freq'); param.freq = 1:50:5000; end % Vector of frequencies

if ~isfield(param,'length_var'); param.length_var = true; end % compute length variation ?
if ~isfield(param,'lips_var'); param.lips_var = true; end % compute lips variation
if ~isfield(param,'area_weight'); param.area_weight = 0; end
if ~isfield(param,'length_weight'); param.length_weight = 0; end
if ~isfield(param,'potential_weight'); param.potential_weight = 0; end
if ~isfield(param,'threshold'); param.threshold = 1; end
if ~isfield(param,'maxNumIter'); param.maxNumIter = 1e2; end
if ~isfield(param,'psi'); param.psi = 0.5; end
if ~isfield(param,'tf_method'); param.tf_method = 'cmp'; end
if ~isfield(param,'verb'); param.verb = true; end

tic

thresh = param.threshold;
Nbreak = param.maxNumIter;

numFormant = size(formant_target,1); % size of the acoustic vector

% get initial solutions
a0 = obj.area_function;
l0 = obj.length_function;
[ numTube, numFrame ] = size(a0);

% Vector must be column vectors
a0vec = a0(:);
l0vec = l0(:);
ftargvec = formant_target(:);

% Initalization
count_loop = 1;
ak = a0vec; 
lk = l0vec; 
lkp1 = lk;
akp1mat = a0; 
lkp1mat = l0;

err_form = [];
titer = [];

[fnest, fk, err_form, titer] = checkformants(obj, param, ftargvec, err_form, titer, count_loop);
a_tmp = {a0};
l_tmp = {l0};

while (err_form(end) >= thresh && count_loop <= Nbreak) % as long as the distance is higher than thresh
    
    [akp1mat, lkp1mat] = newarea(obj, fk, param, numFrame, numTube, ftargvec, fnest, a0, akp1mat, l0, lkp1mat);
    a_tmp{end+1} = akp1mat;
    l_tmp{end+1} = lkp1mat;
    % Inialization    
    obj.area_function = akp1mat;
    obj.length_function = lkp1mat;
    obj.computetransferfunction(param, param.tf_method);
    count_loop = count_loop + 1;
    [fnest, fk, err_form, titer] = checkformants(obj, param, ftargvec, err_form, titer, count_loop);

end

[~, idx] = min(err_form);
if idx < length(err_form)
    akp1mat = a_tmp{idx};
    lkp1mat = l_tmp{idx};    
    err_form = err_form(1:idx);
    titer = titer(1:idx);
end    

akmat = akp1mat;
lkmat = lkp1mat;
obj.area_function = akmat;
obj.length_function = lkmat;

end

function [fnest, fk, err_form, titer] = checkformants(obj, param, ftargvec, err_form, titer, count_loop)

    obj.computetransferfunction(param, param.tf_method);
    fk = computeformants(obj, param);
    fnest = fk(:);
    err_form(count_loop) = mean(abs(fnest-ftargvec)./fnest)*100; % distance from the target acoustic vector (cost function)
    titer(count_loop) = toc;
    if param.verb
        disp(['Distance = ',num2str(err_form(end))])
    end

end

function [ P, U ] = af2acoustics( obj, freq, param )
% Compute the pressure and air flow inside the vocal and nasal tracts
% defined by area function A and l. The constant values are stocked in
% param. After the chain matrix paradigm by Sondhi and Schroeter (A hybrid time-frequency domain articulatory speech
% synthesizer, IEEE TASSP, 1987).

af = obj.area_function;
lf = obj.length_function;
numTube = size(af,1);
numFrame = size(af,2);
if size(freq,2) == 1 && numFrame > 1
    freq = repmat(freq, 1,numFrame);
end

w = 2*pi*freq; % Angular frequency
alp = sqrt(1i*w*param.c1);
bet = 1i*w*param.wo2./((1i*w+param.a)*1i.*w+param.b)+alp;
gam = sqrt((alp+1i*w)./(bet+1i*w));
sig = gam.*(bet+1i*w);

P = zeros(numTube, numFrame, size(freq,1));
U = P;

for kframe = 1:numFrame

    af_tmp = af(:,kframe);
    lf_tmp = lf(:,kframe); 
    freqtmp = freq(:,kframe);
	sigtmp = sig(:,kframe);
	gamtmp = gam(:,kframe);

    % Radiation impedance (Flanagan, Speech Analysis, Synthesis and Perception, 2nd edition, 1972)
    Zrad = param.rho*(2*pi*freqtmp).^2/2/pi/param.c+1i*8*param.rho*(2*pi*freqtmp)/3/pi^(3/2)*af_tmp(end)^(-1/2);
    P1 = Zrad;
    
    U1 = ones(size(P1));
    for ktube = numTube:-1:1 % Compute the chain matrix
        Argh = sigtmp*lf_tmp(ktube)/param.c;
        A = cosh(Argh);
        B = -param.rho*param.c/af_tmp(ktube)*gamtmp.*sinh(Argh);
        C = -af_tmp(ktube)/param.rho/param.c./gamtmp.*sinh(Argh);
        det_k = 1./(A.^2-B.*C);        
        P1tmp = ((A(:).*P1(:)-B(:).*U1(:))./det_k(:)).';
        U1tmp = ((-C(:).*P1(:)+A(:).*U1(:))./det_k(:)).';
        P(ktube,kframe,:) = P1tmp;
        U(ktube,kframe,:) = U1tmp;
        P1 = P1tmp;
        U1 = U1tmp;
    end

end
obj.pressure_freq = P;
obj.flow_freq = U;

end

function Sn_x  = areasensitivityfunction( obj, freq, param )
% Compute the sensitivity function Sn along the vocal tract
%
% Input arguments :
%
% l : length function of the vocal tract
% Af : Area function of the vocal tract
% fn : formant frequency
% param : constant terms 
%
% Output arguments : 
%
% Sn_x : sensitivity function. Sn_x is N*M matrix where N is the number
% formant frequencies (length of fn), and M is the length of Af
%
% Benjamin Elie, july 2013

% Size of the sensitivity function

af = obj.area_function;
lf = obj.length_function;
% freq = freq(:);
numTube = size(af,1);
numFrame = size(af,2);
numFreq = size(freq,1);

Sn_x = zeros(numTube, numFrame,numFreq*numFrame);
rho = param.rho; 
cs = param.c;

[ P, U ] = af2acoustics( obj, freq, param ); % Compute the pressure and volume velocity inside each tubelets at the resoannce frequencies

for kframe = 1:numFrame
    for kfreq = 1:numFreq
        Pn = squeeze(abs(P(:,kframe,kfreq)).^2);
        Un = squeeze(abs(U(:,kframe,kfreq)).^2);
        Sn_x(:,kframe,(kframe-1)*numFreq+kfreq) = lf(:,kframe).*(rho./af(:,kframe).*Un-af(:,kframe)/rho/cs^2.*Pn)./ ...
            sum(lf(:,kframe).*(rho./af(:,kframe).*Un+af(:,kframe)/rho/cs^2.*Pn));
    end % for kfreq
end % for kframe

obj.areasensitivity = Sn_x;

end

function Sn_x  = lengthsensitivityfunction( obj, freq, param )
% Compute the sensitivity function Sn along the vocal tract for the length
% function
%
% Input arguments :
%
% l : length function of the vocal tract
% Af : Area function of the vocal tract
% fn : formant frequency
% param : constant terms 
%
% Output arguments : 
%
% Sn_x : sensitivity function. Sn_x is N*M matrix where N is the number
% formant frequencies (length of fn), and M is the length of Af
%
% Benjamin Elie, july 2013

af = obj.area_function;
lf = obj.length_function;
% freq = freq(:);
numTube = size(af,1);
numFrame = size(af,2);
numFreq = size(freq,1);

Sn_x = zeros(numTube, numFrame,numFreq*numFrame);
rho = param.rho; 
cs = param.c;

[ P, U ] = af2acoustics( obj, freq, param ); % Compute the pressure and volume velocity inside each tubelets at the resoannce frequencies

for kframe = 1:numFrame
    for kfreq = 1:numFreq  
        Pn = squeeze(abs(P(:,kframe,kfreq)).^2);
        Un = squeeze(abs(U(:,kframe,kfreq)).^2);
        Sn_x(:,kframe,(kframe-1)*numFreq+kfreq) = lf(:,kframe).*(rho./af(:,kframe).*Un+af(:,kframe)/rho/cs^2.*Pn)./ ...
            sum(lf(:,kframe).*(rho./af(:,kframe).*Un+af(:,kframe)/rho/cs^2.*Pn));
    end % for kfreq
end % for kframe

obj.lengthsensitivity = Sn_x;
end

function [akp1mat, lkp1mat] = newarea(obj, fk, param, numFrame, numTube, ftargvec, fnest, a0, akp1mat, l0, lkp1mat)

    ak = akp1mat(:);
    lk = lkp1mat(:);
    if numFrame == 1
        carea = 0; 
        cleng = 0; 
    end % if only one frame, no kinetic energy constraints (static configuration)
    dl = param.length_var;
    norm_lips = true-param.lips_var;
    carea = param.area_weight;
    cleng = param.length_weight;
    cpot = param.potential_weight;    
    param.freq(param.freq<=1e-11) = 1e-11;
    % Acceleration term. psii > 1 => enhance speed, but risk of unstability, psii < 1 to make sure we are in the linearity domain...
    psii = param.psi;
    Sn_x  = areasensitivityfunction( obj, fk, param );

    if dl
        Sn_x_l  = lengthsensitivityfunction( obj, fk, param );
        if numFrame > 1
            Sn_x_l = reshape(Sn_x_l,numFrame*numTube,[]);
        else
            Sn_x_l = squeeze(Sn_x_l);
        end
    end
    if numFrame > 1
        Sn_x = reshape(Sn_x,numFrame*numTube,[]);
    else
        Sn_x = squeeze(Sn_x);
    end

    zn = (ftargvec-fnest)./fnest; % Delta_f vector
    alph = real(zn.'*(Sn_x.')*Sn_x*zn/(((Sn_x.')*Sn_x*zn).'*((Sn_x.')*Sn_x*zn)))*psii;
    if dl
        alphl = real(zn.'*(Sn_x_l.')*Sn_x_l*zn/(((Sn_x_l.')*Sn_x_l*zn).'*((Sn_x_l.')*Sn_x_l*zn)))*psii;
    end

    dak = alph*Sn_x*zn; % perturbation of area function, if unconstrained
    dakD = 0; 
    daikD = 0; % initalization of constraint terms

    if carea ~= 0 % if kinetic energy constraint on area function is enabled    
        dpdamat = zeros(size(akp1mat));
        dpmat = diff(akp1mat,1,2);
        dpdamat(:,1) = -2*dpmat(:,1);
        dpdamat(:,2:end-1) = 2*dpmat(:,1:end-1)-2*dpmat(:,2:end);
        dpdamat(:,end) = 2*dpmat(:,end);
        dpdamat2 = dpdamat.*repmat(sum(abs(dpdamat).^2,1),[numTube,1]);
        dakD = dpdamat2(:);        
    end

    if cpot ~= 0 % if potential energy constraint is enabled        
        daikD_mat = 2*(a0-akp1mat)./a0.*repmat(sum(abs((a0-akp1mat)./a0).^2,1),[numTube,1]);
        daikD = daikD_mat(:);    
    end

    akp1 = abs(ak+diag(ak)*((1-carea)*dak+carea*dakD+cpot*daikD)); % Total perturbation to apply to the previous area function
%     Ind = find(akp1<=0.1e-4); % Limit variation in constriction regions (cf Bunton and Story, Estimation of VT area functions in childre based on measurement of lip termination area and inverse acoustic mapping, ICA 2013)
%     akp1(Ind) = exp(log(ak(Ind)) + log(dak(Ind)+1)); % New area function
    akp1mat = reshape(akp1, numTube, numFrame); % Turn area functions into matrix when there are several temporal frames
    if norm_lips; akp1mat(end,:) = a0(end,:); end % if lip aperture is imposed
    akp1 = akp1mat(:); 
    ak = akp1;

    if dl % Compute the new length function        
        dlkD = 0; dlikD = 0;
        if cleng ~= 0
            dldamat = zeros(size(lkp1mat),'single'); 
            dlmat = diff(cumsum(lkp1mat,1),1,2);
            dldamat(:,1) = -2*dlmat(:,1);
            dldamat(:,2:end-1) = 2*dlmat(:,1:end-1)-2*dlmat(:,2:end);
            dldamat(:,end) = 2*dlmat(:,end);
            dldamat2 = dldamat.*repmat(sum(abs(dldamat).^2,1),[numTube,1]);
            dlkD = dldamat2(:);        
        end

        if cpot ~= 0            
            dlikD_mat = 2*(l0-lkp1mat).*repmat(sum(abs((l0-lkp1mat)).^2,1),[numTube,1]);
            dlikD = dlikD_mat(:);            
        end

        xi_vec = ((1-cleng)*Sn_x_l*zn+cleng*dlkD+cpot*dlikD)*alphl;
        Xi_mat = diag(1./(1+xi_vec));
        lkp1 = abs(Xi_mat*lk(:));
        lkp1(lkp1*40 > 0.2) = 0.2/40; % uncomment to avoid unrealistically long vocal tracts
        lk = lkp1;        
    end

    lkp1mat = reshape(lkp1, numTube, numFrame);

end
