function computenoise(obj, Const, namp, Udc, aglot)
% computenoise(obj, Const, namp, Udc, aglot)
% compute the frication noise along the VT_Waveguide object
%
% Input arguments:
% - obj: VT_Waveguide object
% - Const: some physical constants and parameters
% - Udc: low frequency air flow in the vocal tract
% - aglot: glottal area opening
%
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide, VT_Network, esmfsynthesis
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

k = Const.k_iter;
af = obj.area_function;
if isempty(obj.actualVT)
    obj.actualVT = 1:size(af,1);
end
actVT = obj.actualVT;
actVT2 = [ actVT(:); actVT(end)+1 ];

[ nTube, nFrame ] = size(af);
A2o = [ aglot; af(:,k) ];
[ ac, ic ] = min(af(obj.actualVT,k));
fny = Const.sr/2;
Nsk = zeros(nTube+1, 1);
if namp~=0
    W = obj.noise_matrix;
    dk = 50;
    if k-dk >= 1
        idxStart = k-dk;
    else
        idxStart = 1;
    end
    if k+dk <= nFrame
        idxEnd = k+dk;
    else
        idxEnd = nFrame;
    end
    bb = idxStart:idxEnd;
    if ic >= 38 %%%%%% Frication noise generated at the lips (labiodental constriction)
        hl = Const.hld;                 
    else
        fpeak = min(0.15*sqrt(pi)*Udc/(ac*sqrt(ac)),fny*0.999);%*Const.rho/Const.mu;
        ordl = 9+2*(38-ic-1);
        if strcmp(Const.filt,'fir1')
            hl = fir1(ordl,fpeak/fny,'low');
        end
        if strcmp(Const.filt,'firls'); hl = firls(ordl+1,[0 fpeak/fny 10000/fny 1],[0.7 1 0 0]); end
    end   

    Wb = W(actVT2,bb); 
    Wb = filter(hl,1,Wb, [], 2);
    if ic >= 38; Wb = filter(Const.hld2,1,Wb); end
    Wb = Wb/std(Wb(:));
    phik = namp*(obj.reynolds_inst^2-Const.Rec^2);
    Nsk(actVT2) = phik*(Wb(actVT2,round(length(bb)/2)))./(A2o(actVT2).^2.5)*Udc^3;
    if (isa(obj,'MainOralTract') && Const.dynterm)
        obj.Udj(ic+1) = Const.beta*phik; 
    end
    Nsk(1) = 0;
    if length(Nsk) > length(actVT)+1
        Nsk(length(actVT)+2:end) = Nsk(length(actVT)+2:end)*0;
    end
else
    Nsk = zeros(nTube+1,1); 
end       
obj.Ns = Nsk;