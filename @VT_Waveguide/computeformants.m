function [ fmt, bw, amp ] = computeformants(obj, param)
% [ fmt, bw, amp ] = computeformants(obj, param)
% Computes the resonance frequencies of the VT_Waveguide object.
% Use the ESPRIT method on the impulse response to estimate the poles of
% the transfer function
%
% Input arguments:
% - obj: VT_Waveguide object
% - param: various physical constants and parameters
%
% Output arguments:
% - fmt: resonance frequencies
% - bw: resonance bandwidths
% - amp: resonance complex amplitudes
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide, computetransferfunction, formants2af
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


    if nargin < 2; param = []; end
    if isempty(obj.transfer_function)
        obj.computetransferfunction(param);
    end
    nframe = size(obj.transfer_function, 2);
    if ~isfield(param,'nform'); param.nform = 20; end
%     K = 2*(param.nform+2);
    fmt = nan(param.nform,nframe);
    bw = nan(param.nform,nframe);
    amp = nan(param.nform,nframe);
    for kf = 1:nframe
        Hf = obj.transfer_function(:,kf);
        [~, locs] = findpeaks(db(Hf));
        K = 2*length(locs) + 4;
        if isinf(Hf(1)) || isnan(Hf(1)); Hf(1) = Hf(2); end
        y2 = reshape(Hf,1,length(Hf));
        N = length(Hf);
        df = median(diff(obj.freq)); % frequency step
        y2 = [y2 conj(y2(end:-1:2))];
        x = ifft(y2,'symmetric');
        NIfft = length(x); 
        x = NIfft*x; % amplitude normalization
        Ls = ceil(length(x)/2)+1;
        himp = x(1:Ls);
        himp = himp-mean(himp);
        fs = df*2*(N-1);% sampling frequency   
        [ fk, bw_tmp, amp_tmp ] = Esprit( himp,fs, K ); 
        [fk, bw_tmp, amp_tmp] = rejectpoles(fk, bw_tmp, amp_tmp, [90 fs/2-100]);
        nForm = min(length(fk), floor(K/2));
        fmt(1:nForm,kf) = fk(1:nForm);
        bw(1:nForm,kf) = bw_tmp(1:nForm);
        amp(1:nForm,kf) = amp_tmp(1:nForm);
    end
    obj.formants = fmt;
end

function [ fk, bw, amp ] = Esprit( x,fs, K )
% estimate the frequency of the damped sinusoids that model x
if nargin < 2; fs = 1; end
if nargin < 3; K = 12; end

[ ~, idx ] = max(x);
x = x(idx:end);
x = x-mean(x);

% K = 0;
M = min(100, floor(length(x)/2));
% [ ~, R ] = corrmtx(x, M, 'covariance');
R = HklBD( x, M ); % Hankel matrix
[U, ~, ~ ] = svd(R); % Singular value decomposition
% Kest = ester( U ); % Estimates the number of components. Comment if you want to skip this step (K = 12 by default)
% % K = 12;
% if Kest < K; Kest = K; end % Just to make sure to not underestimate
% K = Kest;
Up = U(2:end, 1:K);
Um = U(1:end-1, 1:K);
Phi = pinv(Um) * Up; % Spectral matrix
z = eig(Phi);   % Eigenvalue decomposition, z are the poles that model the signal
freq = angle(z)/2/pi*fs;
alp = -log(abs(z))*fs;
[ fk, isrt ] = sort(freq(freq>50),'ascend');
bw = alp(isrt)./pi./fk;
z = z(isrt);
Vm = exp((0:length(x)-1).'*log(z(:)).');
amp = Vm\x(:);

end

function [ HH ] = HklBD( sp, Ntot )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
Nl = length(sp) - Ntot +1 ; % nombre de colonnes de la "grande" matrice
Nt = fix(Nl/Ntot); % nombre de blocs pour le calcul de Rss 
                   % les sous matrices sont de taille Ntot x Ntot 

HH = zeros(Ntot);
% h=waitbar(0,'fabrication de la matrice');

for k=1:Nt
    deb = (k-1)*Ntot+1;
    fin = deb+2*Ntot-1;
    if fin > length(sp)
        sp = [ sp(:); zeros(fin-length(sp),1) ];
    end
    x = sp(deb:fin);
    H = hankel(x(1:Ntot),x(Ntot:end));
    HH = HH + H*H';
%     waitbar(k/Nt,h);
end

% le petit bout pour finir
deb = Nt*Ntot+1;
x = sp(deb:end);
H = hankel(x(1:Ntot),x(Ntot:end));
HH = HH + H*H';

end

function [ K, J ] = ester( W )
% ESTER algorithm to estimate the number of poles included in the signal,
% after Badeau and al. (A new perturbation analysis for signal enumeration in rotational invariance techniques, IEEE TASP, 2006)
%
% Input argument :
%
% W : matrix of the signal subspace
%
% Output argument :
%
% K : dimension of the signal subspace
%
% Benjamin Elie, March 2010

% maximal dimension of the signal subspace:
pmax = size(W,2);
nu = W(end,1)';
mu = nu;
Wup = W(2:end,:);Wdown = W(1:end-1,:);

Psi_lr = Wdown(:,1)'*Wup(:,1);

Ksi = Wup(:,1)-Wdown(:,1)*Psi_lr;
GrandKsi = Ksi;
Phi = mu*conj(Psi_lr);
E = GrandKsi - 1/(1-norm(nu)^2)*(Wdown(:,1)*nu)*Phi'; 
J = zeros(1, ceil(pmax/2));
J(1) = 1/norm(E,2)^2;

for p = 2:ceil(pmax/2)
    
    nu_transp = W(end,1:p);
    nu = nu_transp';
    nu_pmoins1 = nu(1:end-1);
    mu = nu(end);
    Psi_r = Wdown(:,1:p-1)'*Wup(:,p);
    Psi_l = Wup(:,1:p-1)'*Wdown(:,p);
    Psi_lr = Wdown(:,p)'*Wup(:,p);
    
    Ksi = Wup(:,p)-Wdown(:,1:p-1)*Psi_r-Wdown(:,p)*Psi_lr;
    GrandKsi = [ GrandKsi - Wdown(:,p)*Psi_l', Ksi];
    
    Phi_pmoins1 = Phi + mu*Psi_l;
    Phi = [Phi_pmoins1 ; Psi_r'*nu_pmoins1 + mu*conj(Psi_lr)];
    
    if norm(nu) ~= 1
        
        E = GrandKsi - 1/(1-norm(nu)^2)*(Wdown(:,1:p)*nu)*Phi' ;
        J(p) = 1/norm(E,2)^2;
   
    else 
        J(p) = 1;
    
    end
    
end

Jp = J(2:2:end); % Real signal : K is pair

thresh_max = max(Jp);
Thresh = thresh_max/10;
        
ind = find(Jp >= Thresh,1,'last'); % find the last value of Jp above the threshold
K = 2*ind; 

end

function [fk, bw, ak] = rejectpoles(fk, bw, ak, flim, bwlim)

    if nargin < 4; flim = [90 4900]; end
    if nargin < 5; bwlim = 0.1; end
    idx = find(fk >= flim(1) & fk <= flim(2) & bw <= bwlim);
    fk = fk(idx);
    bw = bw(idx);
    ak = ak(idx);

end

