classdef VT_Waveguide  < VT_Network
% VT_Waveguide: class modeling an acoustic waveguide. Inheris from
% VT_Network
% 
% Properties of VT_Waveguide objects are:
% - area_function
% - length_function
% - formants: formant frequency
% - formans_bandwidth: bandwidth of formants
% - formants_amplitude: amplitude of formants
% - child_wvg: name of the child waveguide object
% - parent_wvg: name of the parent waveguide object
% - twin_wvg: name of the twin waveguide object
% - anabranch_wvg: name of the anabranch waveguide object
% - child_point: point of connection where the child waveguide is connected
% - parent_point: point of connection of the parent waveguide
% - radiation: Boolean. True if the waveguide radiates (e.g. oral tract).
% - transfer_function
% - transfer_functionm1: transfer function of the waveguide at the
% penultimate tubelet
% - transfer_function_constriction: transfer function at the constriction point
% - radiation_impedance
% - freq : frequency vector of the transfer function
% - chain_matrix
% - chain_matrix_derivative
% - noise_matrix: matrix containing the turbulent noise in each tubelet and
% each time frame
% - flow_time_matrix: matrix containing the flow in the waveguide in each
% tubelet and each time frame
% - pressure_time_matrix: matrix containing the acoustic pressure in the waveguide in each
% tubelet and each time frame 
% Some hidden properties may be important:
% - Zin: imput impedance
% - actualVT: tubelets that correspond to the vocal tract. May be important
% if one wants to study the effect of external objects prolonging the vocal
% tract system, such as an oxygen mask, for instance. If not specified, all
% tubelets are considered as the actual VT
%
% Methods of VT_Waveguide objects are
% - setareafunction: enters the area and length function values
% - areafunctioninterpol: interpolation of the area and length function for
% evenly distributed area functions
% - computetransferfunction: compute the transfer function of the waveguide
% - computeformants: compute the formant characteristics of the waveguide
% - formant2af: estimates the area and length functions of a waveguide from
% formant estimates in a speech signal 
% - plotareafunction: plot the area function
% - plottransferfunction: plot the transfer function
% - changejawopening: modify the area function to simulate different jaw
% openings
% - exportaf2json: write area function of the VT_Waveguide object into a JSON
% file
%
% Benjamin Elie, 2020
%
% See also VT_Network, MainOralTract, GlottalChink, SubGlottalTract
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    properties 
        area_function = []; % area function
        length_function = []; % length function
        formants = []; % formants        
        child_wvg = []; % name of the child waveguide object
        parent_wvg = []; % name of the parent waveguide object
        twin_wvg = []; % name of the twin waveguide object
        anabranch_wvg = []; % name of the anabranch waveguide object
        child_point = []; % point of connection where the child waveguide is connected
        parent_point = []; % point of connection of the parent waveguide
        radiation = []; % Boolean. True if the waveguide radiates (e.g. oral tract). 
    end
    
    properties (Hidden=true)
        %%%% Instant acoustics
        pressure_freq = [];      
        flow_freq = [];
        areasensitivity = [];
        lengthsensitivity = [];   
        norder = 1;  
        pressure_time = [];
        flow_time = [];
        Lj = [];
        Rj = [];
        Cj = [];
        Gw = [];
        wl = [];
        wc = [];
        Udj = [];
        bj = [];
        Hj = [];
        Rcm = [];
        Rcp = [];
        Vc = [];
        Q = [];
        V = [];
        Qwl = [];
        Qwc = [];
        Qnc = [];  
        Ns = [];
        A = [];
        B = [];
        C = [];
        D = [];
        Zin = [];
        actualVT = [];     
        transfer_function = []; % transfer function of the waveguide
        transfer_functionm1 = []; % transfer function of the waveguide
        transfer_function_constriction = []; % transfer function at the constriction point
        radiation_impedance = []; % radiation impedance of the waveguide
        freq = []; % frequency vector of the transfer function
        chain_matrix = [];
        chain_matrix_derivative = [];
         
        %%%%% Output acoustic matrix 
        noise_matrix = [];
        flow_time_matrix = [];
        pressure_time_matrix = [];       
    end

    methods
        
        % Constructor method
        function obj = VT_Waveguide(varargin)
            for k = 1:2:length(varargin)
                field = varargin{k};
                argvalue = varargin{k+1};
                if isprop(obj,field)
                    obj.(field) = argvalue;
                end % if isprop
            end % for k ...
        end % end function

        % Method to set the area function of the waveguide
        setareafunction(obj, area, length);
        
        % Method for interpolating the area function
        [ x_out, y_out ] = areafunctioninterpol( obj, nTubes )
        
        % Method to compute the transfer function of the waveguide
        [ trans_fun, freq ] = computetransferfunction(obj, param, meth, loc);
        
        % Method to compute the formant evolution of the waveguide
        [ fmt, bw, amp ] = computeformants(obj, param);
        
        % Method for acoustic to area function inversion
        [ akmat, lkmat, fnest, Hnest, err_form, titer ] = formant2af( obj, formant_target, param )
        
        % Method for computing the acoustic elements of the tubelets
        [ Lj, Rj, Cj, Srad, Gw, wl, wc, Udj, bj, Hj, Rcm, Rcp ] = acou2elec( obj, Const, ujm1, aglot )
        
        % Method to plot the area function
        h_out = plotareafunction(obj, sl, hf)
        
        % Method to plot the transfer function
        h_out = plottransferfunction(obj, sl, hf)
        
        % Method to initialize the derivation and integration terms
        [ Vc, Q, V, Qwl, Qwc, Qnc ] = initderiv(obj)
        
        % Method to initialize the acoustic outputs
        initacoustics(obj)
        
        % Method to compute noise
        computenoise(obj, Const, namp, Udc, aglot)
        
        % Method to change the jaw opening
        [newaf, newlf, isCont] = changejawopening(obj, newangle, orig)
        
        exportaf2json(obj, fileName, vpo, vArea)


    end  
end