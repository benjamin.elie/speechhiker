function [ Vc, Q, V, Qwl, Qwc, Qnc ] = initderiv(obj)
% [ Vc, Q, V, Qwl, Qwc, Qnc ] = initderiv(obj)
% Initialisation of the derivative and integration terms. Used for
% esmfsynthesis

% Input argument:
% - obj: VT_Waveguide object

% Output arguments:
% - various terms used for esmfsynthesis
% 
% Benjamin Elie, 2020
%
% See also VT_Waveguide, VT_Network, esmfsynthesis
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

nTube = size(obj.area_function,1);
Vc = zeros(nTube,1);
Q = zeros(nTube+1,1);
V = zeros(nTube+1,1);
Qwl = Vc;
Qwc = Vc;
nChild = length(obj.child_wvg);
Qnc = zeros(1,nChild);

obj.Vc = Vc;
obj.Q = Q;
obj.V = V;
obj.Qwl = Qwl;
obj.Qwc = Qwc;
obj.Qnc = Qnc;

end