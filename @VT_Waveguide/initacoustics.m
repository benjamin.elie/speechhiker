function initacoustics(obj)
% initacoustics(obj)
% Initialisation of the acoustic terms for the running speech synthesis
%
% Input argument:
% - obj: VT_Waveguide object
% 
% Benjamin Elie, 2020
%
% See also VT_Waveguide, VT_Network, esmfsynthesis
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

stmp = size(obj.area_function);
nTube = stmp(1);
if length(stmp) == 1
    nFrame = 1;    
else
    nFrame = stmp(2);
end
P = zeros(nTube+1,nFrame);
U = zeros(nTube+1,nFrame);
W = randn(nTube+1,nFrame);

if isa(obj,'GlottalChink')
    P = zeros(1,nFrame);
    U = zeros(1,nFrame);
    obj.glottal_flow_vector = zeros(1,nFrame);
    obj.glottal_flow_inst = 0;
end

if isa(obj,'MainOralTract') 
    obj.glottal_flow_vector = zeros(1,nFrame);
    obj.glottal_flow_inst = 0;
    obj.reynolds_inst = 0;
    obj.reynolds_vector = zeros(1,nFrame);
end

if isa(obj,'SubGlottalTract') 
    obj.connection_pressure_inst = 0;
    obj.connection_pressure_vector = zeros(1,nFrame);   
end

obj.pressure_time_matrix = P;
obj.flow_time_matrix = U;
obj.pressure_time = P(:,1);
obj.flow_time = U(:,1);

if nTube > 40
    W(42:end,:) = W(42:end,:)*0;
end
obj.noise_matrix = W;

end