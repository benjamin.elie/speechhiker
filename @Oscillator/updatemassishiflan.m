function [ yt, ytm1, ytm2 ] = updatemassishiflan( obj, Const )
% [ yt, ytm1, ytm2 ] = updatemassishiflan( obj, Const )
% Update the mass position of the Oscillator object for the 2-mass Ishizaka and Flanagan model. Check original paper [1]
% 
% Input arguments:
% - obj: Oscillator object
% - Const: acoustic constant terms
%
% Output arguments:
% - yt: new mass position (step k)
% - ytm1: previous mass position (step k-1)
% - ytm2: mass position at the step k-2
%
% [1] K. Ishizaka, J. L. Flanagan, Synthesis of voiced sounds from a two-mass
% model of the vocal cords, Bell Syst. Tech. J. 51(6) (1972) 1233–1268.
%
% Benjamin Elie, 2020
%
% See also VT_Network, Oscillator, esmfsynthesis
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

rho = Const.rho;
T = Const.T;

etha_k1 = obj.etha_k1;
etha_k2 = obj.etha_k2;
etha_h1 = obj.etha_h1;
etha_h2 = obj.etha_h2;
xvec = obj.x_position;
lg = obj.length;

ytm1 = obj.mass_position_nm1;
ytm2 = obj.mass_position_nm2;
ago = obj.low_frequency_abduction(Const.k_iter);
U1 = obj.inst_flow;
m1 = obj.mass(2,1); 
m2 = obj.mass(2,2);
k1 = obj.stiffness(2,1);
k2 = obj.stiffness(2,2);
kc = obj.coupling_stiffness(1);
Ps = obj.upstream_pressure;

x0 = xvec(1); x1 = xvec(2); x2 = xvec(3); %x3 = xvec(4);
d1 = x1-x0; d2 = x2-x1;
% h0 = hvec(1); h1i = hvec(2); h2i = hvec(3); h3 = hvec(4);

y1m1 = sum(ytm1(:,1)); 
y2m1 = sum(ytm1(:,2)); 
y1m2 = sum(ytm2(:,1)); 
y2m2 = sum(ytm2(:,2)); 
ag1m1 = y1m1*lg+ago;
ag2m1 = y2m1*lg+ago;
ago1 = ago;
ago2 = ago;
h1 = 3*k1; 
h2 = 3*k2;

% Pressure acting on m1
Pm1 =  Ps - 1.37*rho/2*(U1/(ag1m1))^2 - ...
1/2*( obj.resistance(1)*U1+2*obj.inductance(1)/T*U1-obj.Qu1 );
obj.Qu1 = 4/T*obj.inductance(1)*U1-obj.Qu1;

F1 = d1*lg*Pm1; % Force due to Pm1

% Pressure acting on m2
F2 = d2*lg*( Pm1 - 1/2*(sum(obj.resistance))*U1-2*(sum(obj.inductance))/T*U1+obj.Qu2...
-rho/2*U1^2*( 1/ag2m1^2-1/ag1m1^2 ) );
obj.Qu2 = 4/T*(sum(obj.inductance))*U1-obj.Qu2;

% Damping ratio
r1 = 2*0.1*sqrt(m1.*k1);
r2 = 2*0.6*sqrt(m2.*k2);

% Computation of every possible case.
%1 and 2 open
y1oo = ( m1/T^2*(2*y1m1-y1m2) + ...
    r1/T*y1m1-k1*etha_k1*y1m1^3 -...
    kc*(y1m1-y2m1) + F1)...
    /( m1/T^2+r1/T+k1 );
y2oo = ( m2/T^2*(2*y2m1-y2m2) + ...
    r2/T*y2m1-k2*etha_k2*y2m1^3 -...
    kc*(y2m1-y1m1) + F2)...
    /( m2/T^2+r2/T+k2 );

% 1 open and 2 closed
r2 = 2*1.6*sqrt(m2*k2);
y1of = ( m1/T^2*(2*y1m1-y1m2) + ...
    r1/T*y1m1-k1*etha_k1*y1m1^3 -...
    kc*(y1m1-y2m1) +Ps*d1*lg)...
    /( m1/T^2+r1/T+k1 );
y2of = ( m2/T^2*(2*y2m1-y2m2) + ...
    r2/T*y2m1-k2*etha_k2*y2m1^3 -...
    kc*(y2m1-y1m1) + Ps*d2*lg - ...
    h2*(ago2/2/lg+etha_h2*(y2m1+ago2/2/lg)^3) )...
    /( m2/T^2+r2/T+k2+h2);

% 1 closed and 2 open
r1 = 2*1.1*sqrt(m1*k1);
r2 = 2*0.6*sqrt(m2*k2);
y1fo = ( m1/T^2*(2*y1m1-y1m2) + ...
    r1/T*y1m1-k1*etha_k1*y1m1^3 -...
    kc*(y1m1-y2m1) + Ps*d1*lg - ...
    h1*(ago1/2/lg+etha_h1*(y1m1+ago1/2/lg)^3))...
    /( m1/T^2+r1/T+k1+h1 );
y2fo = ( m2/T^2*(2*y2m1-y2m2) + ...
    r2/T*y2m1-k2*etha_k2*y2m1^3 -...
    kc*(y2m1-y1m1) + 0)...
    /( m2/T^2+r2/T+k2 );

% 1 and 2 closed
r2 = 2*1.6*sqrt(m2*k2);
y1ff = ( m1/T^2*(2*y1m1-y1m2) + ...
    r1/T*y1m1-k1*etha_k1*y1m1^3 -...
    kc*(y1m1-y2m1) + Ps*d1*lg - ...
    h1*(ago1/2/lg+etha_h1*(y1m1+ago1/2/lg)^3))...
    /( m1/T^2+r1/T+k1+h1 );
y2ff = ( m2/T^2*(2*y2m1-y2m2) + ...
    r2/T*y2m1-k2*etha_k2*y2m1^3 -...
    kc*(y2m1-y1m1) + 0 - ...
    h2*(ago2/2/lg+etha_h2*(y2m1+ago2/2/lg)^3) )...
    /( m2/T^2+r2/T+k2+h2);

if y1oo>-ago1/(2*lg)
    if y2oo>-ago2/(2*lg)
        y1 = y1oo;
        y2 = y2oo;
    else
        y1 = y1of;
        y2 = y2of;
    end
else
    if y2fo>-ago2/(2*lg)
        %           1 closed 2 open
        y1 = y1fo;
        y2 = y2fo;
    else
        %           1 closed 2 closed
        y1 = y1ff;
        y2 = y2ff;
    end
end

ytm2 = ytm1;
yt = [y1/2*ones(2,1), y2/2*ones(2,1)];
ytm1 = yt;

obj.mass_position_nm2 = ytm2;
obj.mass_position_nm1 = ytm1;
obj.mass_position = yt;
obj.applied_force = [ F1, F2 ];

end

