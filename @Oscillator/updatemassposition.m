function [ yt, ytm1, ytm2 ] = updatemassposition( obj, Const )
% [ yt, ytm1, ytm2 ] = updatemassposition( obj, Const )
% Update the mass position of the Oscillator object for the smooth 2-mass model Check original paper [1]
% 
% Input arguments:
% - obj: Oscillator object
% - Const: acoustic constant terms
%
% Output arguments:
% - yt: new mass position (step k)
% - ytm1: previous mass position (step k-1)
% - ytm2: mass position at the step k-2
%
% [1] N. J. C. Lous, G. C. J. Hofmans, R. N. J. Veldhuis, A. Hirschberg, A
% symetrical two-mass vocal-fold model coupled to vocal tract and trachea,
% with application to prothesis design, Acta Acustica 84 (1998) 1135–1150.

% Benjamin Elie, 2020
%
% See also VT_Network, Oscillator, esmfsynthesis, appliedforce
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Fvf = obj.applied_force;
fs = 1/Const.T;
kc = obj.coupling_stiffness;
mvf = obj.mass;
ke = obj.stiffness;
re = obj.damping;
ytm1 = obj.mass_position_nm1;
ytm2 = obj.mass_position_nm2;

F1u = sum(Fvf(:,1)); 
F2u = sum(Fvf(:,2)); 
F1d = F1u; 
F2d = F2u;
kcup = kc(1); 
kcdown = kc(2);

% Temporary terms to simplify the system resolution 
tmp1_A = mvf(1,1)*fs^2+ke(1,1)+re(1,1)*fs+kc(1);
tmp1_B = mvf(1,1)*fs^2*(2*ytm1(1,1)-ytm2(1,1))+re(1,1)*fs*ytm1(1,1);
tmp2_A = mvf(1,2)*fs^2+ke(1,2)+re(1,2)*fs+kc(1);
tmp2_B = mvf(1,2)*fs^2*(2*ytm1(1,2)-ytm2(1,2))+re(1,2)*fs*ytm1(1,2);

% Get new position by solving the coupled system for the upper part
yt(1,1) = (F1u+tmp1_B+(kcup/tmp2_A)*(F2u+tmp2_B))/(tmp1_A-(kcup^2)/tmp2_A);
yt(1,2) = (F2u+tmp2_B+(kcup/tmp1_A)*(F1u+tmp1_B))/(tmp2_A-(kcup^2)/tmp1_A);

% The same temporary terms for lower part
tmp1_A = mvf(2,1)*fs^2+ke(2,1)+re(2,1)*fs+kc(2);
tmp1_B = mvf(2,1)*fs^2*(2*ytm1(2,1)-ytm2(2,1))+re(2,1)*fs*ytm1(2,1);
tmp2_A = mvf(2,2)*fs^2+ke(2,2)+re(2,2)*fs+kc(2);
tmp2_B = mvf(2,2)*fs^2*(2*ytm1(2,2)-ytm2(2,2))+re(2,2)*fs*ytm1(2,2);
   
% Get new position by solving the coupled system for the lower part
yt(2,1) = (F1d+tmp1_B+(kcdown/tmp2_A)*(F2d+tmp2_B))/(tmp1_A-(kcdown^2)/tmp2_A);
yt(2,2) = (F2d+tmp2_B+(kcdown/tmp1_A)*(F1d+tmp1_B))/(tmp2_A-(kcdown^2)/tmp1_A);
ytm2 = ytm1;
ytm1 = yt;

obj.mass_position_nm2 = ytm2;
obj.mass_position_nm1 = ytm1;
obj.mass_position = yt;

end
