function getclosureinstant(obj)
% getclosureinstant(obj)
% find the closure instants of the oscillator
% 
% Input argument:
% - obj: Oscillator object
%
% Benjamin Elie, 2020
%
% See also Oscillator
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

obj.max_instant = [];
obj.closure_instant = [];
obj.opening_instant = [];
obj.open_quotient = [];

sig_mass_1 = obj.height_vector_output(2,:);
sig_mass_2 = obj.height_vector_output(3,:);
openarea = min(sig_mass_1, sig_mass_2);
openarea(openarea<=0) = 0;
sig_mass_1(sig_mass_1<=0) = 0;
sig_mass_2(sig_mass_2<=0) = 0;

sig = [ sig_mass_1; sig_mass_2; openarea ];

for ksig = 1:3

    sig_tmp = sig(ksig,:);
    [~, max_inst ] = findpeaks(sig_tmp);
    cl_inst = [];
    op_inst = [];
    oq_inst = [];

    for k = 1:length(max_inst)-1
        x_tmp = sig_tmp(max_inst(k):max_inst(k+1));
        idx_cl = find(x_tmp<=0, 1, 'first') + max_inst(k);
        idx_op = find(x_tmp<=0, 1, 'last') + max_inst(k) + 1;
        if ~isempty(idx_cl)
            cl_inst = [ cl_inst; idx_cl ];
            op_inst = [ op_inst; idx_op ];
            n_open = length(x_tmp)-(idx_op-idx_cl-1);
            oq_inst = [oq_inst; n_open/length(x_tmp)];            
        end
    end

    if max_inst(end) < length(sig_tmp)
        x_tmp = sig_tmp(max_inst(end):end);
        idx_cl = find(x_tmp<=0, 1, 'first') + max_inst(k);
        idx_op = find(x_tmp<=0, 1, 'last') + max_inst(k) + 1;
        if ~isempty(idx_cl)
            cl_inst = [ cl_inst; idx_cl ];
            op_inst = [ op_inst; idx_op ];
        end
    end
    
    obj.max_instant{ksig} = max_inst;
    obj.closure_instant{ksig} = cl_inst;
    obj.opening_instant{ksig} = op_inst;
    obj.open_quotient{ksig} = oq_inst;

end

end

