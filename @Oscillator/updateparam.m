function [ mvfo, kvfo, rvfo, kco ] = updateparam( obj, Const)
% [ mvfo, kvfo, rvfo, kco ] = updateparam( obj, Const)
% Adapt the values of the mass and stiffness of the Oscillator object to match the
% fundamental frequency fo
%
% Input arguments:
% - obj: Oscillator object
% - Const: acoustic constant terms
%
% Output arguments:
% - mvfo: new mass values
% - kvfo: new stiffness values
% - rvfo: new dampin,g values
% - kco: new coupling stiffness values
%
% Benjamin Elie, 2020
%
% See also VT_Network, Oscillator, esmfsynthesis
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

kiter = Const.k_iter;
fo = obj.fundamental_frequency(kiter);
dab = max(0.1,1-obj.partial_abduction(kiter));
mvfi = obj.initial_mass;

if (isinf(fo) || isnan(fo) || fo <= 60 ); fo = 60 ; end
mvfo = mvfi*dab;
kvfo = 2*pi^2*fo^2*mvfo; 
kco = [ kvfo(1,1); kvfo(2,1) ]*obj.Qrc;
rvfo = obj.Qr*sqrt(kvfo.*mvfo/2);

%%%% CHECK for contact
kctc = obj.contact_stiffness;
rctc = obj.contact_damping;
obj.isContact = false;

if (obj.height_vector(2) <= Const.blim)   % contact at the 1st mass location    
    kvfo(:,1) = kctc*kvfo(:,1); % New stiffness for 1st mass
    rvfo(:,1) = rctc*sqrt(kvfo(:,1).*mvfo(:,1)/2);%rvf(:,1)+2*sqrt(kvf(:,1).*mvf(:,1)); % New damping for 1st mass  
    obj.isContact = true;
end

if (obj.height_vector(3) <= Const.blim) % contact at the 2nd mass location    
    kvfo(:,2) = kctc*kvfo(:,2); % New stiffness for 2nd mass
    rvfo(:,2) = rctc*sqrt(kvfo(:,2).*mvfo(:,2)/2);%rvf(:,2)+2*sqrt(kvf(:,2).*mvf(:,2)); % New damping for 2nd mass
    obj.isContact = true;
end

obj.mass = mvfo;
obj.stiffness = kvfo;
obj.coupling_stiffness = kco;
obj.damping = rvfo;

end
