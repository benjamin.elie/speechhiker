classdef Oscillator < handle
% Oscillator: class modeling the glottal and supraglottal oscillators in
% the vocal tract
% 
% Properties of Oscillator objects define its geometry:
% - waveguide: cell array containing the Waveguide objects that model the
% vocal tract nework
% - oscillators: cell array containing the oscillators in the vocal tract (e.g. the glottis, the tongue...)     
% - subglottal_control: vector containing the input value of the subglottal pressure for each
% simulation time step
% - phonetic_label: cell array containing the phonetic labels of the
% desired simulated utterance
% - phonetic_instant: array containing the time instants of the onset and
% offset of each phoneme of the desired simulated utterance
% - abduction: vector containing the low frequency abduction of the glottis
% - pressure_radiated: vector containing the acoustic pressure radiated at
% the lips (and potentially at the nostrils)
% - network_transfer_function: transfer function of the whole system
% - force_vector: vector of instantaenous acoustic forces in the vocal
% tract
% - esmf_matrix: acoustic matrix used to solve the esmf equations
% - flow_inst: instantaneous acoustic flow in the vocal tract
% - input_area: ??
% - radiation_position: indices of waveguides that radiate
% - number_tubes: vector containing the number of tubes of each waveguide
% - isChink: true if a glottal chink is connected to the vocal tract and
% the glottis
% - isSubGlott: true if a subglottal system is connected to the vocal tract
% - isGlott: true if an oscillating glottis is connected to the vocal tract
% - isSupOscill: true if a supraglottal oscillator is connected to the vocal tract
%
% Useful methods of Oscillator objects are
% - outputspectrum: compute the spectrum of the oscillating movement of the
% oscillator
% - getclosureinstant: get the closure instants% 
% - getfeatures: compute some features associated to the oscillating
% movement, sich as the jitter and the shimmer
%
% Benjamin Elie, 2020
%
% See also Network, Tongue, Glottis
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    properties
        %%%%% Input
        model = 'ishi';
        length = [];
        width = [];
        initial_mass = [];
        initial_stiffness = [];
        initial_coupling_stiffness = [];
        initial_damping = [];
        rest_position = [];        
        x_position = [];        
        up_height = [];
        down_height = [];
    end

    properties(Hidden=true)
        
        initial_length = [];
        Qr = [];
        Qrc = [];       
        contact_damping = [];
        contact_stiffness = [];
        fundamental_frequency = [];
        low_frequency_abduction = [];   
        partial_abduction = [];
        
        etha_k1 = [];
        etha_k2 = [];
        etha_h1 = [];
        etha_h2 = [];
        
        % Instantaneous Output
        isContact = [];
        height_vector = [];
        mass = [];
        stiffness = [];
        damping = [];
        coupling_stiffness = [];
        bernoulli = [];
        resistance = [];
        inductance = [];
        separation_height = [];   
        mass_position = [];
        mass_position_nm1 = [];
        mass_position_nm2 = [];        
        inst_flow = [];
        flow_nm1 = 0;
        upstream_pressure = [];
        downstream_pressure = [];
        applied_force = [];
        Qu1 = 0;
        Qu2 = 0;

        % Output matrix
        height_vector_output = [];
        mass_position_matrix = [];
        mass_matrix = [];
        stiffness_matrix = [];
        damping_matrix = [];
        coupling_stiffness_matrix = [];
        output_length = [];
        
        % Analysis
        mass_1_spectrum = [];
        mass_2_spectrum = [];
        open_area_spectrum = [];
        closure_instant = [];
        opening_instant = [];
        max_instant = [];
        open_quotient = [];
    end

    methods
         % Constructor method
        function obj = Oscillator(varargin)
            for k = 1:2:length(varargin)
                field = varargin{k};
                argvalue = varargin{k+1};
                if isprop(obj,field)
                    obj.(field) = argvalue;
                end % if isprop
            end % for k ...
        end % end function
        
        [ Rg2, Rg, Lg, hsi, Udc ] = updateimpedance( obj, Const, aChink, ain, ac, lc)
        
        [ mvfo, kvfo, rvfo, kco ] = updateparam( obj, Const )
        
        [ yt, ytm1, ytm2 ] = updatemassishiflan( obj, Const )
        
        [ yt, ytm1, ytm2 ] = updatemassposition( obj, Const )
        
        [ Fvf, Ugim1 ] = appliedforce( obj, Const )

        [Sxx, fxx] = outputspectrum(obj, win, nfft, sr,  whole)
    
        getclosureinstant(obj)

        [jitt1, shim1] = getfeatures(obj, nPeriode)
        
    end
end

