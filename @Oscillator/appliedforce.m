function [ Fvf, Ugim1 ] = appliedforce( obj, Const )
% [ Fvf, Ugim1 ] = appliedforce( obj, Const )
% returns the force applied on the masses of the Oscillator object
% 
% Input arguments:
% - obj: Oscillator object
% - Const: acoustic constant terms
%
% Output arguments:
% - Fvf: applied force
% - Ugim1: acoustic flow through the oscillator. To be used at the next
% step
%
% Benjamin Elie, 2020
%
% See also VT_Network, Oscillator, esmfsynthesis
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

sep = Const.sep;
T = Const.T;

%%% Oscillator shape
xvec = obj.x_position;
% Hvec = obj.mass_position_output;
Hvec = obj.height_vector;
x0 = xvec(1); x1 = xvec(2); x2 = xvec(3); x3 = xvec(4);
h0 = Hvec(1); h1i = Hvec(2); h2i = Hvec(3); h3 = Hvec(4);

Ps = obj.upstream_pressure;
Psup = obj.supraglottal_pressure;
DeltaPi = Ps-Psup;
Ug = obj.inst_flow;
Ugim1 = obj.flow_nm1;
lg = obj.length;

A(1) = (h1i-h0)/(x1-x0); % slope, part 1
A(2) = (h2i-h1i)/(x2-x1); % slope, part 2
A(3) = (h3-h2i)/(x3-x2); % slope, part 3
B(1) = h0-A(1)*x0;  % y-intercept, part 1
B(2) = h1i-A(2)*x1; % y-intercept, part 2
B(3) = h2i-A(3)*x2; % y-intercept, part 3

%%%% Separation point

if (h1i>0) && (h2i>0) % No contact between masses
    if (h1i~=h2i) % if h1 different than h2
         hsi = sep*h1i; 
         xsi = (hsi-h1i)/A(2)+x1;
    else
         xsi = x2; % if h1 = h2
         hsi = h2i; % separation height is h2
    end
    if (h1i>=h2i) || (hsi>=h2i) %
        hsi = h2i;
        xsi = x2;   % separation point is x2
    end
else    
    hsi = eps;   % if contact, separation is 0
    xsi = eps;   %    
end

if DeltaPi>=0
    dUg_tmp = (Ug-Ugim1)/T;
    Fvf = forcecompute(Ug,lg,dUg_tmp,Ps,Psup,xvec,h1i,h2i,A,B,h0,xsi,hsi, Const);
else
    dUg_tmp = (-Ug-Ugim1)/T;
    Fvf = forcecompute(Ug,lg,dUg_tmp,Psup-Ps,0,xvec,h2i,h1i,A,B,h0,xsi,hsi, Const);
end

Ugim1 = Ug;
obj.flow_nm1 = Ug;
obj.applied_force = Fvf;

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% The following functions are from N. Ruty %%%%%%%%%%%%%%%%%
%%% "N. Ruty. Modèles d'interactions fluide/parois dans le conduit vocal. 
%%% Applications aux voix et aux pathologies. PhD thesis, 2007 %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Fvf = forcecompute(Ug,lg,d_Ug_dt,Ps,Psupra,xvec,h1,h2,A,B,h0,xs,hs,const)
% Computes forces applied in the oscillator constriction

rho = const.rho;
co_mu = const.mu;
sep = const.sep;
Fl_h2 = 0;
Fr_h1 = 0;

x0 = xvec(1);
x1 = xvec(2);
x2 = xvec(3); 
x3 = xvec(4);
pres = 1e-5;

% Some temporary terms
tmp1  = 0.5*rho*(Ug^2)/(lg^2); % Bernoulli
tmp2  = -12*co_mu*Ug/lg; % Poiseuille
tmp3  = -rho*d_Ug_dt/lg; % Inertia

Fl_h1 = 0.5*(x1-x0)*lg*Ps; 

if (h1>0) && (h2>0) % No contact
    
    i1 = 1;
    bern = tmp1*(1/(h0^2)-2*(Xv1(x0,x1,A,B,i1)-x0*Wv1(x0,x1,A,B,i1))/(x1-x0)^2); % Bernoulli 
    pois = tmp2/(2*A(1))*(1/(h0^2)-2*(Xv1(x0,x1,A,B,i1)-x0*Wv1(x0,x1,A,B,i1))/(x1-x0)^2); % Poiseuille
    inst = tmp3/A(1)*(2*(Zv1(x0,x1,A,B,i1)-x0*Yv1(x0,x1,A,B,i1))/(x1-x0)^2-log(h0)); % Inertia   
    Fl_h1 = 0.5*(x1-x0)*lg*(Ps+bern+pois+inst);
    
    if (h2 < h1*sep) 
        if (abs(A(2)*x2/B(2))<pres) % Parrallel duct
            i2 = 2;
            bern = tmp1*(1/(h0^2)+2*(Xv1(x1,x2,A,B,i2)-x2*Wv1(x1,x2,A,B,i2))/(x2-x1)^2); %  Bernoulli
            pois = tmp2*(1/(2*A(1))*(1/(h0^2)-1/(h1^2))-1/B(2)^3*(x1-3/2*A(2)/B(2)*x1^2+2*A(2)^2/B(2)^2*x1^3)...
                -2/(x2-x1)^2/B(2)^3*((x2^3/3-3/8*A(2)/B(2)*x2^4+2/5*A(2)^2/B(2)^2*x2^5-...
                                       x2*(x2^2/2-1/2*A(2)/B(2)*x2^3+1/2*A(2)^2/B(2)^2*x2^4))...
                                       -(x1^3/3-3/8*A(2)/B(2)*x1^4+2/5*A(2)^2/B(2)^2*x1^5-...
                                       x2*(x1^2/2-1/2*A(2)/B(2)*x1^3+1/2*A(2)^2/B(2)^2*x1^4)))); % Poiseuille
            inst = tmp3*(log(h1/h0)/A(1)-1/B(2)*(x1-1/2*A(2)/B(2)*x1^2+1/3*A(2)^2/B(2)^2*x1^3)-...
                2/(x2-x1)^2/B(2)*((1/3*x2^3-1/8*A(2)/B(2)*x2^4+1/15*A(2)^2/B(2)^2*x2^5-...
                                  x2*(1/2*x2^2-1/6*A(2)/B(2)*x2^3+1/12*A(2)^2/B(2)^2*x2^4))...
                                  -(1/3*x1^3-1/8*A(2)/B(2)*x1^4+1/15*A(2)^2/B(2)^2*x1^5-...
                                  x2*(1/2*x1^2-1/6*A(2)/B(2)*x1^3+1/12*A(2)^2/B(2)^2*x1^4)))); % Inertia
        else
            i2 = 2;
            bern = tmp1*(1/(h0^2)+2*(Xv1(x1,x2,A,B,i2)-x2*Wv1(x1,x2,A,B,i2))/(x2-x1)^2);% Bernoulli
            pois = tmp2*(1/(2*A(1))*(1/(h0^2)-1/(h1^2))+1/(2*A(2))*(1/(h1^2)+2*(Xv1(x1,x2,A,B,i2)-x2*Wv1(x1,x2,A,B,i2))/(x2-x1)^2));% Poiseuille
            inst = tmp3*(log(h1/h0)/A(1) + 1/A(2)*(2*(x2*Yv1(x1,x2,A,B,i2)-Zv1(x1,x2,A,B,i2))/(x2-x1)^2-log(h1)));% Inertia
        end        
        Fr_h1 = 0.5*(x2-x1)*lg*(Ps+bern+pois+inst);
    else 
        if (abs(A(2)*x2/B(2))<pres)
            i2 = 2; 
            tmpG = (xs-x1)/(x2-x1)*(x2-(xs+x1)/2);
            bern = tmp1*(1/(h0^2)-(x2*Wv1(x1,xs,A,B,i2)-Xv1(x1,xs,A,B,i2))/(tmpG*(x2-x1)));
            pois = tmp2*(1/(2*A(1))*(1/(h0^2)-1/(h1^2))-1/B(2)^3*(x1-3/2*A(2)/B(2)*x1^2+2*A(2)^2/B(2)^2*x1^3)...
                -2/(x2-x1)/tmpG/B(2)^3*((xs^3/3-3/8*A(2)/B(2)*xs^4+2/5*A(2)^2/B(2)^2*xs^5-...
                                       x2*(xs^2/2-1/2*A(2)/B(2)*xs^3+1/2*A(2)^2/B(2)^2*xs^4))...
                                       -(x1^3/3-3/8*A(2)/B(2)*x1^4+2/5*A(2)^2/B(2)^2*x1^5-...
                                       x2*(x1^2/2-1/2*A(2)/B(2)*x1^3+1/2*A(2)^2/B(2)^2*x1^4)))); 
            inst = tmp3*(log(h1/h0)/A(1)-1/B(2)*(x1-1/2*A(2)/B(2)*x1^2+1/3*A(2)^2/B(2)^2*x1^3)...
                -2/(x2-x1)/tmpG/B(2)*((1/3*xs^3-1/8*A(2)/B(2)*xs^4+1/15*A(2)^2/B(2)^2*xs^5-...
                                  x2*(1/2*xs^2-1/6*A(2)/B(2)*xs^3+1/12*A(2)^2/B(2)^2*xs^4))...
                                  -(1/3*x1^3-1/8*A(2)/B(2)*x1^4+1/15*A(2)^2/B(2)^2*x1^5-...
                                  x2*(1/2*x1^2-1/6*A(2)/B(2)*x1^3+1/12*A(2)^2/B(2)^2*x1^4))));            
        else
        i2 = 2; 
        tmpG = (xs-x1)/(x2-x1)*(x2-(xs+x1)/2);
        bern = tmp1*(1/(h0^2)-(x2*Wv1(x1,xs,A,B,i2)-Xv1(x1,xs,A,B,i2))/(tmpG*(x2-x1)));
        pois = tmp2*(1/(2*A(1))*(1/(h0^2)-1/(h1^2))+1/(2*A(2))*(1/(h1^2))...
            -(x2*Wv1(x1,xs,A,B,i2)-Xv1(x1,xs,A,B,i2))/(tmpG*(x2-x1)));
        inst = tmp3*(log(h1/h0)/A(1)+1/A(2)*((x2*Yv1(x1,xs,A,B,i2)...
            -Zv1(x1,xs,A,B,i2))/(tmpG*(x2-x1))-log(h1))); 
        end        
        Fr_h1 = lg*tmpG*(Ps+bern+pois+inst)+lg*Psupra*(x2-xs)/(x2-x1)*(x2-(x2+xs)/2);
    end
    
    if (h2 < h1*sep)
        if (abs(A(2)*x2/B(2))<pres)
            i2 = 2;
            bern = tmp1*(1/(h0^2)-2*(Xv1(x1,x2,A,B,i2)-x1*Wv1(x1,x2,A,B,i2))/(x2-x1)^2);
            pois = tmp2*(1/(2*A(1))*(1/(h0^2)-1/(h1^2))-1/B(2)^3*(x1-3/2*A(2)/B(2)*x1^2+2*A(2)^2/B(2)^2*x1^3)...
                +2/(x2-x1)^2/B(2)^3*((x2^3/3-3/8*A(2)/B(2)*x2^4+2/5*A(2)^2/B(2)^2*x2^5-...
                                       x1*(x2^2/2-1/2*A(2)/B(2)*x2^3+1/2*A(2)^2/B(2)^2*x2^4))...
                                       -(x1^3/3-3/8*A(2)/B(2)*x1^4+2/5*A(2)^2/B(2)^2*x1^5-...
                                       x1*(x1^2/2-1/2*A(2)/B(2)*x1^3+1/2*A(2)^2/B(2)^2*x1^4)))); 
            inst = tmp3*(log(h1/h0)/A(1)-1/B(2)*(x1-1/2*A(2)/B(2)*x1^2+1/3*A(2)^2/B(2)^2*x1^3)+...
                2/(x2-x1)^2/B(2)*((1/3*x2^3-1/8*A(2)/B(2)*x2^4+1/15*A(2)^2/B(2)^2*x2^5-...
                                  x1*(1/2*x2^2-1/6*A(2)/B(2)*x2^3+1/12*A(2)^2/B(2)^2*x2^4))...
                                  -(1/3*x1^3-1/8*A(2)/B(2)*x1^4+1/15*A(2)^2/B(2)^2*x1^5-...
                                  x1*(1/2*x1^2-1/6*A(2)/B(2)*x1^3+1/12*A(2)^2/B(2)^2*x1^4))));            
        else 
            i2 = 2; 
            bern = tmp1*(1/(h0^2)-2*(Xv1(x1,x2,A,B,i2)-x1*Wv1(x1,x2,A,B,i2))/(x2-x1)^2);
            pois = tmp2*(1/(2*A(1))*(1/(h0^2)-1/(h1^2))+1/(2*A(2))*(1/(h1^2)...
                -2*(Xv1(x1,x2,A,B,i2)-x1*Wv1(x1,x2,A,B,i2))/(x2-x1)^2));
            inst = tmp3*(log(h1/h0)/A(1) +1/A(2)*(2*(Zv1(x1,x2,A,B,i2)...
                -x1*Yv1(x1,x2,A,B,i2))/(x2-x1)^2-log(h1)));
        end        
        Fl_h2 = 0.5*(x2-x1)*lg*(Ps+bern+pois+inst);        
    else
        if (abs(A(2)*x2/B(2))<pres)
            i2 = 2;
            tmpH = (xs-x1)/(x2-x1)*((xs+x1)/2-x1);
            bern = tmp1*(1/(h0^2)-(Xv1(x1,xs,A,B,i2)-x1*Wv1(x1,xs,A,B,i2))/(tmpH*(x2-x1)));
            pois = tmp2*(1/(2*A(1))*(1/(h0^2)-1/(h1^2))-1/B(2)^3*(x1-3/2*A(2)/B(2)*x1^2+2*A(2)^2/B(2)^2*x1^3)...
                +2/(x2-x1)/tmpG/B(2)^3*((xs^3/3-3/8*A(2)/B(2)*xs^4+2/5*A(2)^2/B(2)^2*xs^5-...
                                       x1*(xs^2/2-1/2*A(2)/B(2)*xs^3+1/2*A(2)^2/B(2)^2*xs^4))...
                                       -(x1^3/3-3/8*A(2)/B(2)*x1^4+2/5*A(2)^2/B(2)^2*x1^5-...
                                       x1*(x1^2/2-1/2*A(2)/B(2)*x1^3+1/2*A(2)^2/B(2)^2*x1^4)))); 
            inst = tmp3*(log(h1/h0)/A(1)-1/B(2)*(x1-1/2*A(2)/B(2)*x1^2+1/3*A(2)^2/B(2)^2*x1^3)...
                +2/(x2-x1)/tmpH/B(2)*((1/3*xs^3-1/8*A(2)/B(2)*xs^4+1/15*A(2)^2/B(2)^2*xs^5-...
                                  x1*(1/2*xs^2-1/6*A(2)/B(2)*xs^3+1/12*A(2)^2/B(2)^2*xs^4))...
                                  -(1/3*x1^3-1/8*A(2)/B(2)*x1^4+1/15*A(2)^2/B(2)^2*x1^5-...
                                  x1*(1/2*x1^2-1/6*A(2)/B(2)*x1^3+1/12*A(2)^2/B(2)^2*x1^4))));            
        else
            i2 = 2;
            tmpH = (xs-x1)/(x2-x1)*((xs+x1)/2-x1);
            bern = tmp1*(1/(h0^2)-(Xv1(x1,xs,A,B,i2)-x1*Wv1(x1,xs,A,B,i2))/(tmpH*(x2-x1)));
            pois = tmp2*(1/(2*A(1))*(1/(h0^2)-1/(h1^2))+1/(2*A(2))*(1/(h1^2))-(Xv1(x1,xs,A,B,i2)-x1*Wv1(x1,xs,A,B,i2))/(tmpH*(x2-x1)));
            inst = tmp3*(log(h1/h0)/A(1) +1/A(2)*((Zv1(x1,xs,A,B,i2)-x1*Yv1(x1,xs,A,B,i2))/(tmpH*(x2-x1))-log(h1)));
        end       
     	Fl_h2 = lg*tmpH*(Ps+bern+pois+inst)+ lg*Psupra*(x2-xs)/(x2-x1)*((x2+xs)/2-x1);        
    end
elseif (h1>0)&&(h2<=0)
    Fr_h1 = 0.5*(x2-x1)*lg*Ps;
    Fl_h2 = 0.5*(x2-x1)*lg*Ps;
elseif (h1<=0)&&(h2>0)
    Fr_h1 = 0.5*(x2-x1)*lg*Psupra; 
    Fl_h2 = 0.5*(x2-x1)*lg*Psupra;
end

Fr_h2 = 0.5*lg*Psupra*(x3-x2);
Fvf = [ Fl_h1, Fl_h2; Fr_h1, Fr_h2 ];

end

%%% Some functions to compute primitives
function W = Wv1(xi,xip1,A,B,i)
    pres = 1e-5;
    hi = A(i)*xi + B(i);
    hip1 = A(i)*xip1 + B(i);
    if (abs(A(i)*xip1/B(i))<pres)
          W2 = 1/B(i)^2*(xip1-A(i)*xip1^2/B(i)+A(i)^2*xip1^3/B(i)^2);
          W1 = 1/B(i)^2*(xi-A(i)*xi^2/B(i)+A(i)^2*xi^3/B(i)^2); 
          W = W2-W1;
    else
        W = 1/A(i)*(1/hi-1/hip1);
    end
end

function X = Xv1(xi,xip1,A,B,i)
    pres = 1e-5;
    hi = A(i)*xi+B(i);
    hip1 = A(i)*xip1+B(i);
    if (abs(A(i)*xip1/B(i))<pres)
          X2 = 1/B(i)^2*(xip1^2/2-2*A(i)*xip1^3/3/B(i)+3*A(i)^2*xip1^4/4/B(i)^2);
          X1 = 1/B(i)^2*(xi^2/2-2*A(i)*xi^3/3/B(i)+3*A(i)^2*xi^4/4/B(i)^2); 
          X=X2-X1;
    else
        X = 1/(A(i)^2)*log(hip1/hi)+1/A(i)*(xi-hi/A(i))*(1/hi-1/hip1);
    end 
end

function Y = Yv1(xi,xip1,A,B,i)
    pres = 1e-5;
    hi = A(i)*xi+B(i);
    hip1 = A(i)*xip1+B(i);
    if (abs(A(i)*xip1/B(i))<pres)
        Y2=(xip1*log(B(i))+A(i)/B(i)/2*xip1^2-A(i)^2/B(i)^2/6*xip1^3);
        Y1=(xi*log(B(i))+A(i)/B(i)/2*xi^2-A(i)^2/B(i)^2/6*xi^3);
        Y=Y1-Y2;
    else
        Y = 1/A(i)*(hip1*log(hip1)-hip1-hi*log(hi)+hi);
    end
end

function Z = Zv1(xi,xip1,A,B,i)
    pres = 1e-5;
    hi = A(i)*xi+B(i);
    hip1 = A(i)*xip1+B(i);
    if (abs(A(i)*xip1/B(i))<pres)
        Z2=(xip1^2/2*log(B(i))+A(i)*xip1^3/B(i)/3-A(i)^2*xip1^4/8/B(i)^2);
        Z1=(xi^2/2*log(B(i))+A(i)*xi^3/B(i)/3-A(i)^2*xi^4/8/B(i)^2);
        Z=Z2-Z1;
    else
        tempo = 1/(A(i)^2);
        tempo = tempo*(((hip1^2)*log(hip1)-(hi^2)*log(hi))/2-((hip1^2)-(hi^2))/4);
        Z     = tempo+1/A(i)*(xi-hi/A(i))*(hip1*log(hip1)-hip1-hi*log(hi)+hi);    
    end
end
