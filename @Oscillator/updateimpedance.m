function [ Rg2, Rg, Lg, hsi, Udc ] = updateimpedance( obj, Const, aChink, ain, ac, lc)
% [ Rg2, Rg, Lg, hsi, Udc ] = updateimpedance( obj, Const, aChink, ain, ac, lc)
% Update the internal impedance of the Oscillator object. Check Eq.6 of the paper [1]

% 
% Input arguments:
% - obj: Oscillator object
% - Const: acoustic constant terms
% - aChink: chink area
% - ain: input area of the Oscillator object
% - ac: constriction area
% - lc: constriction length
%
% Output arguments:
% - Rg2: Bernoulli resistance
% - Rg: Viscous term resistance
% - Lg: inductance due to unsteady terms
% - hsi: height at the separation point
% - Udc: low frequency flow in the vocal tract
%
% [1] Elie B., and Laprie Y. "Extension of the single-matrix formulation 
% of the vocal tract: consideration of bilateral channels and connection 
% of self-oscillating models of the vocal folds with a glottal chink". 
% Speech Comm. 82, pp. 85-96 (2016)

%
% Benjamin Elie, 2020
%
% See also VT_Network, Oscillator, esmfsynthesis
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

rho = Const.rho;
co_mu = Const.mu;
sep = Const.sep;
armin = Const.amin;
kiter = Const.k_iter;
T = Const.T;

xvec = obj.x_position;
lg = obj.length;
selfmod = obj.model;
yt = obj.mass_position;

if isa(obj,'Tongue')
    agp = obj.oscillation_factor(kiter);
else
    agp = 1;
end

h1i = sum(yt(:,1)*agp)+obj.low_frequency_abduction(kiter)/lg;
h2i = sum(yt(:,2)*agp)+obj.low_frequency_abduction(kiter)/lg;
h0i = obj.up_height(kiter);
h3i = obj.down_height(kiter);
Hvec = [ h0i, h1i, h2i, h3i ];  
obj.height_vector = Hvec;

%%% Glottis shape
x0 = xvec(1); x1 = xvec(2); x2 = xvec(3); dgl = xvec(end)-xvec(1);
h0 = Hvec(1); h1i = Hvec(2); h2i = Hvec(3);

if strcmp(selfmod,'smooth')
    A(1) = (h1i-h0)/(x1-x0); % slope, part 1
    A(2) = (h2i-h1i)/(x2-x1); % slope, part 2
    
    %%%% Separation point    
    if (h1i>0) && (h2i>0) % No contact between masses
        if (h1i~=h2i) % if h1 different than h2
            hsi = sep*h1i;
        else
            hsi = h2i; % separation height is h2
        end        
        if (h1i>=h2i) || (hsi>=h2i) %
            hsi = h2i;
        end
    else
        hsi = armin/lg;   % if contact, separation height is 0 (set to eps to avoid singularities)
    end
    
    Ag22 = 1/(hsi*lg+aChink)^2;
    Rg2 = 0.5*rho/(lg^2)*(1/(hsi^2)-1/(h0^2));  % Bernoulli
    
    if (h1i~=h2i) % if plate 2 is non-horizontal
        Rg = 0.5*12*co_mu/lg*(1/A(1)*(1/(h0^2)-1/(h1i^2)) + 1/A(2)*(1/(h1i^2)-1/(hsi^2))); % viscous term
        Lg = rho/lg*(1/A(1)*log(h1i/h0)+1/A(2)*log(hsi/h1i)); % inertia
    else  % if plate 2 is horizontal
        Rg = 12*co_mu/lg*(0.5/A(1)*(1/(h0^2)-1/(h1i^2)) + (x2-x1)/(h1i^3)); % viscous term
        Lg = rho/lg*(1/A(1)*log(h1i/h0)+(x2-x1)/h1i); % inertia
    end
elseif strcmp(selfmod,'ishi')
    ag1 = h1i*lg; 
    ag2 = h2i*lg;
    Rk1 = 0.19*rho/(ag1)^2;
    Rk2 = (0.5 - ag2/ain*(1-ag2/ain)) / (ag2^2)*rho;
    Lg1 = rho*(x1-x0)/ag1;
    Lg2 = rho*(x2-x1)/ag2;
    Rv1 = 12*co_mu*lg^2*(x1-x0)/(ag1^3);
    Rv2 = 12*co_mu*lg^2*(x2-x1)/(ag2^3);
    Rg2 = Rk1+Rk2;
    Rg = [Rv1, Rv2];
    Lg = [Lg1,Lg2];
    hsi = min(h1i,h2i); 
    hsi = max(hsi,armin/lg);%
    Ag22 = 1/(hsi*lg+aChink)^2;
end

if ~Const.unst; Lg = Lg*0; end
if strcmp(Const.model,'smooth')
    Rg = [Rg Rg]/2;
    Lg = [Lg Lg]/2;
end

obj.mass_position = yt*agp; 

if isa(obj,'Glottis')
    Ac2 = 1/ac.^2;
    rv_sum = (12*lg*dgl*Ag22.*sqrt(Ag22)+8*pi*lc.*Ac2)*co_mu;
    % rv_sum = 12*(dgl*Ag22+lc.*Ac2)*co_mu;
    rk_sum = 1.38*rho*(Ac2+Ag22);
    % rv_sum = Rg+8*pi*lc.*Ac2*co_mu;
    % rk_sum = 1.38*rho*Ac2+Rg2;
    Udc = (-rv_sum+sqrt(rv_sum*rv_sum+4*rk_sum*obj.upstream_pressure))/(2*rk_sum);
    if ( isempty(Udc) || isnan(Udc) ); Udc = 0; end
%     obj.DC_flow = Udc;
    
    obj.DC_flow = obj.DC_flow+(Udc-obj.DC_flow)*2*pi*500*T;
    Udc = obj.DC_flow;   
    obj.area = max([armin, abs(hsi*lg)+aChink]);  
end

obj.bernoulli = Rg2;
obj.resistance = Rg;
obj.inductance = Lg;
obj.separation_height = hsi;

end