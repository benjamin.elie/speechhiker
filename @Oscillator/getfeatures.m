function [ jitt, shim ] = getfeatures(obj, nPeriode)
% [ jitt, shim ] = getfeatures(obj, nPeriode)
% Compute features on mass oscillation of the Oscillator object
% 
% Input arguments:
% - obj: Oscillator object
% - nPeriode: number of periodes on which the jitter and shimmer are
% computed
%
% Output arguments:
% - jitt: jitter
% - shimmer: shimmer
%
% Benjamin Elie, 2020
%
% See also Oscillator
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; nPeriode = 10; end
if isempty(obj.max_instant)
 obj.getclosureinstant;
end

idx = [ 2, 3, 5 ];

jitt = nan(size(obj.height_vector_output(idx,:)));
shim = nan(size(obj.height_vector_output(idx,:)));

for ksig = 1:3

    sig_mass_1 = obj.height_vector_output(idx(ksig),:);
    max_mass_1 = sig_mass_1(obj.max_instant{ksig});
    perio1 = diff(obj.max_instant{ksig});
    kperio1 = length(perio1);

    if nPeriode >= kperio1
        jitta = mean(abs(diff(perio1)));
        jitt1 = 100*jitta/mean(perio1);
        shim1 = mean(abs(20*log10(max_mass_1(2:end)./max_mass_1(1:end-1))));
    else
        nWin = kperio1-nPeriode+1;
        jitt1 = nan(nWin,1);
        shim1 = nan(nWin,1);
        for k = 1:nWin
            per_tmp = perio1(k:k+nPeriode-1);
            jitta = mean(abs(diff(per_tmp)));
            jitt1(k) = 100*jitta/mean(per_tmp);

            amp_tmp = max_mass_1(k:k+nPeriode-1);
            shim1(k) = mean(abs(20*log10(amp_tmp(2:end)./amp_tmp(1:end-1))));
        end
    end

    jitt1_intp = interp1(obj.max_instant{1}(1:length(jitt1)), jitt1, 1:length(sig_mass_1), 'linear', 'extrap');
    shim1_intp = interp1(obj.max_instant{1}(1:length(shim1)), shim1, 1:length(sig_mass_1), 'linear', 'extrap');

    jitt1 = jitt1_intp; jitt1(jitt1<=0) = 0;
    shim1 = shim1_intp; shim1(shim1<=0) = 0;

    jitt(ksig,:) = jitt1;
    shim(ksig,:) = shim1;
end

end

