function [Sxx, fxx] = outputspectrum(obj, win, nfft, sr,  whole)
% [Sxx, fxx] = outputspectrum(obj, win, nfft, sr,  whole)
% Compute the spectrum of the mass oscillation of the Oscillator object
% 
% Input arguments:
% - obj: Oscillator object
% - win: window type (rect, hann or hamming), default is 'rect'
% - nfft: length of the FFT
% - sr: sampling_frequency
% - whole: if true, returns the FFT from 0 to sr, else from 0 to sr/2
%
% Output arguments:
% - Sxx: complex spectrum
% - fxx: spectrum frequency vector
%
% Benjamin Elie, 2020
%
% See also Oscillator
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; win = 'rect'; end
if nargin < 3; nfft = length(obj.signal); end
if nargin < 4; sr = 1; end
if nargin < 5; whole = false; end 

sig = obj.height_vector_output([2,3,5],:);

switch win
    case 'rect'
        w = ones(size(sig));
    case ' hann'
        w = hann(size(sig, 2));
        w = repmat(w(:).', 3, 1);
    case 'hamming'
        w = hamming(size(sig, 2));
        w = repmat(w(:).', 3, 1);
    otherwise
        warning('Warning: the window type is badly defined. Spectrum is computed with default rectangular window');
        w = ones(size(sig));
end

Sxx = fft(sig.*w, nfft, 2);
fxx = (0:nfft-1)*sr/nfft;

if ~whole
    L = nfft/2+1;
    fxx = fxx(1:L);
    Sxx = Sxx(:,1:L);
end
obj.mass_1_spectrum = Sxx(1,:);
obj.mass_2_spectrum = Sxx(2,:);
obj.open_area_spectrum = Sxx(3,:);
obj.spectrum_frequency_vector = fxx;
end

