function fig_handle = tfplot(obj, dyn, norm, tiers)
% fig_handle = tfplot(obj, dyn, norm)
% plot time frequency objects
%
% Input arguments:
% - obj: TimeFreq object
% - dyn: dynamic range for z-axis, in dB (default is 100 dB)
% - norm: if true, the max of the time-frequency representation is 0 dB
%
% Output argument:
% - fig_handle: figure object
%
% Benjamin Elie, 2020
%
% See also TimeFreq, SpeechAudio, computespectrogram
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; dyn = 100; end
if nargin < 3; norm = false; end
if nargin < 4; tiers = 1; end

set(0,'defaulttextinterpreter','latex')
set(0,'defaultAxesFontSize',20)

objParent = obj.parent;
if isempty(objParent.time_vector)
    objParent.time_vector = (0:length(objParent.signal)-1)/objParent.sampling_frequency;
end
Pxx = abs(obj.time_frequency_matrix).^2;
if norm
    Pxx = Pxx/max(abs(Pxx(:)));
end
max_Pxx = max(abs(Pxx(:)));

fsp = obj.frequency_vector;
tsp = obj.time_vector;

fig_handle = figure;
h(1) = subplot(3,1,[ 1 2 ]);
imagesc(tsp, fsp, db(Pxx)); axis xy
colormap(1-gray)
set(gca,'ticklabelinterpreter','latex')
xlabel('Time (s)')
ylabel('Frequency (Hz)')
caxis([db(max_Pxx)-dyn db(max_Pxx)])
hold on;
if ~isempty(objParent.phonetic_labels)
    plot_phonetic_segment(objParent.phonetic_labels{tiers}, ...
        objParent.phonetic_instants{tiers}, fsp(end));
end
ylim([0 fsp(end) ])
% colorbar
h(2) = subplot(313);
plot(objParent.time_vector, objParent.signal, 'linewidth',2);
set(gca,'ticklabelinterpreter','latex')
xlabel('Time (s)')
ylabel('Signal')
hold on;
if ~isempty(objParent.phonetic_labels)
    plot_phonetic_segment(objParent.phonetic_labels{tiers}, ...
        objParent.phonetic_instants{tiers}, max(objParent.signal));
end
ylim([min(objParent.signal), max(objParent.signal) ])
linkaxes(h, 'x')
xlim([0 max(objParent.time_vector)])
set(gcf, 'units','normalized','position',[ 0 0 1 1 ])

end

function plot_phonetic_segment(label, instant, ylim)    
    tei = -1;
    for k = 1:length(label)
        lblTmp = label{k};
        lblTmp = strrep(lblTmp,'#','\#');
        lblTmp = strrep(lblTmp,'_','\_');
        lblTmp = strrep(lblTmp,'~','\~');
        timeStart = instant(k,1);
        timeEnd = instant(k,2);
        
        if timeStart - tei > 1e-3
            plot(timeStart*ones(2,1), [ -1e10 1e10 ], '--','color',[ 0 0.498 0 ],'linewidth',1)
        end
        plot(timeEnd*ones(2,1), [ -1e10 1e10 ], '--','color',[ 0 0.498 0 ],'linewidth',1)
        text(mean(instant(k,:)), ylim, lblTmp, ...
            'color',[ 0 0.498 0 ],'fontsize',14,'fontweight','bold',...
            'horizontalalignment','center','verticalalignment','top')
        tei = timeEnd;
    end
end

