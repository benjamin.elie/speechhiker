clearvars
close all
clc

% Import area function. The example is a /a/, in the library folder
file2import = 'library/a_example.json';
[af, lf, wvg] = importjson(file2import);

% plot area function
wvg.plotareafunction;

% compute the transfer function
% First, we need to define the physical parameters. You can load them in
% the library...
run('makeconstantparameters.m') % returns Const
[ transFun, freq ] = wvg.computetransferfunction(Const);

% plot the transfer function
h_TF = wvg.plottransferfunction;

% Compute formants
formFreq = wvg.computeformants;

figure(h_TF);
hold on;
% plot formants
for k = 1:length(formFreq)
    [ ~, idx ] = min(abs(freq-formFreq(k)));
    plot(formFreq(k), abs(transFun(idx)), 'ok', 'markerfacecolor', 'r', ...
        'markersize', 10)
end
tt = title('Transfer function of /a/');
set(tt, 'interpreter', 'latex')
leg = legend('Transfer function', 'Formants', 'Location', 'Best');
set(leg, 'interpreter', 'latex')


