clearvars
close all
clc

%% initialization
% Define the target formants, estimated from real /a/ (female speaker)
form_target = [885.9, 1590, 2901, 4290];

% Import area function. 
% The example is a /a/ obtained from MRI of a male subject
% It will serve as the initial solution
file2import = 'library/a_example.json';
[af, lf, wvg] = importjson(file2import);

% plot area function of the initial solution
af_fig = figure;
subplot(221);
plot(cumsum(lf), af, '--k'); hold on;
set(gca,'fontsize',20)
set(gca, 'ticklabelinterpreter','latex')
xlabel('Distance from glottis (m)','interpreter','latex')
ylabel('Area (cm\textsuperscript{2})','interpreter','latex')
set(gcf, 'units','normalized','position', [0 0 1 1 ])
title('Area function',  'interpreter', 'latex');


% plot the transfer function of the initial solution
run('makeconstantparameters.m');
[ init_TF, freq ] = wvg.computetransferfunction(Const, 'cmp');
subplot(212);
semilogy(Const.freq, abs(init_TF),'--k'); hold on;
set(gca,'fontsize',20)
set(gca, 'ticklabelinterpreter','latex')
xlabel('Frequency (Hz)','interpreter','latex')
ylabel('Transfer function','interpreter','latex')
set(gcf, 'units','normalized','position', [0 0 1 1 ])
init_form = wvg.computeformants;
hold on;
% plot formants
for k = 1:length(init_form)
    [ ~, idx(k) ] = min(abs(freq-init_form(k)));   
end
plot(init_form, abs(init_TF(idx)), 'sk', 'markerfacecolor', 'k', ...
        'markersize', 10)

%% Inversion
% set the parameters for the inversion
Const.threshold = 0.1;
Const.potential_weight = 1e-4;
% Const.freq = 0:10:5000;
[ new_af, new_lf, err_form, titer ] = wvg.formant2af(form_target, Const);

%% Plot results
% Compute formants
new_TF = wvg.computetransferfunction(Const, 'cmp');
new_form = wvg.computeformants;

% plot new area function
subplot(221);
plot(cumsum(new_lf), new_af, 'r', 'linewidth', 2)
leg = legend('Initial solution', 'Solution', 'Location', 'NorthWest');
set(leg, 'interpreter', 'latex')

subplot(212);
hold on;
semilogy(Const.freq, abs(new_TF),'r', 'linewidth', 2); hold on;
% plot formants
for k = 1:length(new_form)
    [ ~, idx(k) ] = min(abs(freq-new_form(k)));    
end
plot(new_form, abs(new_TF(idx)), 'ok', 'markerfacecolor', 'r', ...
        'markersize', 10)
for k = 1:length(form_target)
    [ ~, idx(k) ] = min(abs(freq-form_target(k)));    
end
plot(form_target, abs(new_TF(idx)), 'vk', 'markerfacecolor', 'b', ...
        'markersize', 10)
tt = title('Transfer function');
set(tt, 'interpreter', 'latex')
leg = legend('Initial transfer function', 'Initial formants', ...
    'Estimated transfer function', 'Estimated formants $f_\mathrm{est}$', ...
    'Target formants $f_\mathrm{targ}$', 'Location', 'SouthEast');
set(leg, 'interpreter', 'latex')

subplot(222);
hold on;
stem(titer, err_form,'ok','markerfacecolor', 'r', 'linewidth', 2, 'color', 'r'); hold on;
set(gca, 'yscale', 'log')
title('Cost function $\mathcal{C} = \langle \frac{f_\mathrm{est}-f_\mathrm{targ}}{f_\mathrm{targ}} \rangle$', 'interpreter', 'latex');
set(gca, 'ticklabelinterpreter','latex')
xlabel('Computation time (s)','interpreter','latex')
ylabel('$\mathcal{C}$ (\%)','interpreter','latex')