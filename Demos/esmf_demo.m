%%%%%%%%%% This is the demo file of running speech synthesis using ESMF.
% The utterance is a VCV sequence. Try to decode which one by listening to
% the result. The response can be obtained by running the follonig line:
% VT.phonetic_label{2:end-1} % uncomment to know which is the VCV sequence.
%%%%%%%%%%

clearvars 
close all
clc

set(0,'defaulttextinterpreter','latex')
set(0,'defaultAxesFontSize',20)

% Some parameters...
prm.psubmax = 500; % The maximal subglottal pressure
prm.fsim = 6e4; % The simulation frequency
sro = 1.2e4; % Output sampling frequency

run makeconstantparameters.m
Const.namp = 1e-6*1e4/prm.fsim; % Amplitude of frication noise (no way to automatically tune it, unfortunately...)
Const.dispcomp = true; % false for minimal verbosity
Const.T = 1/prm.fsim;

fileJSON = 'JSON_files_examples/scenario.json';
disp('Reading scenario file')
VT = vtjsonread(fileJSON, 'JSON_files_examples', prm);
disp('Scenario file read')
disp('Start simulation')
VT.esmfsynthesis(Const);

outputresample(VT, sro, prm.fsim);
p_o = VT.pressure_radiated;

nwin = 64;
novlp = nwin*7/8;
nfft = 2^nextpow2(sro);

% We use SpeechAudio object
SP = SpeechAudio('signal', p_o, ...
    'sampling_frequency', sro);
SP.phonetic_labels = {VT.phonetic_label};
SP.phonetic_instants =  {VT.phonetic_instant};

% Plot input parameters
VT.plotinputparameters;

% Plot spectrogram
SP.computespectrogram(nwin, novlp, nfft);
h_spectro = SP.spectrogram.tfplot;
tt = title('Spectrogram of the simulated VCV sequence');
set(tt, 'interpreter', 'latex');

% Plot output parameters
VT.plotoutputparameters;