%%%%%%%%%% This is the demo file for X-GLOS, a method for voiced/noise source separation.

clearvars
close all
clc

set(0,'defaulttextinterpreter','latex')
set(0,'defaultAxesFontSize',20)

% audio file
audioName = 'input_data/azha.wav';

% Create the SpeechAudio object
Utt = file2speech(audioName);

% Parameters for X-GLOS
prm.fomin = 80;
prm.fomax = 200;
% prm.jitt = true;
% prm.shim = true;
prm.hopfactor = 16;
Utt.xglos(prm);

% parameters for spectrogram
nwin = 512;
novlp = nwin*7/8;
nfft = 2^nextpow2(Utt.sampling_frequency);

UttVoiced = Utt.voiced_signal;
UttNoise = Utt.unvoiced_signal;

Utt.computespectrogram(nwin, novlp, nfft);
Utt.spectrogram.tfplot;
tt = suptitle('Spectrogram of the utterance /aZa/');
set(tt, 'interpreter', 'latex')

UttVoiced.computespectrogram(nwin, novlp, nfft);
UttVoiced.spectrogram.tfplot;
tt = suptitle('Spectrogram of the voiced components');
set(tt, 'interpreter', 'latex')

UttNoise.computespectrogram(nwin, novlp, nfft);
UttNoise.spectrogram.tfplot;
tt = suptitle('Spectrogram of the unvoiced components');
set(tt, 'interpreter', 'latex')

Utt.computeVQ(nwin, novlp, nfft, 'rms');
figure;
h(1) = subplot(211);
plot(Utt.time_vector, Utt.signal, 'linewidth', 2);
set(gca,'ticklabelinterpreter','latex')
ylabel('Signal')
ylabel('Time (s)')
h(2) = subplot(212);
plot(Utt.time_vector, Utt.voicing_quotient, 'linewidth', 2);
set(gca,'ticklabelinterpreter','latex')
ylabel('VQ (\%)')
xlabel('Time (s)')
linkaxes(h,'x');
xlim([0 max(Utt.time_vector)])
ylim([0 100])
set(gcf, 'units', 'normalized', 'position', [ 0 0 1 1 ])
