%%%%%%%%%% This is the demo file for to show how to analyze oral corpus
% The corpus presented is very small. Only one speaker, who utters three
% VCV pseudowords: aZa, iZi, and uZu. We will plot his vowel space, which
% contains only 2 /a/, 2 /i/, and 2 /u/. 

clearvars
close all
clc

set(0,'defaulttextinterpreter','latex')
set(0,'defaultAxesFontSize',20)

% audio file
audioName_1 = 'input_data/azha.wav'; 
audioName_2 = 'input_data/izhi.wav';
audioName_3 = 'input_data/uzhu.wav';

% segmentation files
segName_1 = 'input_data/azha.TextGrid';
segName_2 = 'input_data/izhi.TextGrid';
segName_3 = 'input_data/uzhu.TextGrid';

% Create our speaker object
Sp = Speaker('id', 1, 'mother_tongue', 'French', 'age', 32, 'sex', 'Male', ...
    'utterance', {}, 'phonemes', {});

% Create the SpeechAudio object
Utt1 = file2speech(audioName_1);
Utt2 = file2speech(audioName_2);
Utt3 = file2speech(audioName_3);

% Segmentation
Utt1.alignread(segName_1);
a_real = Utt1.segmentspeech; 

Utt2.alignread(segName_2);
i_real = Utt2.segmentspeech;

Utt3.alignread(segName_3);
u_real = Utt3.segmentspeech;
Sp.utterance = [Utt1, Utt2, Utt3];
Sp.phonemes = [ a_real(:) ; i_real(:) ; u_real(:) ];

Sp.plotvowelspace('oral');



