function [labels, instants] = read_whc_segmentation_files(fileName)
% [labels, instants] = read_whc_segmentation_files(filename)
% read WHC segmentation files and returns segmentation labels and instants of all
% tiers
%
% Input arguments:
% - filename: path to WHC segmentation file
% 
% Output arguments:
% - labels: cell array containing the labels of all segmentation tiers
% - instants: cell array containing the start and end values of all
% segmentation tiers
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, alignread, segmentspeech
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

    dataWord = importdata(fileName, '\n');

    nWord = length(dataWord);
    lblWord = {};
    instWord = nan(nWord,2);
    kiter = 1;
    for k = 1:nWord
        currWord = dataWord{k};    
        if strcmp(currWord(1), '(')
            idx = strfind(currWord, '-');
            sentStart = str2double(currWord(idx(1)+1:idx(2)-1));
            sentEnd =  str2double(currWord(idx(2)+1:end));
        else 
            idx = strfind(currWord,' ');
            label = currWord(1:idx(1)-1);
            start = str2double(currWord(idx(1)+1:idx(2)-1))/100 + sentStart - 1/100;
            wEnd = str2double(currWord(idx(2)+1:idx(3)-1))/100 + sentStart;

            lblWord{end+1} = label;
            instWord(kiter, 1) = start;
            instWord(kiter, 2) = wEnd;
            kiter = kiter + 1;
        end
    end
    instWord(isnan(instWord(:,1)),:) = [];
    instants = {instWord};
    labels = {lblWord}; 

end