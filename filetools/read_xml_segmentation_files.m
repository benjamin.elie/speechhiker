function [labels, instants] = read_xml_segmentation_files(fileName)
% [labels, instants] = read_xml_segmentation_files(filename)
% read XML sefgmzntation files and returns segmentation labels and instants of all
% tiers
%
% Input arguments:
% - filename: path to XML segmentation file
% 
% Output arguments:
% - labels: cell array containing the labels of all segmentation tiers
% - instants: cell array containing the start and end values of all
% segmentation tiers
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, alignread, segmentspeech
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

    fileParsed = parseXML(fileName);
    idx = find(arrayfun(@(x)strcmpi(x.Name,'segmentlist'), fileParsed.Children));
    varTmp = fileParsed.Children(idx).Children;
    structSegm = varTmp(arrayfun(@(x)strcmpi(x.Name,'speechsegment'), varTmp));
    
    nSeg = length(structSegm);
    start = [];
    dur = [];
    labels = {};
    
    for kS = 1:nSeg
        varTmp = structSegm(kS).Children;

        structWord = varTmp(arrayfun(@(x)strcmpi(x.Name,'word'), varTmp));
        nWord = length(structWord);    
        for kW = 1:nWord
            currWord = structWord(kW);
            att = currWord.Attributes;
            readWord = currWord.Children.Data;
            readWord(strfind(readWord,' ')) = [];
%             readWord(strfind(readWord,'.')) = [];
%             readWord(strfind(readWord,',')) = [];
%             readWord(strfind(readWord,'?')) = [];
%             readWord(strfind(readWord,';')) = [];
%             readWord(strfind(readWord,'!')) = [];
%             readWord(strfind(readWord,'/')) = [];
%             if ~isempty(readWord)
%                 if strcmp(readWord(end), char(8217)) || strcmp(readWord(end), char(39))
%                     readWord(end) = [];
%                 end
%             end
            if ~isempty(readWord)      
                idx_dur = find(arrayfun(@(x)strcmpi(x.Name,'dur'), att));
                idx_start = find(arrayfun(@(x)strcmpi(x.Name,'stime'), att));
                dur(end+1) = str2double(att(idx_dur).Value);
                start(end+1) = str2double(att(idx_start).Value);
                labels{end+1} = readWord;
            end
        end
    end
    stop = start+dur;
    
	labels = {labels};
    instants = {[ start(:), stop(:) ]};
end
