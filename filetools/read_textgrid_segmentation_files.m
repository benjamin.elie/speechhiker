function [labels, instants] = read_textgrid_segmentation_files(filename)
% [labels, instants] = read_textgrid_segmentation_files(filename)
% read texgrid files and returns segmentation labels and instants of all
% tiers
%
% Input arguments:
% - filename: path to textgrid file
% 
% Output arguments:
% - labels: cell array containing the labels of all segmentation tiers
% - instants: cell array containing the start and end values of all
% segmentation tiers
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, alignread, segmentspeech
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    POINT_BUFFER = 0.025; % 25 ms buffer on either side of the point

    if ~exist(filename, 'file')
        error('Error: %s not found\n', filename);
        labels = {};
        instants = {};
    end

    fid = fopen(filename, 'rt');
    C = textscan(fid, '%s', 'delimiter', '\n');
    fclose(fid);

    C = C{1};
    tiers = 0;
    proceed_int = 0; % proceed with intervals
    proceed_pnt = 0; % proceed with point-sources
    xmin = 0;
    xmax = 1;

    for k=1:length(C)
        % try to read a string in the format: %s = %s
        if (isempty(C{k}))
            continue;
        end

        A = textscan(C{k}, '%[^=] = %s', 'delimiter', '\n');  

        if (~isempty(A{1}{1}))
            switch A{1}{1}
                % intervals
                case {'intervals: size '} % we found the start of a new tier, now allocate mem
                    tiers = tiers + 1;
                    tier_len = str2double(A{2}{1});
                    labels{tiers} = cell(tier_len, 1);
                    start{tiers}  = zeros(tier_len, 1);
                    stop{tiers}   = zeros(tier_len, 1);
                    cnt = 1;
                    proceed_int = 1;
                case {'xmin '}
                    if (proceed_int)
                        start{tiers}(cnt) = str2double(A{2}{1});
                    else
                        xmin = str2double(A{2}{1});
                    end
                case {'xmax '}
                    if (proceed_int)
                        stop{tiers}(cnt) = str2double(A{2}{1});
                    else
                        xmax = str2double(A{2}{1});
                    end
                case {'text '}
                    if (proceed_int)
                        lab = A{2}{1};
                        if (lab(end) ~= '"')
                            lab = lab(1:end-1);
                        end

                        labels{tiers}{cnt} = lab;
                        cnt = cnt + 1;
                        if (cnt > tier_len)
                            proceed_int = 0;
                        end
                    end
                % point-sources    
                case {'points: size '} % we found the start of a new tier, now allocate mem
                    tiers = tiers + 1;
                    tier_len = str2double(A{2}{1});
                    labels{tiers} = cell(tier_len, 1);
                    start{tiers}  = zeros(tier_len, 1);
                    stop{tiers}   = zeros(tier_len, 1);
                    cnt = 1;
                    proceed_pnt = 1;
                case {'time '}
                    if (proceed_pnt)
                        start{tiers}(cnt) = str2double(A{2}{1}) - POINT_BUFFER;
                        stop{tiers}(cnt) = str2double(A{2}{1}) + POINT_BUFFER;                    
                        if (start{tiers}(cnt) < xmin)
                            start{tiers}(cnt) = xmin;
                        end

                        if (stop{tiers}(cnt) > xmax)
                            stop{tiers}(cnt) = xmas;
                        end
                    end
                case {'mark '}
                    if (proceed_pnt)
                        lab = A{2}{1};
                        if (lab(end) ~= '"')
                            lab = lab(1:end-1);
                        end

                        labels{tiers}{cnt} = lab;
                        cnt = cnt + 1;
                        if (cnt > tier_len)
                            proceed_pnt = 0;
                        end
                    end
            end
        end            
    end 
  
    labels = removequotes(labels);
    instants = cell(length(labels),1);

    for k = 1:length(labels)
        instants{k} = [ start{k}(:), stop{k}(:) ];
    end
end

function new_label = removequotes(label)

    new_label = label;
    for k1 = 1:length(label)
        lbltmp = label{k1};
        new_label_tmp = lbltmp;
        for k2 = 1:length(lbltmp)
            lbltmp2 = lbltmp{k2};
            lbltmp2(strfind(lbltmp2, '"')) = [];
            new_label_tmp{k2} = lbltmp2;
        end    
        new_label{k1} = new_label_tmp;
    end
end
