function [labels, instants] = read_seg_segmentation_files(fileName)
% [labels, instants] = read_seg_segmentation_files(filename)
% read SEG segmentation files and returns segmentation labels and instants of all
% tiers
%
% Input arguments:
% - filename: path to SEG segmentation file
% 
% Output arguments:
% - labels: cell array containing the labels of all segmentation tiers
% - instants: cell array containing the start and end values of all
% segmentation tiers
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, alignread, segmentspeech
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
    dataPhon = importdata(fileName, '\n');
    nPhon = length(dataPhon);
    lblPhon = {};
    instPhon = nan(nPhon,2);
    kiter = 1;

    lblWord = {};
    instWord = [];

    lblSent = {};
    instSent = [];
    iterSent = 1;

    for k = 1:nPhon
        currPhon = dataPhon{k};    
        if strcmp(currPhon(1:6), '#@ sid')
            idx = strfind(currPhon, '-');
            sentStart = str2double(currPhon(idx(1)+1:idx(2)-1));
            sentEnd =  str2double(currPhon(idx(2)+1:end));
            instSent(iterSent,:) = [ sentStart, sentEnd ];
            if iterSent > 1
                lblSent{iterSent-1} = lblSentTmp;
            end        
            iterSent = iterSent + 1;
            lblSentTmp = [];
        else
             if strcmp(currPhon(1:7), '#@ word')
                idx1 = strfind(currPhon, '=');
                idx2 = strfind(currPhon, ' ');
                Word = currPhon(idx1+1:idx2(2)-1);
                lblWord{end+1} = Word;
                if isempty(lblSentTmp)
                    lblSentTmp = Word;
                else
                    lblSentTmp = [ lblSentTmp, ' ', Word ];
                end
            end

            if ~strcmp(currPhon(1), '#')
                idx = strfind(currPhon,' ');
                fileId = currPhon(1:idx(1)-1);
                sentId = currPhon(idx(1)+1:idx(2)-1);
                idx2 = strfind(sentId,':');        
                label = sentId(idx2+2);
                sentPos = sentId(end);
                start = str2double(currPhon(idx(3)+1:idx(4)-1))/100 + sentStart - 1e-2;
                for kc = 1:2
                    c1(kc) = str2double(currPhon(idx(3+kc)+1:idx(4+kc)-1))/100;
                end
                wEnd = start + sum(c1) + str2double(currPhon(idx(end)+1:end))/100;

                lblPhon{end+1} = label;
                instPhon(kiter, 1) = start;
                instPhon(kiter, 2) = wEnd;
                kiter = kiter + 1;

                if sentPos == 1
                    instWord = [ instWord; start, wEnd ];
                end
                if sentPos == 2
                    instWord(end,2) = wEnd;
                end

            end
        end
    end
    instPhon(isnan(instPhon(:,1)),:) = [];
    instants = {instPhon, instWord, instSent};
    labels = {lblPhon, lblWord, lblSent};   
end
