%%%%% Example of nasal tract area function by Serrurier :
%%%%% Serrurier A., and Badin P. "A three-dimensional articulatory model of the velum and nasopharyngeal wall based on MRI and CT data". The Journal of the Acoustical Society of America, 123(4), pp. 2335-2355 (2008)

Arat = 0.6306*1e-4; Lrat = 1.0638e-2;
Antleft = [ 4.45 4.5 5.05 4.25 1.5 1.5 1.5 1.5 1.35 1.3 1.45 1.7 1.75 1.45 1.15 1.25 1.25 1 1.05 1 0.9 1.15 1.35 1.7 1.8 1.6 ].'*Arat;
Antright = [ 3.25 3.45 3.4 3.4 3.05 2.8 3 3.5 3.5 3.05 2.75 2.9 2.9 2.4 2.1 1.75 1.65 2 2.35 3.05 3.45 3.15 ].'*Arat;
Lnt = [0.5 0.45 0.4 0.75 0.5 0.4 0.45 0.4 0.45 0.4 0.4 0.5 0.4 0.45 0.45 0.4 0.4 0.55 0.45 0.4 0.25 0.3 0.3 0.35 0.3 0.4].'*Lrat;
Ant = [Antleft(1:4); Antright+Antleft(5:end)]; 
Lntright = Lnt(5:end); 
Lntleft = Lnt;
