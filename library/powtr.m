function y = powtr( x, a, b, dr )
% converts area function to midsagittal distance, or the contrary according
% ot the power transformation

if nargin < 4; dr = 1; end

if dr == 1 % midsagittal to area function
    x = 100*x; % x should be in cm
    y = a.*x.^b;
    y = y*1e-4; % goes back to m^2
else % area to midsagittal distance
    x = 1e4*x; % x should be in cm^2
    y = exp((log(x)-log(a))/b);
    y = y*1e-2; % goes back to m
end

end

