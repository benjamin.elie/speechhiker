Tair = 37;
%%%%%%%%% Coefficients for wall parameters
loss_terms = 'birk'; % maeda, birk or mokh
switch loss_terms
    case 'maeda' % %% From Maeda
        wall_resi = 1600;	%/* wall resistance, gm/s/cm2		*/
        wall_mass = 1.5;	%/* wall mass per area, gm/cm2		*/
        wall_comp = 3.0e5;	%/* wall compliance			*/
    case 'birk' % %%%% From Birkholz
        wall_resi = 8e3;	%/* wall resistance, gm/s/cm2		*/
        wall_mass = 21;	%/* wall mass per area, gm/cm2		*/
        wall_comp = 8.45e6;	%/* wall compliance			*/
    case 'mokh' %%%% From Mokhtari
        wall_resi = 14e3;	%/* wall resistance, gm/s/cm2		*/
        wall_mass = 15e0;	%/* wall mass per area, gm/cm2		*/
        wall_comp = 3e6;	%/* wall compliance			*/
end

Const.sr = 2e4;
Const.freq = 1:10:5000;
% Const.freq(1) = 1e-10;
Const.tm = 1;
Const.tcl = 0.01;
Const.namp = 1e-7;
Const.dispcomp = 1;
Const.wallyield = 1;
Const.dynterm = 1;
Const.wr = wall_resi;
Const.wm = wall_mass;
Const.wc = wall_comp;
Const.rho = 1.2929*(1-0.0037*Tair);
Const.c = 331.45*(1+ 0.0018*Tair);
Const.mu = 1.85*10^(-5);
Const.lg = 0.01;
Const.Xg = 0.3*1e-2;
Const.kc = 1.42;
Const.dist_source = 2; % Gaussian noise
Const.fh = 0.2e3; % Cut-off frequency for high-pass filter
Const.fl = 4e3; % Cut-off frequency for low-pass filter
Const.ordh = 1; % Order of high-pass filter
Const.ordl = 7; % Order of low-pass filter
Const.bf = [0.9 0.1 -0.8]; 
Const.af = 1;
Const.K_b = Const.kc*Const.rho;
Const.Lord = 50e-3;
Const.Rec = 1.7e3; % Reynolds threshold for frication noise generation
Const.G = 9/128;
Const.S = 3/8;
Const.sep = 1.2;
Const.filt = 'fir1';
Const.blim = 0;
Const.beta = 5e-8;
Const.nmass = 2;
Const.amin = 1e-11;
Const.model = 'ishi';