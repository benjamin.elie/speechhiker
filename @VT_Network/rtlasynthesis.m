function pout = rtlasynthesis(obj, Const)
% pout = rtlasynthesis(obj, Const)
% running speech synthesis using the RTLA method.
% THIS METHOD IS NOT FULLY IMPLEMENTED YET. CHECK FOR UPDATES
% 
% Input arguments:
% - obj: network object
% - Const: acoustic constant terms
%
% Output arguments:
% - pout: radiated acoustic pressure
%
% Benjamin Elie, 2020
%
% See also VT_Network, VT_Waveguide, MainOralTract, SubGlottalTract,
% GlottalChink, Oscillator, Glottis, Tongue, SpeechAudio
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; Const = []; end
if ~isfield(Const,'kctc'); Const.kctc = 4; end
if ~isfield(Const,'rctc'); Const.rctc = 2.2; end
if ~isfield(Const,'kctctt'); Const.kctctt = 1.5; end
if ~isfield(Const,'rctctt'); Const.rctctt = 1.2; end
if ~isfield(Const,'c'); Const.c = 352.3313; end
if ~isfield(Const,'mu'); Const.mu = 1.8500e-06; end
if ~isfield(Const,'rho'); Const.rho = 1.1255; end
if ~isfield(Const,'blim'); Const.blim = 1e-5; end
if ~isfield(Const,'filt'); Const.filt = 'fir1'; end
if ~isfield(Const,'sep'); Const.sep = 1.2; end
if ~isfield(Const,'Rec'); Const.Rec = 1.7e3; end
if ~isfield(Const,'nmass'); Const.nmass = 2; end
if ~isfield(Const,'dist_source'); Const.dist_source = 2; end
if ~isfield(Const,'fh'); Const.fh = 200; end
if ~isfield(Const,'fh'); Const.fl = 4e3; end
if ~isfield(Const,'ordh'); Const.ordh = 1; end
if ~isfield(Const,'ordl'); Const.ordl = 7; end
if ~isfield(Const,'bf'); Const.bf = [0.9000 0.1000 -0.8000]; end
if ~isfield(Const,'af'); Const.af = 1; end
if ~isfield(Const,'K_b'); Const.K_b = 1.5982; end
if ~isfield(Const,'G'); Const.G = 9/128; end
if ~isfield(Const,'S'); Const.S = 3/8; end
if ~isfield(Const,'dispcomp'); Const.dispcomp = true; end
if ~isfield(Const,'wallyield'); Const.wallyield = true; end
if ~isfield(Const,'dynterm'); Const.dynterm = true; end
if ~isfield(Const,'beta'); Const.beta = 5e-8; end
if ~isfield(Const,'Qr'); Const.Qr = 0.2; end
if ~isfield(Const,'Qrc'); Const.Qrc = 0.05; end
if ~isfield(Const,'wr'); Const.wr = 8000; end
if ~isfield(Const,'wm'); Const.wm = 21; end
if ~isfield(Const,'wc'); Const.wc = 8450000; end
if ~isfield(Const,'selfoscil'); Const.selfoscil = true; end
if ~isfield(Const,'amin'); Const.amin = 1e-11; end
if ~isfield(Const,'model'); Const.model = 'ishi'; end
if ~isfield(Const,'osc_tg'); Const.osc_tg = false; end
if (~isfield(Const,'model_tt') && Const.osc_tg); Const.model_tt = 'smooth'; end
if ~isfield(Const,'unst'); Const.unst = true; end
if ~isfield(Const,'rmlt'); Const.rmlt = 4; end
if ~isfield(Const, 'visco'); Const.visco = true; end
if ~isfield(Const, 'R_loss_filter_order'); Const.R_loss_filter_order = 20; end
if ~isfield(Const, 'P_loss_filter_order'); Const.P_loss_filter_order = 20; end
if ~isfield(Const, 'gamma'); Const.gamma = 1.4; end
if ~isfield(Const, 'air_molar_mass'); Const.air_molar_mass = 28.94; end



% Initialization of the network
[chbool, sgbool, ich, isg ] = searchchinkandsubglott(obj);
[ isGlott, isSupOscill, idxGlott, idxSupOscill ] = searchoscillators(obj);
if isSupOscill
    nSupOscill = length(idxSupOscill); 
    warning('RTLA does not account for supraglottal oscillators')
end

%%%%% Special objects
OralTract = obj.waveguide{1};
[lf, af ] = checkareafunction(OralTract);

numIter_i = size(OralTract.area_function,2);
sr = Const.c/lf(1);
T = Const.T;
tvec_i = (0:numIter_i-1)*T;
tMax = numIter_i*T;
numIter_o = fix(tMax*sr);
tvec_o = (0:1/sr:tMax);
freq = 0:1/tMax:sr; 
freq(1) = 1e-11;

if numIter_o ~= numIter_i
   obj.inputresample(tvec_i, tvec_o);
end

if isGlott
    objGlottis = obj.oscillators{idxGlott};
else
    if  isempty( obj.input_area ) || length(obj.input_area) < numIter
        error('Glottal opening parameters not set properly for parametric glottis based simulation. Use self-oscillations instead'); 
    else
        glottalArea = obj.input_area;
        ag0 = obj.abduction;
        objGlottis = Glottis;
        objGlottis.partial_abduction = ag0/objGlottis.max_opening;
        objGlottis.length = 0.022;
    end
end

[ alpha_sup, gamma_sup, beta_sup ] = acouscoef(af);
[ loss_sup, rad_sup ] = computefilters(OralTract);


if sgbool
    SubGlottTract = obj.waveguide{isg};
    [lf_sub, af_sub ] = checkareafunction(SubGlottTract);
    [ alpha_sub, gamma_sub, beta_sub ] = acouscoef(af_sub);
    [ loss_sup, rad_sup ] = computefilters(SubGlottTract);
end



end

function [ alpha_coeff, gamma_coeff, beta_coeff ] = acouscoef(af)

    alpha_coeff = (2*af(1:end-1,:))./(af(1:end-1,:)+af(2:end,:));    
    gamma_coeff = (af(1:end-1,:)-af(2:end,:))./(af(1:end-1,:)+af(2:end,:));
    beta_coeff = (2*af(2:end,:))./(af(1:end-1,:)+af(2:end,:));

end

function [lf, af ] = checkareafunction(obj)

    af = obj.area_function;
    lf = obj.length_function;
    nTubes = size(af,1);

    if any(diff(lf,1,1))
        warning('Automatic interpolation: the vocal tract is not evenly discretized along x')    
        [ lf, af ] = areafunctioninterpol( obj, nTubes );
    end


end

function [ loss, rad ] = computefilters(obj, freq, Const )

gamma = Const.gamma;
molarMass = Const.air_molar_mass;
Cv = (gamma-1)*8315/molarMass;
Cp = gamma*Cv;
omega = 2*pi*freq;
lambda = 5/3*Const.mu*Cv;
c = Const.c;

af = sqrt(obj.area_function/pi);
af2 = pi*af.^2;
lf = obj.length_function;
[ nTubes, nFrame ] = size(af);
nFreq = length(freq);
nF_loss = Const.P_loss_filter_order;
nF_rad = Const.R_loss_filter_order;
loss = zeros(nTubes, nFrame, nF_loss);
rad = zeros(nFrame, nF_rad);

if Const.thermal_loss
    lv = Const.mu / (Const.rho*c);  % viscous bounday tickness
    lt = lambda*molarMass/(Const.rho*c*Cp); %thermal boundary tickness
else
    lv = 0;
    lt = 0;
end

%%%%% THIS SHOULD BE VECTORIZED !!!
for kT = 1:nTubes
    for kF = 1:nFrame
        GGamma = (omega/c).* sqrt(-1.*(1+sqrt(2*c*lv./omega)*(1-1i)/af(kT, kF)).*(1+sqrt(2*c*lt/omega)*(1-1i)*(gamma-1)/a(kT, kF)));
        DPf = exp(-GGamma*lf(kT, kF));
        idx = 1:fix(nFreq/2);
        DPf2 = abs(DPf(idx)).*exp(1i*angle(DPf(idx))-1i*angle(DPf(idx(end))).*omega(idx)./omega(idx(end)));
        DPt = real(ifft([DPf2 conj(DPf2(end-1:-1:2))], 'symmetric')); 
        loss(kT, kF, :) = DPt(2:Const.P_loss_filter_order+1);          
    end
end

for kF = 1:nFrame
    
    aTmp = af(end, kF);
    w = 2*(omega)*af2(end, kF)/c;
    Zr = (Const.rho*c)*((1-2*besselj(1,w)./w)+1i*2*struve1(w,16)./w); % radiation impedance
    ksi = sqrt(Const.rho^2*c^2*(1+sqrt(2*c*lv./omega)*(1-1i)/aTmp)./(1+sqrt(2*c*lt./omega)*(1-1i)*(gamma-1)/aTmp));
    GGamma = sqrt(-omega.^2/c^2.*(1+sqrt(2*c*lv./omega)*(1-1i)/aTmp).*(1+sqrt(2*c*lt./omega)*(1-1i)*(gamma-1)/aTmp));
    romega = (-Zr+ksi)./(Zr+ksi).*exp(-GGamma*lf(end,kF));

    idx = find(abs(imag(romega))<5e-2, 1, 'last');
    ff = freq(idx);

    Fznew = (0:freq(2):ff);
    Fznew(1) = 1e-15; 
    omeganew = 2*pi*Fznew;

    w = 2*(omeganew)*af2(end, kF)/c;
    Zr = (Const.rho*c)*((1-2*besselj(1,w)./w)+1i*2*struve1(w,16)./w); % radiation impedance
    ksi = sqrt(Const.rho^2*c^2*(1+sqrt(2*c*lv./omeganew)*(1-1i)/aTmp)./(1+sqrt(2*c*lt./omeganew)*(1-1i)*(gamma-1)/aTmp));
    GGamma = sqrt(-omeganew.^2/c^2.*(1+sqrt(2*c*lv./omeganew)*(1-1i)/aTmp).*(1+sqrt(2*c*lt./omeganew)*(1-1i)*(gamma-1)/aTmp));
    romeganew = (-Zr+ksi)./(Zr+ksi).*exp(-GGamma*lf(end,kF));

    rtImFmax0 = real(ifft([romeganew conj(romeganew(end-1:-1:2))], 'symmetric'));
    [p,q] = rat(freq(end)/(2*ff) , 0.0001);
    rtresample = resample(rtImFmax0, p,q,1);     
    rt = rtresample(1:nF_rad);           
    rad(kF,:) = rt./sum(rt);
end

end