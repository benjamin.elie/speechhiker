function updateacoustics(obj, Const)
% updateacoustics(obj, Const)
% update the acoustic terms in the waveguides of the networks
% 
% Input arguments:
% - obj: network object
% - Const: acoustic constant terms
%
% Benjamin Elie, 2020
%
% See also VT_Network, VT_Waveguide, MainOralTract, SubGlottalTract,
% esmfsynthesis
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

nPipe = length(obj.waveguide);
Nx = obj.number_tubes;

chbool = obj.isChink;
isg = obj.whereSubGlott; 
ich = obj.whereChink;
T = Const.T;
Srad = T/2*Const.S*pi*sqrt(pi)/Const.rho;

Ut = obj.flow_inst;
Uch = 0;
Ug = obj.waveguide{1}.glottal_flow_inst;
if chbool; Uch = obj.waveguide{ich}.glottal_flow_inst; end

for k = 1:nPipe % Store U values in different pipes
    wvg_tmp = obj.waveguide{k};
    switch k
        case 1
            OralTract = wvg_tmp;
            wvg_tmp.flow_time(1) = wvg_tmp.glottal_flow_inst;
            wvg_tmp.flow_time(2:Nx(1)+1) = Ut(1:Nx(1));
        case isg % strcmp(Coup(k).Nat,'subglott')
            wvg_tmp.flow_time =  Ut(sum(Nx(1:k-1))+k-1:sum(Nx(1:k))+k-2);
            wvg_tmp.flow_time(Nx(k)+1) = Ug+Uch;
            SubGlott = wvg_tmp;
        case ich
            wvg_tmp.flow_time = Uch;
        otherwise
            wvg_tmp.flow_time = Ut(sum(Nx(1:k-1))+k-1:sum(Nx(1:k))+k-1);
    end
end

Lgl = OralTract.glottal_inductance+OralTract.Lj(1);

for k = 1:nPipe 
    Pt = zeros(Nx(k)+1,1);
    wvg_tmp = obj.waveguide{k};
    ch = [];
    child = wvg_tmp.child_wvg;
    if ~isempty(child)
        pos_ch = wvg_tmp.child_point;
        for kch = 1:length(child)
            ch(kch) = child{kch}.norder;
        end
    end
    switch k
        case 1               
            OralTract.Q(1) = 4*Lgl/T*Ug+4/T*(OralTract.Lj(1))*Uch-OralTract.Q(1);
        case ich  
            Chink = wvg_tmp;
            Lch = Chink.Lj(1);
            Chink.Q(1) = 4*(Lch+OralTract.Lj(1))/T*Uch+4/T*OralTract.Lj(1)*Ug-Chink.Q(1); 
            Pt = Chink.bj(1).*(Ug+Uch-OralTract.flow_time(2)+Chink.Udj+Chink.V(1:end-1));
            Chink.pressure_time = Pt(:);
            Chink.Vc = 4/T*Chink.Cj.*Pt-Chink.Vc;
            u3 = Chink.Gw.*(Pt+Chink.Qwl-Chink.Qwc);
            Chink.Qwc = 2*Chink.wc.*u3+Chink.Qwc;
            Chink.Qwl = 2*Chink.wl.*u3-Chink.Qwl;          
        case isg %    elseif strcmp(Coup(k).Nat,'subglott')  
            SubGlott.Q(1) = 4*(SubGlott.Lj(1))/T*SubGlott.flow_time(1)-SubGlott.Q(1);  
        otherwise
            Parent = wvg_tmp.parent_wvg(1);
            ncoup = wvg_tmp.parent_point(1);
            if ~isempty(wvg_tmp.twin_wvg)
                Twin = wvg_tmp.twin_wvg;
                wvg_tmp.Q(1) = 4*(wvg_tmp.Lj(1)+Parent.Lj(ncoup))/T*wvg_tmp.flow_time(1)+4*Parent.Lj(ncoup)/T*(Parent.flow_time(ncoup+1)+Twin.flow_time(1))-wvg_tmp.Q(1); 
            else
                wvg_tmp.Q(1) = 4*(wvg_tmp.Lj(1)+Parent.Lj(ncoup))/T*wvg_tmp.flow_time(1)+4*Parent.Lj(ncoup)/T*Parent.flow_time(ncoup+1)-wvg_tmp.Q(1);                
            end 
    end
    
    if k ~= ich 
        Pt(1:end-1) = wvg_tmp.bj(1:end-1).*(wvg_tmp.flow_time(1:end-1)-wvg_tmp.flow_time(2:end)+wvg_tmp.Udj+wvg_tmp.V(1:end-1));
        if k == 1
            Pt(1) = wvg_tmp.bj(1)*(Ug+Uch-wvg_tmp.flow_time(2)+wvg_tmp.Udj(1)+wvg_tmp.V(1));
        end
        wvg_tmp.Q(2:end-1) = 4*(wvg_tmp.Lj(1:end-1)+wvg_tmp.Lj(2:end))/T.*wvg_tmp.flow_time(2:end-1)-wvg_tmp.Q(2:end-1);   
        if ~isempty(ch) 
            for Kq = 1:length(unique(pos_ch))                
                ncoup = pos_ch(Kq);
                sumarray = 0;
                idx = ch(pos_ch == ncoup);
                if ~isempty(idx)
                   for kidx = 1:length(idx)
                       xtmp = obj.waveguide{idx(kidx)}.flow_time(1);
                       sumarray = sumarray+xtmp;                      
                   end
                end               
                wvg_tmp.Qnc(Kq) = 4*(wvg_tmp.Lj(ncoup)+wvg_tmp.Lj(ncoup+1))/T*wvg_tmp.flow_time(ncoup+1)+4/T*wvg_tmp.Lj(ncoup)*sum(sumarray)-wvg_tmp.Qnc(Kq);
                wvg_tmp.Q(ncoup+1) = wvg_tmp.Qnc(Kq);
                Pt(ncoup) = wvg_tmp.bj(ncoup).*(wvg_tmp.flow_time(ncoup)-wvg_tmp.flow_time(ncoup+1)-sum(sumarray)+wvg_tmp.Udj(ncoup)+wvg_tmp.V(ncoup));                
            end            
        end
        
        if ~isempty(wvg_tmp.anabranch_wvg)   
            Anabranch = wvg_tmp.anabranch_wvg;            
            brg = Anabranch.parent_point(2);
            wvg_tmp.Qnc(end) = 4*(wvg_tmp.Lj(brg)+wvg_tmp.Lj(brg+1))/T*wvg_tmp.flow_time(brg+1)+4/T*wvg_tmp.Lj(brg+1)*Anabranch.flow_time(end)-wvg_tmp.Qnc(end);
            wvg_tmp.Q(brg+1) = wvg_tmp.Qnc(end);
            Pt(brg+1) = wvg_tmp.bj(brg+1).*(wvg_tmp.flow_time(brg+1)-wvg_tmp.flow_time(brg+2)+Anabranch.flow_time(end)+wvg_tmp.Udj(brg+1)+wvg_tmp.V(brg+1));
        end
        
        wvg_tmp.pressure_time(1:Nx(k)) = Pt(1:end-1);
        wvg_tmp.Vc = 4/T*wvg_tmp.Cj.*Pt(1:end-1)-wvg_tmp.Vc;
        u3 = wvg_tmp.Gw.*(Pt(1:end-1)+wvg_tmp.Qwl-wvg_tmp.Qwc);
        wvg_tmp.Qwc = 2*wvg_tmp.wc.*u3+wvg_tmp.Qwc;
        wvg_tmp.Qwl = 2*wvg_tmp.wl.*u3-wvg_tmp.Qwl;        
        
        if k == 1
            wvg_tmp.Q(Nx(k)+1) = 4*wvg_tmp.Lj(end)/T*wvg_tmp.flow_time(Nx(k)+1)-wvg_tmp.Q(Nx(k)+1); 
        end
        if k~=1            
            if length(wvg_tmp.parent_wvg) > 1                
                brg = Anabranch.parent_point(2);
                Anabranch = wvg_tmp.anabranch_wvg;
                wvg_tmp.Q(Nx(k)+1) = 4/T*((wvg_tmp.Lj(end)+Anabranch.Lj(brg+1))*wvg_tmp.flow_time(Nx(k)+1)+Anabranch.Lj(brg+1)*Anabranch.flow_time(brg+1))-wvg_tmp.Q(Nx(k)+1);                
            else                
                wvg_tmp.Q(Nx(k)+1) = 4*wvg_tmp.Lj(end)/T*wvg_tmp.flow_time(Nx(k)+1)-wvg_tmp.Q(Nx(k)+1);                
            end            
        end  
        if k ~= isg
            Pt(Nx(k)+1) = wvg_tmp.bj(Nx(k)+1)*(wvg_tmp.flow_time(Nx(k)+1)+wvg_tmp.V(Nx(k)+1));            
        else
            Pt(Nx(k)+1) = wvg_tmp.connection_pressure_inst;
        end
        wvg_tmp.pressure_time(Nx(k)+1) = Pt(Nx(k)+1);
        wvg_tmp.V(Nx(k)+1) = -2*Srad*sqrt(wvg_tmp.area_function(end,Const.k_iter))*Pt(Nx(k)+1)+wvg_tmp.V(Nx(k)+1);    
    else        
        wvg_tmp.pressure_time = OralTract.pressure_time(1);        
    end   
end
