function exportnetwork2json(obj, jsonFileName, scenario_time_sampling)
% exportnetwork2json(obj, jsonFileName, scenario_time_sampling)
% export input data of the VT_Network into json file
%
% Input arguments:
% - obj: VT_Network object
% - jsonFileName: file of the scenario JSON file (.json extension not
% required)
% - scenario_time_sampling: time spacing between each time stamp
%
% Benjamin Elie, 2020
%
% See also VT_Network, VT_Waveguide, MainOralTract, Oscillator, Glottis
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 3; scenario_time_sampling = 20e-3; end

% Check if file has the JSON extension
idx = strfind(jsonFileName, '.json');
if ~isempty(idx)
    jsonFileName = jsonFileName(1:idx-1); 
end

MOT = obj.waveguide{1};
af = MOT.area_function;
lf = MOT.area_function;
newMOT = MainOralTract('area_function', af, ...
    'length_function', lf);
sr = obj.simulation_frequency;
[ nTubes, nFrameI ] = size(af);
tvec = (0:nFrameI-1)/sr;
tres = (0:scenario_time_sampling:tvec(end));
nFrameO = length(tres);

Ntmp = VT_Network('waveguide', {newMOT}, 'oscillators',obj.oscillators, ...
   'subglottal_control', obj.subglottal_control);
Ntmp.inputresample(tres, tvec);

newMOT.exportaf2json([jsonFileName,'_af']);

[ isGlott, ~, idxGlott, ~ ] = searchoscillators(obj);

if isGlott
    Gl = obj.oscillators{idxGlott};
end

id = struct('globalScenario', []);
id.globalScenario = struct('F0Commands',struct('FOInHz',[],'timeInMs',[]),...
    'keyAG0', struct('Ag0Opening', [], 'phoneticLabel', [], 'synthesisTime', [], 'transitionFromPreviousAreaFunction', []), ...
    'keyAreaFunctions', struct('mainAreaFunctionFile', [], 'synthesisTime', [], 'transitionFromPreviousAreaFunction', []),...
    'numberOfTubes', nTubes, ...
    'numberOfKeyAG0Commands', nFrameO, 'numberOfKeyAreaFunctions', nFrameO, ...
    'segmentation', struct('beginsAtInMs', [], 'endsAtInMs', [], 'sampaCode', []));

% write F0
pab = Gl.partial_abduction;
f0 = Gl.fundamental_frequency;
if ~isempty(obj.phonetic_label)
    labels = obj.phonetic_label;
    start_time = obj.phonetic_instant(:,1);
    stop_time = obj.phonetic_instant(:,2);
end

disp('Writing scenario file')
for k = 1:nFrameO
    % find phoneme
    idx = find(start_time<=tres(k), 1, 'last');    
   afFile = [jsonFileName,'_af_', num2str(k, '%.5d'), '.json'];
   id.globalScenario.F0Commands(k) = struct('FOInHz',f0(k), ...
       'timeInMs', tres(k)*1e3);
   id.globalScenario.keyAG0(k) = struct('Ag0Opening',pab(k), ...
       'phoneticLabel', labels(idx), 'synthesisTime', tres(k)*1e3, ...
       'transitionFromPreviousAreaFunction', 'COS');
   id.globalScenario.keyAreaFunctions(k) = struct('mainAreaFunctionFile',afFile, ...
       'synthesisTime', tres(k)*1e3, ...
       'transitionFromPreviousAreaFunction', 'COS');   
end
id.globalScenario.keyAreaFunctions(1).transitionFromPreviousAreaFunction = '***';
id.globalScenario.keyAG0(1).transitionFromPreviousAreaFunction = 'SET';

if ~isempty(obj.phonetic_label)
    for k = 1:length(labels)
        id.globalScenario.segmentation(k) = struct('beginsAtInMs',start_time(k)*1e3, ...
           'endsAtInMs', stop_time(k)*1e3, ...
           'sampaCode', labels(k));
    end
end

txt = jsonencode(id);
fileJSON = [ jsonFileName, '.json' ];
fid = fopen(fileJSON,'w');
fwrite(fid,txt,'char');
fclose(fid);
disp('Scenario file written')

end

