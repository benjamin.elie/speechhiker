function outputresample(obj, sro, sri)
% outputresample(obj, tvec_o, tvec_i)
% Resample output parameters of the VT_Network object
% 
% Input arguments :
% - obj: network object
% - sro: output sampling frequency
% - sri: input sampling frequency
%
% Benjamin Elie, 2020
%
% See also VT_Network
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if sro >= sri
    warning('The output frequency should be less than the input frequency')
    return 
else
    obj.pressure_radiated = resample(obj.pressure_radiated, sro, sri, 30);
    obj.subglottal_control = resample(obj.subglottal_control, sro, sri, 30);
    nPipe = length(obj.waveguide);    
    for kPipe = 1:nPipe
        wvg_tmp = obj.waveguide{kPipe};
        ptmp = wvg_tmp.pressure_time_matrix;
        utmp = wvg_tmp.flow_time_matrix;
        ptmp = resample(ptmp.',sro, sri, 30).';
        utmp = resample(utmp.',sro, sri, 30).';
        wvg_tmp.pressure_time_matrix = ptmp;
        wvg_tmp.flow_time_matrix = utmp;    
        switch kPipe
            case 1
                wvg_tmp.glottal_flow_vector = ...
                    resample(wvg_tmp.glottal_flow_vector, sro, sri, 30);
                wvg_tmp.reynolds_vector = ...
                    resample(wvg_tmp.reynolds_vector, sro, sri, 30);
            case obj.whereChink
                wvg_tmp.glottal_flow_vector = ...
                    resample(wvg_tmp.glottal_flow_vector, sro, sri, 30);
        end 
        afi = wvg_tmp.area_function;
        lfi = wvg_tmp.length_function;
        wvg_tmp.area_function = resample(afi.', sro, sri, 30).';
        wvg_tmp.length_function = resample(lfi.', sro, sri, 30).';
    end
    
    nOscill = length(obj.oscillators);
    for kOscill = 1:nOscill
       oscill_tmp = obj.oscillators{kOscill}; 
%        oscill_tmp.mass_position_matrix = ...
%            resample(oscill_tmp.mass_position_matrix.',sro, sri, 30).';
       oscill_tmp.height_vector_output = ...
           resample(oscill_tmp.height_vector_output.',sro, sri, 30).';
%        oscill_tmp.mass_matrix = ...
%            resample(oscill_tmp.mass_matrix.',sro, sri, 30).';
%        oscill_tmp.stiffness_matrix = ...
%            resample(oscill_tmp.stiffness_matrix.',sro, sri, 30).';
%        oscill_tmp.coupling_stiffness_matrix = ...
%            resample(oscill_tmp.coupling_stiffness_matrix.',sro, sri, 30).';
%        oscill_tmp.damping_matrix = ...
%            resample(oscill_tmp.damping_matrix.',sro, sri, 30).';
       oscill_tmp.fundamental_frequency = ...
           resample(oscill_tmp.fundamental_frequency,sro, sri, 30);
       oscill_tmp.low_frequency_abduction = ...
           resample(oscill_tmp.low_frequency_abduction,sro, sri, 30);
       oscill_tmp.partial_abduction = ...
           resample(oscill_tmp.partial_abduction,sro, sri, 30);
       oscill_tmp.output_length = ...
           resample(oscill_tmp.output_length,sro, sri, 30);
       if kOscill == obj.whereGlott
           oscill_tmp.DC_vector = ...
           resample(oscill_tmp.DC_vector,sro, sri, 30);
       end
    end
    obj.simulation_frequency = sro;
end

