function inputresample(obj, tvec_o, tvec_i)
% inputresample(obj, tvec_o, tvec_i)
% Resample input parameters of the VT_Network object
% 
% Input arguments :
% - obj: network object
% - tvec_o: output time vector 
% - tvec_i: input time_vector
%
% Benjamin Elie, 2020
%
% See also VT_Network
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


wvg = obj.waveguide;
oscill = obj.oscillators;
isGlott = searchoscillators(obj);
sro = 1/median(diff(tvec_o));

% isGlott = obj.isGlott;

for k = 1:length(wvg)
    wvg_tmp = wvg{k};
    af = wvg_tmp.area_function;
    lf = wvg_tmp.length_function;
    af = interp1(tvec_i, af.', tvec_o).';
    lf = interp1(tvec_i, lf.', tvec_o).';
    wvg_tmp.area_function = af;
    wvg_tmp.length_function = lf;
end

if isGlott
    whereGlott = obj.whereGlott;
    objGlottis = oscill{whereGlott};
    
    f0 = objGlottis.fundamental_frequency;
    pa = objGlottis.partial_abduction; 
    
    f0 = interp1(tvec_i, f0, tvec_o);    
    pa = interp1(tvec_i, pa, tvec_o);    
    
    objGlottis.fundamental_frequency = f0;	  
    objGlottis.partial_abduction = pa; 
    
    if ~isempty(objGlottis.low_frequency_abduction)
        lfa = objGlottis.low_frequency_abduction;   
        lfa = interp1(tvec_i, lfa, tvec_o);
        objGlottis.low_frequency_abduction = lfa; 
    end    
else
    obj.input_area = interp1(tvec_i, obj.input_area, tvec_o);
    ag0 = obj.abduction;
    ag0 = interp1(tvec_i, ag0, tvec_o);
    objGlottis = Glottis;
    objGlottis.partial_abduction = ag0/objGlottis.max_opening;
    objGlottis.length = 0.022;
end

obj.subglottal_control = interp1(tvec_i, obj.subglottal_control, tvec_o);
obj.simulation_frequency = sro;

end

