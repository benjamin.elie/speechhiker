function writeoutputmatrix(obj, k)
% writeoutputmatrix(obj, k)
% update the output pressure and flow matrix at the iteration k
% 
% Input arguments:
% - obj: network object
% - k: iteration step
%
% Benjamin Elie, 2020
%
% See also VT_Network, VT_Waveguide, Oscillator, esmfsynthesis
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

nPipe = length(obj.waveguide);
for kPipe = 1:nPipe
    wvg_tmp = obj.waveguide{kPipe};
    wvg_tmp.pressure_time_matrix(:,k) = wvg_tmp.pressure_time;
    wvg_tmp.flow_time_matrix(:,k) = wvg_tmp.flow_time;
    switch kPipe
        case 1
            wvg_tmp.glottal_flow_vector(k) = ...
                wvg_tmp.glottal_flow_inst;
            wvg_tmp.reynolds_vector(k) = ...
                wvg_tmp.reynolds_inst;
        case obj.whereChink
            wvg_tmp.glottal_flow_vector(k) = ...
                wvg_tmp.glottal_flow_inst;
    end
end

nOscill = length(obj.oscillators);
for kOscill = 1:nOscill
    oscill_tmp = obj.oscillators{kOscill};
    oscill_tmp.mass_position_matrix(:,:,k) =  oscill_tmp.mass_position;
    oscill_tmp.height_vector_output(:,k) = ...
        [oscill_tmp.height_vector(:); oscill_tmp.separation_height];
    oscill_tmp.mass_matrix(:,:,k) =  oscill_tmp.mass;
    oscill_tmp.stiffness_matrix(:,:,k) = oscill_tmp.stiffness;
    oscill_tmp.damping_matrix(:,:,k) = oscill_tmp.damping;
    oscill_tmp.coupling_stiffness_matrix(:,:,k) = oscill_tmp.coupling_stiffness;
    if kOscill == obj.whereGlott
        oscill_tmp.DC_vector(k) = oscill_tmp.DC_flow;
    end
end
end


