classdef VT_Network < handle
% VT_Network: class modeling the whole vocal production system. VT_Network is
% used for running speech synthesis
% 
% Properties of VT_Network objects are:
% - waveguide: cell array containing the VT_Waveguide objects that model the
% vocal tract nework
% - oscillators: cell array containing the oscillators in the vocal tract (e.g. the glottis, the tongue...)     
% - subglottal_control: vector containing the input value of the subglottal pressure for each
% simulation time step
% - phonetic_label: cell array containing the phonetic labels of the
% desired simulated utterance
% - phonetic_instant: array containing the time instants of the onset and
% offset of each phoneme of the desired simulated utterance
% - abduction: vector containing the low frequency abduction of the glottis
% - pressure_radiated: vector containing the acoustic pressure radiated at
% the lips (and potentially at the nostrils)
% - network_transfer_function: transfer function of the whole system
% - force_vector: vector of instantaenous acoustic forces in the vocal
% tract
% - esmf_matrix: acoustic matrix used to solve the esmf equations
% - flow_inst: instantaneous acoustic flow in the vocal tract
% - input_area: ??
% - radiation_position: indices of waveguides that radiate
% - number_tubes: vector containing the number of tubes of each waveguide
% - isChink: true if a glottal chink is connected to the vocal tract and
% the glottis
% - isSubGlott: true if a subglottal system is connected to the vocal tract
% - isGlott: true if an oscillating glottis is connected to the vocal tract
% - isSupOscill: true if a supraglottal oscillator is connected to the vocal tract
%
% Methods of VT_Network objects are
% - esmfsynthesis: running speech synthesis
% - inputresample: time-domain resampling of the input data
% - outputresample: time-domain resampling of the output data
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide, MainOralTract, GlottalChink, SubGlottalTract
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    
    properties
        %%% Input        
        waveguide = [];
        oscillators = [];        
        subglottal_control = [];
        phonetic_label = [];
        phonetic_instant = [];
        abduction = [];
        simulation_frequency = [];
        
        %%% Output
        pressure_radiated = [];
        network_transfer_function = [];
    end
    
    properties (Hidden = true)
        force_vector = [];
        esmf_matrix = [];
        flow_inst = [];
        input_area = [];
        radiation_position = [];
        number_tubes = [];        
        isChink = false;
        isSubGlott = false;
        isGlott = false;
        isSupOscill = false;
        whereChink = [];
        whereSubGlott = [];
        whereGlott = [];
        whereSupOscill = [];   
    end
    
    methods
         % Constructor method
        function obj = VT_Network(varargin)
            for k = 1:2:length(varargin)
                field = varargin{k};
                argvalue = varargin{k+1};
                if isprop(obj,field)
                    obj.(field) = argvalue;
                end % if isprop
            end % for k ...
        end % end function
        
        [ isChink, isSubGlott, idxChink, idxSubGlott ] = searchchinkandsubglott(obj)        
        [ isGlott, isSupOscill, idxGlott, idxSupOscill ] = searchoscillators(obj)        
        [ transFun, freq ] = computetransferfunction(obj, param, meth)               
        [ esmfMtx, forceVector ] = esmfmatrix( obj )        
        [ Ut, Ug, Uch, po ] = esmfmtxsolver(obj, bool)        
        updateacoustics(obj, Const)  
        [ Pout, obj_out ] = esmfsynthesis(obj, Const)        
        inputresample(obj, tvec_o, tvec_i)        
        outputresample(obj, fso, fsi)        
        writeoutputmatrix(obj, k)        
        h_handle = plotinputparameters(obj)
        h_handle = plotoutputparameters(obj)
        esmftransferfunction(obj, Const, slices)
        exportnetwork2json(obj, jsonFileName, scenario_time_sampling)
        pout = rtlasynthesis(obj, Const)
        
    end
end

