function [ transFun, freq ] = computetransferfunction(obj, param, meth)
% [ transFun, freq ] = computetransferfunction(obj, param)
% returns the transfer function and the impulse response of the VT_Network
% object. Use The chain matrix paradigm
% MAY BE VERY BUGGY AT THE MOMENT... CHECK FOR UPDATES
% 
% Input arguments :
% - obj: network object
% - param: acoustic constant terms
%
% Output arguments :
% - transFun : complex transfer function of the VT_Network object
% - freq : vector containing the frequency points, in Hz, for which the TF
% has been computed
%
% 
% Benjamin Elie, 2020
%
% See also Waveguide, esmfsynthesis, VT_Network
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    if nargin < 2; param = []; end
    if ~isfield(param,'rho'); param.rho = 1.204/1000; end %Volumic Mass of the air; %Volumic Mass of the air
    if ~isfield(param,'c'); param.c = 343.4; end %Sound celerity in the air
    if ~isfield(param,'mu'); param.mu = 1.9831e-5; end
    if ~isfield(param,'a'); param.a = 130*pi; end
    if ~isfield(param,'b'); param.b = (30*pi)^2; end
    if ~isfield(param,'c1'); param.c1 = 4; end
    if ~isfield(param,'wo2'); param.wo2 = (406*pi)^2; end
    if ~isfield(param,'c1n'); param.c1n = 72; end
    if ~isfield(param,'heat_cond'); param.heat_cond = 0.0034; end
    if ~isfield(param,'specific_heat'); param.specific_heat = 160; end
    if ~isfield(param,'adiabatic'); param.adiabatic = 1.4; end
    if ~isfield(param,'wallyield'); param.wallyield = true; end
    if ~isfield(param,'loss'); param.loss = true; end
    if ~isfield(param,'freq'); param.freq = 0:50:5000; end %Vector of frequencies
    if nargin < 3; meth = 'cmp'; end
        
    param.freq(param.freq<=1e-11) = 1e-11;
    freq = param.freq(:);    
    w = 2*pi*freq; % angular frequency
    lw = length(w);
    
    if strcmpi(meth, 'cmp')
        param.alp = sqrt(1i*w*param.c1);
        param.bet = 1i*w*param.wo2./((1i*w+param.a)*1i.*w+param.b)+param.alp;
        param.gam = sqrt((param.alp+1i*w)./(param.bet+1i*w));
        param.sig = param.gam.*(param.bet+1i*w);    
    end
    
    Nwave = length(obj.waveguide);
    aftmp = obj.waveguide{1}.area_function; % to make it from the lips to the glottis
%     
    nframe = size(aftmp,2);
    transFun = zeros(lw, nframe);
    
    for kf = 1:nframe        
        for kw = Nwave:-1:1            
            % Check if waveguide has a child
            objtmp = obj.waveguide{kw};
            
            if ~isa(objtmp, 'GlottalChink') && ~isa(objtmp, 'SubGlottalTract')

                Avt = objtmp.area_function(:,kf);
                lvt = objtmp.length_function(:,kf);
                Zrad = param.rho*(2*pi*freq).^2/2/pi/param.c+1i*8*param.rho*(2*pi*freq)/3/pi^(3/2)*Avt(end)^(-1/2);

                if isempty(objtmp.child_wvg)        
                    [ A, B, C, D ] = ChainMatrix(Avt, lvt, freq, param, meth);
                    objtmp.A = A;
                    objtmp.B = B;
                    objtmp.C = C;
                    objtmp.D = D;
                    objtmp.Zin = (D.*Zrad(:)-B)./(-C.*Zrad(:)+A);    
                else       
                    kn = objtmp.child_point;
                    if isempty(objtmp.anabranch_wvg)
                        child = objtmp.child_wvg; 
                        [ ~, ~, ~, ~, Tf ] = ChainMatrix(Avt(kn:end), lvt(kn:end), freq, param, meth);                    
                        Acoup(1,1,:) = ones(size(Zrad));
                        Bcoup(1,1,:) = zeros(size(Zrad));
                        Ccoup(1,1,:) = -1./child.Zin;
                        Tn = [ Acoup Bcoup; Ccoup Acoup ];
                        Tf = ProdMat3D( Tf, Tn );
                        [ A, ~, C, ~, ~ ] = ChainMatrix(Avt(1:kn-1), lvt(1:kn-1), freq, param, meth, Tf);
                    else
                        anabranch = objtmp.anabranch_wvg;
    %                     afTmp = [ Avt(kn(1)-1); anabranch.area_function(:,kf); Avt(kn(2)+1) ];
    %                     lfTmp = [ lvt(kn(1)-1); anabranch.length_function(:,kf); lvt(kn(2)+1) ];
                        afTmp = anabranch.area_function(:,kf);
                        lfTmp = anabranch.length_function(:,kf);
                        [ A_ana, B_ana, C_ana, D_ana, ~ ] = ChainMatrix(afTmp, lfTmp, freq, param, meth); 
                        [ ~, ~, ~, ~, Tf ] = ChainMatrix(Avt(kn(2)+1:end), lvt(kn(2)+1:end), freq, param, meth);                   
                        [ Atmp, Btmp, Ctmp, Dtmp, ~ ] = ChainMatrix(Avt(kn(1):kn(2)), lvt(kn(1):kn(2)), freq, param, meth);  
                        A_coup = (Atmp.*B_ana+A_ana.*Btmp)./(Btmp+B_ana);
                        B_coup = (Btmp.*B_ana)./(Btmp+B_ana);
                        C_coup = Ctmp+C_ana-(Atmp-A_ana).*(Dtmp-D_ana)./(Btmp+B_ana);
                        D_coup = (Dtmp.*B_ana+D_ana.*Btmp)./(Btmp+B_ana);
                        Tn_coup = zeros(2,2,lw);
                        Tn_coup(1,1,:) = A_coup;
                        Tn_coup(1,2,:) = B_coup;
                        Tn_coup(2,1,:) = C_coup;
                        Tn_coup(2,2,:) = D_coup;
                        Tf = ProdMat3D( Tf, Tn_coup );  
                        [ A, ~, C, ~, ~ ] = ChainMatrix(Avt(1:kn(1)-1), lvt(1:kn(1)-1), freq, param, meth, Tf); 
                    end                    
                end        
            end % if not Chink or subglott
        end % for kf = 1:nframe    

        Hf = 1./(-C.*Zrad(:)+A);
        transFun(:,kf) = Hf;
    end

    obj.network_transfer_function = transFun;
%     obj.radiation_impedance = Zrad;
%     obj.freq = freq;
end % end function