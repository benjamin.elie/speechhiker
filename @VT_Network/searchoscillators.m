function [ isGlott, isSupOscill, idxGlott, idxSupOscill ] = searchoscillators(obj)
% [ isGlott, isSupOscill, idxGlott, idxSupOscill ] = searchoscillators(obj)
% check if glottal and supraglottal oscillators are connected to the
% network. Returns also their position in the network
% 
% Input argument:
% - obj: network object
%
% Output arguments:
% - isGlott: boolean that is true if a Glottis object is connected in
% the VT_Network object
% - isSupOscill: boolean that is true if a supraglottal oscillator 
% (as a Tongue object) is connected in the VT_Network object
% - idxGlott: position of the Glottis object in the oscillator
% property of the VT_Network object
% - idxSupOscill: position of the supraglottal oscillator in the oscillator
% property of the VT_Network object
%
% Benjamin Elie, 2020
%
% See also VT_Network, Oscillator, Glottis, Tongue, esmfsynthesis
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

nOscill = length(obj.oscillators);
isGlott = false;
isSupOscill = false;
idxGlott = [];
idxSupOscill = [];
% check for chink or subglottal pressure
for k = 1:nOscill
    oscill_tmp = obj.oscillators{k};
    if isa(oscill_tmp,'Glottis')
        isGlott = true;
        idxGlott = [idxGlott, k];
    end
    if isa(oscill_tmp,'Tongue')
        isSupOscill = true;
        idxSupOscill = [ idxSupOscill, k ];
    end    
end

obj.isGlott = isGlott;
obj.isSupOscill = isSupOscill;
obj.whereGlott = idxGlott;
obj.whereSupOscill = idxSupOscill;

end