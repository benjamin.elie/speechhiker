function h_handle = plotoutputparameters(obj)
% h_handle = plotoutputparameters(obj)
% plot the output parameters of the VT_Network object
%
% Input argument:
% - obj: VT_Network object
%
% Output argument: 
% - h_handle: figure object
%
% Benjamin Elie, 2020
%
% See also VT_Network
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% get the output parameters
sr = obj.simulation_frequency;
[chbool, sgbool, ich, isg ] = searchchinkandsubglott(obj);
MOT = obj.waveguide{1};
P = MOT.pressure_time_matrix;
Ug = MOT.glottal_flow_vector;
[ nTubes, nFrame ] = size(MOT.area_function);
tvec = (0:nFrame-1)/sr;

Gl = obj.oscillators{1};
lg = Gl.output_length;

sig_mass_1 = Gl.height_vector_output(2,:);
sig_mass_2 = Gl.height_vector_output(3,:);
openarea = min(sig_mass_1, sig_mass_2);
openarea(openarea<=0) = 0;
sig_mass_1(sig_mass_1<=0) = 0;
sig_mass_2(sig_mass_2<=0) = 0;
area1 = sig_mass_1(:).*lg(:);
area2 = sig_mass_2(:).*lg(:);
openarea = openarea(:).*lg(:);

if chbool
    Chink = obj.waveguide{ich};
    chinkArea = Chink.area_function(:);
    area1 = area1 + chinkArea;
    area2 = area2 + chinkArea;
    openarea = openarea + chinkArea;
end

instPoral = P(round(3*nTubes/4), :);
lwin = ceil(0.1*sr);
Poral = smooth(instPoral, lwin, 'loess');

% Plot output parameters
h_handle = figure; 
h(1)=subplot(311); 
pl(1) = plot(tvec, Ug, '--b', 'linewidth',2); hold on; title('Glottal flow')
hold on; 
set(gca,'ticklabelinterpreter','latex')
ylabel('Glottal flow (m\textsuperscript{3}/s)')
maxy = max(Ug)*1.2;

if chbool   
    Uch = Chink.flow_time_matrix;
    Ut = Ug+Uch;
    maxy = max(Ut)*1.2;
    pl(2)=  plot(tvec, Uch, '--','color',[0 0.498 0],'linewidth',2);
    pl(3)=  plot(tvec, Ut, 'r','linewidth',2);
    
end
for k = 1:length(obj.phonetic_label)
    plot(obj.phonetic_instant(k,2)*ones(2,1), [ -1 1 ], '--','color',[ 0 0.498 0 ],'linewidth',1)
     text(mean(obj.phonetic_instant(k,:)),maxy,obj.phonetic_label{k}, ...
        'color',[ 0 0.498 0 ],'fontsize',14,'fontweight','bold',...
        'horizontalalignment','center','verticalalignment','top')
end
if chbool
    leg1 = legend(pl,'Oscillating part','Through the chink', 'Through the whole glottis', 'Location','Best');
    set(leg1,'interpreter','latex')
end
ylim([0 maxy ])

h(2)=subplot(312); 
plot(tvec, area1, 'b', 'linewidth',2); hold on; title('Glottal opening area')
hold on; 
plot(tvec, area2, 'r', 'linewidth',2); 
plot(tvec, openarea, 'color',[0 0.498 0 ], 'linewidth',2); 

set(gca,'ticklabelinterpreter','latex')
ylabel('Glottal opening area (m\textsuperscript{2})')
maxy = max(openarea)*1.2;

for k = 1:length(obj.phonetic_label)
    plot(obj.phonetic_instant(k,2)*ones(2,1), [ -1 1 ], '--','color',[ 0 0.498 0 ],'linewidth',1)
     text(mean(obj.phonetic_instant(k,:)),maxy,obj.phonetic_label{k}, ...
        'color',[ 0 0.498 0 ],'fontsize',14,'fontweight','bold',...
        'horizontalalignment','center','verticalalignment','top')
end

leg2 = legend('Mass 1','Mass 2', 'Glottal opening area', 'Location','Best');
set(leg2,'interpreter','latex')

ylim([0 maxy ])

h(3)=subplot(313); plot(tvec, Poral,'linewidth',2);title('Intraoral pressure')
maxy = max(Poral)*1.2;
hold on; 
for k = 1:length(obj.phonetic_label)
    plot(obj.phonetic_instant(k,2)*ones(2,1), [ -1 1e3 ], '--','color',[ 0 0.498 0 ],'linewidth',1)
	text(mean(obj.phonetic_instant(k,:)),maxy,obj.phonetic_label{k}, ...
        'color',[ 0 0.498 0 ],'fontsize',14,'fontweight','bold',...
        'horizontalalignment','center','verticalalignment','top')
end
set(gca,'ticklabelinterpreter','latex')
ylabel('$P_{\mathrm{Oral}}$ (Pa)')
xlabel('Time (s)')
ylim( [0 maxy ])

linkaxes(h,'x');
xlim([0 tvec(end)])
set(gcf,'units','normalized','position',[0 0 1 1])

end

