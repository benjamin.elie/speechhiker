function h_handle = plotinputparameters(obj)
% h_handle = plotinputparameters(obj)
% plot the input parameters of the VT_Network object
%
% Input argument
% - obj: VT_Network object
%
% Output argument: 
% - h_handle: figure object
%
% Benjamin Elie, 2020
%
% See also VT_Network
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% get the input parameters
sr = obj.simulation_frequency;
[chbool, sgbool, ich, isg ] = searchchinkandsubglott(obj);
psub = obj.subglottal_control;
MOT = obj.waveguide{1};
ac = min(MOT.area_function);
nFrame = length(ac);
tvec = (0:nFrame-1)/sr;
fo = obj.oscillators{1}.fundamental_frequency;

% Plot Input parameters
h_handle = figure; 
h(1)=subplot(311); pl(1) = plot(tvec, ac, 'linewidth',2); hold on; title('Miniminal cross-section area')
hold on; 
set(gca,'ticklabelinterpreter','latex')
ylabel('Area (m\textsuperscript{2})')
maxy = max(ac)*1.2;
for k = 1:length(obj.phonetic_label)
    plot(obj.phonetic_instant(k,2)*ones(2,1), [ -1 1 ], '--','color',[ 0 0.498 0 ],'linewidth',1)
     text(mean(obj.phonetic_instant(k,:)),maxy,obj.phonetic_label{k}, ...
        'color',[ 0 0.498 0 ],'fontsize',14,'fontweight','bold',...
        'horizontalalignment','center','verticalalignment','top')
end
if chbool
    Chink = obj.waveguide{ich};
    pl(2)=  plot(tvec, Chink.area_function, 'r','linewidth',2);
    leg1 = legend(pl,'Supraglottal tract','Glottal opening','Location','Best');
    set(leg1,'interpreter','latex')
end
ylim([0 maxy ])

h(2)=subplot(312); plot(tvec, fo,'linewidth',2);title('Fundamental frequency')
hold on; 
for k = 1:length(obj.phonetic_label)
    plot(obj.phonetic_instant(k,2)*ones(2,1), [ -1 1e3 ], '--','color',[ 0 0.498 0 ],'linewidth',1)
	text(mean(obj.phonetic_instant(k,:)),100*ceil(max(fo)/100),obj.phonetic_label{k}, ...
        'color',[ 0 0.498 0 ],'fontsize',14,'fontweight','bold',...
        'horizontalalignment','center','verticalalignment','top')
end
set(gca,'ticklabelinterpreter','latex')
ylabel('$F_0$ (Hz)')
xlabel('Time (s)')
ylim( [0 100*ceil(max(fo)/100) ])
h(3) = subplot(313); plot(tvec, psub,'linewidth',2);title('Subglottal pressure')
hold on; 
for k = 1:length(obj.phonetic_label)
    plot(obj.phonetic_instant(k,2)*ones(2,1), [ -1 1e4 ], '--','color',[ 0 0.498 0 ],'linewidth',1)
    text(mean(obj.phonetic_instant(k,:)),100*ceil(max(obj.subglottal_control)/100),obj.phonetic_label{k}, ...
        'color',[ 0 0.498 0 ],'fontsize',14,'fontweight','bold',...
        'horizontalalignment','center','verticalalignment','top')
end
set(gca,'ticklabelinterpreter','latex')
ylabel('$P_{Sub}$ (Pa)')
xlabel('Time (s)')
ylim([0 100*ceil(max(psub)/100)])
% h(4) = subplot(414); plot(tvec, inparam.fo)
linkaxes(h,'x');
xlim([0 tvec(end)])
set(gcf,'units','normalized','position',[0 0 1 1])

end

