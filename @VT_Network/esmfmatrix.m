function [ esmfMtx, forceVector ] = esmfmatrix( obj )
% [ esmfMtx, forceVector ] = esmfmatrix( obj )
% returns the ESMF matrix Z and the force vector f containing the acoustic
% impedances and losses, the coupling terms between waveguides, and the acoustic forces
% Step to solve the acoustic flow u in the network, given by the system f = Zu
% 
% Input argument:
% - obj: VT_Network object
%
% Output arguments:
% - esmfMtx: ESMF matrix Z (see eq.3 of the paper [1])
% - forcevector: vector of acoustic forces f (see eq.3 of the paper [1])
%
% [1] Elie B., and Laprie Y. "Extension of the single-matrix formulation 
% of the vocal tract: consideration of bilateral channels and connection 
% of self-oscillating models of the vocal folds with a glottal chink". 
% Speech Comm. 82, pp. 85-96 (2016)
% 
% Benjamin Elie, 2020
%
% See also esmfsynthesis, esmfmtxsolver, VT_Network
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

OralTract = obj.waveguide{1};
Ps = OralTract.subglottal_pressure;
b_eq = OralTract.glottal_resistance;
Lg = OralTract.glottal_inductance;
T = 1/obj.simulation_frequency;

nPipe = length(obj.waveguide);
Nx = obj.number_tubes;

sgbool = obj.isSubGlott;
chbool = obj.isChink;
isg = obj.whereSubGlott; 
ich = obj.whereChink;

for k = 1:nPipe
    if isa(obj.waveguide{k},'GlottalChink')
        Chink = obj.waveguide{k};
        Hch = -(OralTract.bj(1)+OralTract.Rj(1)+2*OralTract.Lj(1)/T+OralTract.Rcm(1));      
        H1_ch = -(Chink.bj(1)*2+OralTract.bj(1)+Chink.Rj(1)+OralTract.Rj(1)+2*(Chink.Lj(1)+OralTract.Lj(1))/T);        
    end 
    if isa(obj.waveguide{k},'SubGlottalTract')
       SubGlott = obj.waveguide{k};
       H1_sg = -(2/T*SubGlott.Lj(1)+SubGlott.Rj(1)+SubGlott.bj(1)+SubGlott.Rcm(1));
       Hsg = -(2/T*SubGlott.Lj(Nx(k))+SubGlott.Rj(Nx(k))+SubGlott.bj(Nx(k))+SubGlott.Rcp(Nx(k)));  
       SubGlott.connection_impedance = Hsg;
    end 
end
nspipe = chbool+sgbool;

if chbool && sgbool      
    Fch = OralTract.bj(1)*(OralTract.Udj(1)+OralTract.V(1))-Chink.Q(1)+Chink.Ns(1);
elseif chbool && ~sgbool
    Fch = OralTract.bj(1)*(OralTract.Udj(1)+OralTract.V(1))-Ps-Chink.Q(1)+Chink.Ns(1);
end
 
Ft = zeros(sum(Nx+1)-chbool,1);
Wont = zeros(sum(Nx+1)-chbool);
W = [];

for k = 1:nPipe
    switch k
        case 1
            Fj = zeros(Nx(k)+1,1);
            OralTract.V(1:Nx(k)) = OralTract.Vc-OralTract.Gw.*(OralTract.Qwl-OralTract.Qwc);
            Fj(2:Nx(k)) = -OralTract.bj(1:end-2).*(OralTract.Udj(1:end-1)+OralTract.V(1:end-2))+OralTract.bj(2:end-1).*(OralTract.Udj(2:end)+OralTract.V(2:end-1))-OralTract.Q(2:end-1)+OralTract.Ns(2:Nx(k))-OralTract.Ns(3:Nx(k)+1);
            Fj(Nx(k)+1) = -OralTract.bj(Nx(k))*(OralTract.Udj(Nx(k))+OralTract.V(Nx(k)))+OralTract.bj(Nx(k)+1)*OralTract.V(Nx(k)+1)-OralTract.Q(Nx(k)+1)+OralTract.Ns(Nx(k)+1);
            OralTract.Hj(1) = -b_eq-OralTract.bj(1);
            if sgbool
                Fj(1) = OralTract.bj(1)*(OralTract.Udj(1)+OralTract.V(1))-OralTract.Q(1)+OralTract.Ns(1)-OralTract.Ns(2);%+a_eq*Ugim1^2;
            else
                Fj(1) = OralTract.bj(1)*(OralTract.Udj(1)+OralTract.V(1))-Ps-OralTract.Q(1)+OralTract.Ns(1)-OralTract.Ns(2);
            end
        case ich
            Chink.Hj(1) = H1_ch;
            Fj = Fch;
            Wont(1,end) = Hch;
            Wont(2,end) = OralTract.bj(1);
            Wont(end,2) = OralTract.bj(1);
            Wont(end,1) = Hch;
        case isg
            Fj = zeros(Nx(k)+1,1);
            SubGlott.V(1:Nx(k)) = SubGlott.Vc-SubGlott.Gw.*(SubGlott.Qwl-SubGlott.Qwc);
            Fj(2:Nx(k)) = -SubGlott.bj(1:end-2).*(SubGlott.Udj(1:end-1)+SubGlott.V(1:end-2))+SubGlott.bj(2:end-1).*(SubGlott.Udj(2:end)+SubGlott.V(2:end-1))-SubGlott.Q(2:end-1)+SubGlott.Ns(2:Nx(k))-SubGlott.Ns(3:Nx(k)+1);
            SubGlott.Hj(1) = H1_sg;
            SubGlott.Hj(end) = -1;
            Fj(1) = SubGlott.bj(1)*(SubGlott.Udj(1)+SubGlott.V(1))-Ps-SubGlott.Q(1)+SubGlott.Ns(1)-SubGlott.Ns(2);
            Fj(end) = -SubGlott.bj(Nx(k))*(SubGlott.Udj(Nx(k))+SubGlott.V(Nx(k)))-SubGlott.Q(Nx(k)+1);
            Wont(1,end-chbool) = 1;
            Wont(end-nspipe,1) = SubGlott.bj(Nx(k));
            Wont(end-chbool,1) = Hsg;
            if chbool
                Wont(end-2,end) = SubGlott.bj(Nx(k));
                Wont(end-1,end) = Hsg;
                Wont(end,end-1) = 1;
            end
        otherwise
            PipeTmp = obj.waveguide{k};
            Parent = PipeTmp.parent_wvg(1);
            nParent = Parent.norder;
            ncoup = PipeTmp.parent_point(1);
            Fj = zeros(Nx(k)+1,1);
            PipeTmp.V(1:Nx(k)) = PipeTmp.Vc-PipeTmp.Gw.*(PipeTmp.Qwl-PipeTmp.Qwc);
            Fj(2:Nx(k)) = -PipeTmp.bj(1:end-2).*(PipeTmp.Udj(1:end-1)+PipeTmp.V(1:end-2))+PipeTmp.bj(2:end-1).*(PipeTmp.Udj(2:end)+PipeTmp.V(2:end-1))-PipeTmp.Q(2:end-1)+PipeTmp.Ns(2:Nx(k))-PipeTmp.Ns(3:Nx(k)+1);
            Fj(Nx(k)+1) = -PipeTmp.bj(Nx(k))*(PipeTmp.Udj(Nx(k))+PipeTmp.V(Nx(k)))+PipeTmp.bj(Nx(k)+1)*PipeTmp.V(Nx(k)+1)-PipeTmp.Q(Nx(k)+1)+PipeTmp.Ns(Nx(k)+1);
            Fj(1) = PipeTmp.bj(1)*(PipeTmp.Udj(1)+PipeTmp.V(1))-Parent.bj(ncoup)*(Parent.Udj(ncoup)+Parent.V(ncoup))-PipeTmp.Q(1)+PipeTmp.Ns(1)-PipeTmp.Ns(2);
            PipeTmp.Hj(1) = -(2*(PipeTmp.Lj(1)+Parent.Lj(ncoup))/T+PipeTmp.Rj(1)+PipeTmp.Rcm(1)+Parent.Rj(ncoup)+Parent.Rcp(ncoup)+PipeTmp.bj(1)+Parent.bj(ncoup));
            Hnc = -(Parent.bj(ncoup)+Parent.Rj(ncoup)+2*Parent.Lj(ncoup)/T+Parent.Rcp(ncoup));
            Wont(ncoup+1+(nParent-1)*sum(Nx(1:nParent)+1),sum(Nx(1:k-1)+1)+1) = Hnc;
            Wont(sum(Nx(1:k-1)+1)+1,ncoup+1+(nParent-1)*sum(Nx(1:nParent)+1)) = Hnc;
            Wont(sum(Nx(1:k-1)+1)+1,ncoup+(nParent-1)*sum(Nx(1:nParent)+1)) = Parent.bj(ncoup);
            Wont(ncoup+(nParent-1)*sum(Nx(1:nParent)+1),sum(Nx(1:k-1)+1)+1) = Parent.bj(ncoup);
            if ~isempty(PipeTmp.twin_wvg) % If there is a twin pipe
                nTwin = PipeTmp.twin_wvg.norder;
                Wont(sum(Nx(1:k-1)+1)+1,sum(Nx(1:nTwin-1)+1)+1) = Hnc;
            end % if twin
            if length(PipeTmp.parent_wvg) > 1
                Parent = PipeTmp.parent_wvg(2);
                brg = PipeTmp.parent_point(2);
                nParent = Parent.norder;
                NN = Nx(k);
                Fj(end) = -PipeTmp.bj(NN)*(PipeTmp.Udj(NN)+Qp(k).V(NN))+Parent.bj(brg+1)*(Parent.Udj(brg+1)+Parent.V(brg+1))-Qp(k).Q(NN+1)+PipeTmp.Ns(Nx(k)+1);
                PipeTmp.Hj(NN+1) =  -(PipeTmp.bj(NN)+Parent.bj(brg+1)+2/T*(PipeTmp.Lj(NN)+Parent.Lj(brg+1))+PipeTmp.Rj(NN)+PipeTmp.Rcp(NN)+Parent.Rj(brg+1)+Parent.Rcm(brg+1));
                Hcp1 = -(Parent.bj(brg+1)+Parent.Rj(brg+1)+Parent.Rcm(brg+1)+2*Parent.Lj(brg+1)/T);
                Wont(brg+1+(nParent-1)*sum(Nx(1:nParent)+1),sum(Nx(1:k-1)+1)+NN+1) = Hcp1;
                Wont(sum(Nx(1:k-1)+1)+1+NN,brg+1+(nParent-1)*sum(Nx(1:nParent)+1)) = Hcp1;
                Wont(sum(Nx(1:k-1)+1)+1+NN,brg+2+(nParent-1)*sum(Nx(1:nParent)+1)) = Parent.bj(brg+1);
                Wont(brg+2+(nParent-1)*sum(Nx(1:nParent)+1),sum(Nx(1:k-1)+1)+NN+1) = Parent.bj(brg+1);
            end % if anabranch
    end % switch pipe
 
    if k == ich          
        Ft(end) = Fch;
        Wmat = H1_ch;
        W = [ W,zeros(size(W,1),size(Wmat,2));zeros(size(Wmat,1),size(W,2)),Wmat ];        
    else    
        Ft(1+sum(Nx(1:k-1)+1):sum(Nx(1:k-1)+1)+length(Fj)) = Fj(:);        
        Wmat = diag(obj.waveguide{k}.Hj);
        bd = diag(obj.waveguide{k}.bj(1:end-1));
        Wmat(1:end-1,2:end) = Wmat(1:end-1,2:end)+bd;
        Wmat(2:end,1:end-1) = Wmat(2:end,1:end-1)+bd;
        if k == isg
            Wmat(end-1,end) = 0;
        end
        W = [ W,zeros(size(W,1),size(Wmat,2));zeros(size(Wmat,1),size(W,2)),Wmat ];        
    end

end% for k = 1:nPipe    
esmfMtx = Wont+W;
forceVector = Ft;
obj.esmf_matrix = esmfMtx;
obj.force_vector = Ft;
end % end function
