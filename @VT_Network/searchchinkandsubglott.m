function [ isChink, isSubGlott, idxChink, idxSubGlott ] = searchchinkandsubglott(obj)
% [ isChink, isSubGlott, idxChink, idxSubGlott ] = searchchinkandsubglott(obj)
% check if a glottal chink and a subglottal system is connected to the
% network. Returns also their position in the VT_Network object
% 
% Input argument:
% - obj: network object
%
% Output arguments:
% - isChink: boolean that is true if a GlottalChink object is connected in
% the VT_Network object
% - isSubGlott: boolean that is true if a SubGlottTact object is connected in
% the VT_Network object
% - idxChink: position of the GlottalChink object in the waveguide
% property of the VT_Network object
% - idxSubGlott: position of the SubGlottTract object in the waveguide
% property of the VT_Network object
%
% Benjamin Elie, 2020
%
% See also VT_Network, VT_Waveguide, SubGlottalTract, GlottalChink, esmfsynthesis
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

nPipe = length(obj.waveguide);
isChink = false;
isSubGlott = false;
idxChink = [];
idxSubGlott = [];
% check for chink or subglottal pressure
for k = 1:nPipe
    wvg_tmp = obj.waveguide{k};
    if isa(wvg_tmp,'GlottalChink')
        isChink = true;
        idxChink = [ idxChink, k ];        
    end
    if isa(wvg_tmp,'SubGlottalTract')
        isSubGlott = true;
        idxSubGlott = [ idxSubGlott, k ];        
    end    
end
if isempty(idxChink); idxChink = 0; end
if isempty(idxSubGlott); idxSubGlott = 0; end
obj.whereChink = idxChink;
obj.whereSubGlott = idxSubGlott;
obj.isChink = isChink;
obj.isSubGlott = isSubGlott;
end