function [ Ut, Ug, Uch, po ] = esmfmtxsolver(obj, bool)
% [ Ut, Ug, Uch, po ] = esmfmtxsolver(obj, bool)
% solve the ESMF system and returns the acoustic flow in the network and
% the glottal flow
% 
% Input argument:
% - obj: VT_Network object
% - bool: boolean which is true when vocal folds are in contact
%
% Output arguments:
% - Ut: acoustic flow in the network
% - Ug: glottal flow in the oscillating part of the glottis
% - Uch: glottal flow in the chink
% - po: pressure at the subglottal connection. Applies if a subglottal
% tract is connected
%
% Benjamin Elie, 2020
%
% See also esmfsynthesis, esmfmatrix, VT_Network, VT_Waveguide, GlottalChink,
% SubGlottalTract
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
opts2 = struct('SYM',true);
Ft = obj.force_vector;
Wont = obj.esmf_matrix;
OralTract = obj.waveguide{1};
sgbool = obj.isSubGlott;
chbool = obj.isChink;
nspipe = chbool+sgbool;
T = 1/obj.simulation_frequency;
if chbool
    Hch = -(OralTract.bj(1)+OralTract.Rj(1)+2*OralTract.Lj(1)/T+OralTract.Rcm(1));
end
isg = obj.whereSubGlott; 
Uch = [];
po = [];
a_eq = OralTract.glottal_bernoulli;

% check for chink or subglottal pressure
for k = 1:length(obj.waveguide)
    wvg_tmp = obj.waveguide{k};
    if isa(wvg_tmp,'GlottalChink')
        Chink = wvg_tmp;
    end
    if isa(wvg_tmp,'SubGlottalTract')
        SubGlott = wvg_tmp;
        Hsg = SubGlott.connection_impedance;
    end
end

if bool
	Ug = 0; % if contact, airflow is null
    % To be used with a self-oscillating model of the vocal folds
    Ft2 = Ft(2:end);  
    Wont2 = Wont(2:end,2:end);   
    Ut = Wont2\Ft2;       
    if chbool; Uch = Ut(end); end
    if sgbool; po = Ut(end-chbool); end    
else    
    GSF = linsolve(Wont,Ft,opts2);  % rearrange the system to solve Ug
    F11 = GSF(1);     
    GSi = inv(Wont);
    a2 = -a_eq*GSi(1,1);
    if (a2~=0 && ~isinf(a2) && ~isnan(a2));  a = solve_pol_2(a2,1,-F11); else a = F11; end % Solve quadratic equation of Ug
    Ug = max(a,0); % If the solution is negative, keep 0.
    Ft2 = Ft(2:end); 
    if Ug ~= 0
        Ft2(1) = Ft2(1)-OralTract.bj(1)*Ug; % Update the linear system by removing the Ug coefficients ....
        if chbool; Ft2(end) = Ft2(end) - Hch*Ug;  end % at the chink
        if sgbool; Ft2(end-nspipe) = Ft2(end-nspipe)-SubGlott.bj(obj.numer_tubes(isg))*Ug; Ft2(end-chbool) = Ft2(end-chbool)-Hsg*Ug; end % at the subglottal connection
    end
    Wont2 = Wont(2:end,2:end); % remove the first line and the first column of the matrix of the linear coefficients
    Ut = Wont2\Ft2;  % solve remaining system;
    if chbool; Uch = Ut(end); end % get the flow through the chink
    if sgbool; po = Ut(end-chbool); end % get the pressure at the subglottal connection
end

OralTract.glottal_flow_inst = Ug;
if chbool; Chink.glottal_flow_inst = Uch; end
if sgbool; SubGlott.connection_pressure_inst = po; end
obj.flow_inst = Ut;

end

function x = solve_pol_2(a,b,c)
% x = solve_pol_2(a,b,c)
% solve second order polynomial and returns the max value of found roots. Returns an error if there is no real
% solution.

discr = b^2-4*a*c;

if discr>=0    
    x1 = (-b+sqrt(discr))/(2*a);
    x2 = (-b-sqrt(discr))/(2*a);    
else    
    error('No real solution for this equation');    
end
x = max(x1,x2);
end