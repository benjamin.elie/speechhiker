function esmftransferfunction(obj, Const, slices)
% esmftransferfunction(obj, Const, slices)
% returns the transfer function and the impulse response of the VT_Network
% object. Use the esmfsynthesis with a glottal area step function to
% compute the impulse response.
% THIS METHOD SHOULD BE BUGGY. DO NOT USE.
% 
% Input arguments :
% - obj: network object
% - Const: acoustic constant terms
% - slices: time frames in which we want the transfer function
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide, esmfsynthesis, VT_Network
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; Const = []; end
if nargin > 3; slices = 1:size(obj.area_function,2); end
if ~isfield(Const,'selfoscil'); Const.selfoscil = 0; end
if ~isfield(Const,'sampling_rate'); Const.sampling_rate = 6e4; end
if ~isfield(Const,'amin'); Const.amin = 1e-11; end

Ag = 0.4e-5;
tvec = 0:1/Const.samplig_rate:0.3;
tc = 0.01;

ago = zeros(size(tvec));
ago(1:floor(tc*Const_sampling_rate)) = Ag;
ago(ago <= Const.amin) = Const.amin;
obj.subglottal_control = 500*ones(size(tvec));

for k = slices
   
    af = area_function
    
end

end

