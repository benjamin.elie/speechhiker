function [ Pout, obj_out ] = esmfsynthesis(obj, Const)
% [ Pout, obj_out ] = esmfsynthesis(obj, Const)
% Running speech synthesis in the VT_Network object using ESMF (see original paper [1])
% 
% Input argument:
% - obj: VT_Network object
% - Const: acoustic constant terms and parameters
%
% Output arguments:
% - Pout: radiated acoustic pressure
% - obj_out: returns the synthesized speech as a SpeechAudio oject
%
% [1] Elie B., and Laprie Y. "Extension of the single-matrix formulation 
% of the vocal tract: consideration of bilateral channels and connection 
% of self-oscillating models of the vocal folds with a glottal chink". 
% Speech Comm. 82, pp. 85-96 (2016)
% 
% Benjamin Elie, 2020
%
% See also VT_Network, VT_Waveguide, MainOralTract, SubGlottalTract,
% GlottalChink, Oscillator, Glottis, Tongue, SpeechAudio
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Default values for the tunable parameters
if nargin < 2; Const = []; end
if ~isfield(Const,'kctc'); Const.kctc = 4; end
if ~isfield(Const,'rctc'); Const.rctc = 2.2; end
if ~isfield(Const,'kctctt'); Const.kctctt = 1.5; end
if ~isfield(Const,'rctctt'); Const.rctctt = 1.2; end
if ~isfield(Const,'c'); Const.c = 352.3313; end
if ~isfield(Const,'mu'); Const.mu = 1.8500e-05; end
if ~isfield(Const,'rho'); Const.rho = 1.1255; end
if ~isfield(Const,'blim'); Const.blim = 1e-5; end
if ~isfield(Const,'filt'); Const.filt = 'fir1'; end
if ~isfield(Const,'sep'); Const.sep = 1.2; end
if ~isfield(Const,'Rec'); Const.Rec = 1.7e3; end
if ~isfield(Const,'nmass'); Const.nmass = 2; end
if ~isfield(Const,'dist_source'); Const.dist_source = 2; end
if ~isfield(Const,'fh'); Const.fh = 200; end
if ~isfield(Const,'fh'); Const.fl = 4e3; end
if ~isfield(Const,'ordh'); Const.ordh = 1; end
if ~isfield(Const,'ordl'); Const.ordl = 7; end
if ~isfield(Const,'bf'); Const.bf = [0.9000 0.1000 -0.8000]; end
if ~isfield(Const,'af'); Const.af = 1; end
if ~isfield(Const,'K_b'); Const.K_b = 1.5982; end
if ~isfield(Const,'G'); Const.G = 9/128; end
if ~isfield(Const,'S'); Const.S = 3/8; end
if ~isfield(Const,'dispcomp'); Const.dispcomp = treu; end
if ~isfield(Const,'wallyield'); Const.wallyield = treu; end
if ~isfield(Const,'dynterm'); Const.dynterm = true; end
if ~isfield(Const,'beta'); Const.beta = 5e-8; end
if ~isfield(Const,'Qr'); Const.Qr = 0.2; end
if ~isfield(Const,'Qrc'); Const.Qrc = 0.05; end
if ~isfield(Const,'wr'); Const.wr = 8000; end
if ~isfield(Const,'wm'); Const.wm = 21; end
if ~isfield(Const,'wc'); Const.wc = 8450000; end
if ~isfield(Const,'selfoscil'); Const.selfoscil = true; end
if ~isfield(Const,'amin'); Const.amin = 1e-11; end
if ~isfield(Const,'model'); Const.model = 'ishi'; end
if ~isfield(Const,'osc_tg'); Const.osc_tg = false; end
if (~isfield(Const,'model_tt') && Const.osc_tg); Const.model_tt = 'smooth'; end
if ~isfield(Const,'unst'); Const.unst = 1; end
if ~isfield(Const,'rmlt'); Const.rmlt = 4; end

% %%%% Warning messages
% if (~Const.selfoscil) && (~isfield(inparam,'ag') || isempty(inparam(1).ag) || length(inparam(1).ag) < length(tvec))
%     warning on; warning('Glottal opneing parameters not set properly for parametric glottis based simulation. Use self-oscillations instead'); warning off;
%     Const.selfoscil = 1;
% end
% 
% if (~strcmp(Const.model,'smooth') && ~strcmp(Const.model,'ishi'))
%     warning on;warning('Incorrect definition of the glottis model. Set to smooth contours by default'); warning off;
%     Const.selfmod = 'smooth';
% end
% 
% if (Const.osc_tg && length(vfparam) < 2)
%     warning on; warning('Tongue oscillation is set to 1 but parameters not defined. Tongue oscilations wont be taken into account.'); warning off;
%     Const.osc_tg = 0;
% end

% Initialization of the network
[chbool, sgbool, ich, isg ] = searchchinkandsubglott(obj);
[ isGlott, isSupOscill, idxGlott, idxSupOscill ] = searchoscillators(obj);
if isSupOscill; nSupOscill = length(idxSupOscill); end

%%%%% Special objects
OralTract = obj.waveguide{1};
numIter = size(OralTract.area_function,2);

if isGlott
    objGlottis = obj.oscillators{idxGlott};
else
    if  isempty( obj.input_area ) || length(obj.input_area) < numIter
        error('Glottal opening parameters not set properly for parametric glottis based simulation. Use self-oscillations instead'); 
    else
        glottalArea = obj.input_area;
        ag0 = obj.abduction;
        objGlottis = Glottis;
        objGlottis.partial_abduction = ag0/objGlottis.max_opening;
        objGlottis.length = 0.022;
    end
end

if sgbool
    SubGlottTract = obj.waveguide{isg};
end
if chbool
    if isGlott
        Chink = obj.waveguide{ich}; 
    else
        obj.waveguide{ich} = [];
        isGlott = 0; 
        chbool = 0;
        aChink = 0;
    end
else
    aChink = 0;  
    if isGlott
        objGlottis.partial_abduction = zeros(numIter,1);
    end
end

Nx = computenumtubes(obj);
nPipe = length(Nx);
initnetwork(obj);
Uk1 = 0;

% %%%%%%%%%%%%% Check potential unrealistic pitch values and correct them
% objGlottis.fundamental_frequency(objGlottis.fundamental_frequency<=60) = 60;

armin = Const.amin;
co_mu = Const.mu;

%%%%%%%%%%% Definition of all the constants used during resolution.
sr = obj.simulation_frequency;
T = 1/sr;
Const.T = T;

kwb = 0;
if isempty(OralTract.actualVT)
    OralTract.actualVT = 1:size(OralTract.area_function, 1);
end

[ac,ic] = min(OralTract.area_function(OralTract.actualVT,:));
inputArea = OralTract.area_function(1,:);
% if length(inparam(1).Psub) ~= numIter; inparam(1).Psub = mean(inparam(1).Psub)*ones(size(tvec)); end
% if length(inparam(1).fo) ~= numIter; inparam(1).fo = 100*ones(size(tvec)); end

%%%%%%%%%% Initialization of glottal parameters
if isGlott  
    objGlottis = obj.oscillators{isGlott};
    objGlottis.mass_position = objGlottis.rest_position;
    objGlottis.mass_position_nm1 = objGlottis.rest_position;
    objGlottis.mass_position_nm2 = objGlottis.rest_position;
    if sgbool
        pstmp = 0; 
        objGlottis.up_height = powtr(SubGlottTract.area_function(end,:),1.8, 1.2, 0 );
    else
        asg = 3.1314e-05;
        h0 = powtr( asg, 1.8, 1.2, 0 );
        objGlottis.up_height = h0*ones(numIter,1); 
        clear asg h0
    end
    objGlottis.down_height = powtr(inputArea,1.8, 1.2, 0 );
    if isempty(objGlottis.low_frequency_abduction)
        objGlottis.low_frequency_abduction = zeros(numIter,1);
    end  
end

if ~isfield(Const,'sro'); Const.sro = sr; end
Fric_Amp = Const.namp;
Reyn = 0;
fny = sr/2; % Nyquist frequency

if isSupOscill
    for ksup = 1:nSupOscill
        objSup = obj.oscillators{idxSupOscill(ksup)};
        objwvg = objSup.waveguide;
        objSup.mass_position = objSup.rest_position;
        objSup.mass_position_nm1 = objSup.rest_position;
        objSup.mass_position_nm2 = objSup.rest_position;
        objSup.down_height = powtr(objwvg.area_function(objSup.position-1,:),1.8, 1.2, 0 );
        objSup.up_height = powtr(objwvg.area_function(objSup.position+1,:),1.8, 1.2, 0 );
    end
end

if any(ic>=38)
    if strcmp(Const.filt,'fir1')
        Const.hld = fir1(Const.ordl*2,1000/fny,'low'); 
        Const.hld2 = fir1(1,1000/fny,'high'); 
    end
    if strcmp(Const.filt,'firls')
        Const.hld = firls(Const.ordl+Const.ordh,[0 6000/fny 10000/fny 1],[0.9 1 1 0]);
    end 
end
%%%%%%%%%%%%%%%% Resolution of the system at every
warning off
if ~isempty(objGlottis.partial_abduction)
    lchk = objGlottis.partial_abduction.^2;
else
    lchk = zeros(numIter,1);
end
lchk(lchk<=armin) = armin;
lchk(lchk>=1-armin) = 1-armin;
lgvec = objGlottis.length.*(1-lchk);
objGlottis.output_length = lgvec;

if ~isGlott
    lc = zeros(size(ac));
    for k = 1:length(ic)
        lc(k) = OralTract.length_function(ic(k),k);
    end
    Ag2 = 1./(glottalArea.^2);
    Ac2 = 1./(ac.^2);
    rv_sum = (12*lgvec(:)*objGlottis.width.*Ag2(:).*sqrt(Ag2(:))+8*pi*lc(:).*Ac2(:))*co_mu;
    rk_sum = 1.38*Const.rho*(Ac2(:)+Ag2(:));
    udc = (-rv_sum + sqrt(rv_sum.*rv_sum + 4*rk_sum.*obj.subglottal_control(:)))./(2*rk_sum); 
    udc(isnan(udc)) = 0;  
    rgvec = 12*co_mu*lgvec.^2*objGlottis.width./(glottalArea.^3);
end

for k = 1:numIter
    Const.k_iter = k;
    objGlottis.length = lgvec(k);
    ain = inputArea(k);
    lc =  OralTract.length_function(ic(k),k);
    if (~sgbool) || (k == 1)
        pstmp = obj.subglottal_control(k);
    end
    if chbool
        aChink = Chink.area_function(1,k); 
    end
    objGlottis.upstream_pressure = pstmp;
    if isSupOscill
        for ksup = 1:nSupOscill
            objSup = obj.oscillators{idxSupOscill(ksup)};
            objwvg = objSup.waveguide;
            objSup.length = powtr( objSup.low_frequency_abduction(k), 1.8, 1.2, 0 );
            dlt = powtr( objSup.length, 1.8, 0.2, 1 )*1e2;
            ain_tongue = objSup.down_height*dlt;
            ldtt = objwvg.length_function(objSup.position,k);
            if strcmp(objSup.model,'ishi')
                Qmass = objSup.initial_mass(1,2)/objSup.initial_mass(1,1);
                x1tt = ldtt/(1+Qmass);  % 1st mass
                x2tt = ldtt-1e-11;   % 2nd mass
                x3tt = ldtt;
            else
                x1tt = 2e-4;
                x2tt = ldtt-2*x1tt;
                x3tt = ldtt;
            end
            objSup.x_position = [ 0 x1tt x2tt x3tt];
            updateimpedance( objSup, 0, ain_tongue, Const );   
            hsii = objSup.sepration_height;
            objwvg.area_function(objSup.position,k) = max(1e-8,powtr(hsii,1.8,1.2,1));    
        end
%         %%%%%% Thing to be fixed
%         if ~isempty(OralTract.anabranch_wvg)
%             lat = OralTract.anabranch_wvg(1);
%             cp = OralTract.child_wvg;
%             
%             ii = find(cp(1,:) == lat);
%             af_main(cp(2,ii(1)):cp(2,ii(2))) = af_main(cp(2,ii(1)):cp(2,ii(2))) + af(lat).A(:,k);
%         end
    end
    
    if isGlott
        updateimpedance( objGlottis, Const, aChink, ain, ac(k), lc );     
        Lg = objGlottis.inductance;
        Udc = objGlottis.DC_flow;
        Aglot = objGlottis.area;
    else
        objGlottis.bernoulli = 0;
        objGlottis.inductance = 0;
        Lg = 0;
        objGlottis.resistance = rgvec(k);
        Udc = udc(k);
        objGlottis.DC_flow = Udc;        
        Aglot = glottalArea(k);        
    end   

    kpc = 100*((k-1)/(numIter-1)) ;
    if (kpc >= 10*kwb && Const.dispcomp );disp(['Completion: ',num2str(10*kwb),' %']); kwb = kwb+1; end   
    
    for kpipe = 1:nPipe % Electric components of each pipe  
         wvg_tmp = obj.waveguide{kpipe};  

        if k == 1
            uim1 = zeros(Nx(kpipe),1);
        else
            if kpipe > 1                
                wvg_parent = wvg_tmp.parent_wvg(1);
                Aglot = wvg_parent.area_function( wvg_tmp.parent_point(1),k);
            end
            uim1 = wvg_tmp.flow_time_matrix(:,k-1);
        end
        acou2elec(wvg_tmp, Const, uim1, Aglot); 

        namp = Fric_Amp; 
        if kpipe == 1              
            if (isGlott && chbool && namp > 0) || (~isGlott)
                Reyn = computereynolds(wvg_tmp, Udc, Const);
            end
            %%% Check if there is a supraglottal oscillator
            if isSupOscill
                for ksup = 1:nSupOscill
                    objSup = obj.oscillators{idxSupOscill(ksup)};
                    ptt = objSup.position;
                    wvg_tmp.Lj(ptt) = 0;
                    wvg_tmp.Hj(ptt:ptt+1) = -2*(wvg_tmp.Lj(ptt-1:ptt)+wvg_tmp.Lj(ptt:ptt+1))/T ...
                    -wvg_tmp.Rj(ptt-1:ptt)-wvg_tmp.Rj(ptt:ptt+1)- ...
                    wvg_tmp.bj(ptt-1:ptt)-wvg_tmp.bj(ptt:ptt+1)- ...
                    wvg_tmp.Rcm(ptt-1:ptt)-wvg_tmp.Rcp(ptt:ptt+1);
                end
            end
%             if (~Const.unst && Const.osc_tg)
%                 wvg_tmp.Lj(ptt) = 0;
%                 wvg_tmp.Hj(ptt:ptt+1) = -2*(wvg_tmp.Lj(ptt-1:ptt)+wvg_tmp.Lj(ptt:ptt+1))/T ...
%                     -wvg_tmp.Rj(ptt-1:ptt)-wvg_tmp.Rj(ptt:ptt+1)- ...
%                     wvg_tmp.bj(ptt-1:ptt)-wvg_tmp.bj(ptt:ptt+1)- ...
%                     wvg_tmp.Rcm(ptt-1:ptt)-wvg_tmp.Rcp(ptt:ptt+1);
%             end     
        else 
            namp = 0;
        end        
        
        if (Reyn < Const.Rec || isa(wvg_tmp,'GlottalChink')); namp = 0; end        
        
        if isa(wvg_tmp,'GlottalChink')
            wvg_tmp.Lj = wvg_tmp.Lj*2; 
            wvg_tmp.Rj = 2*wvg_tmp.Rj/(Const.rmlt/4); 
        end
        if (isGlott && chbool && namp > 0) || (~isGlott)
            computenoise( wvg_tmp, Const, namp, Udc, Aglot );   
        end
    end
    if isGlott || isSupOscill
        for k_oscill = 1:length(obj.oscillators)
            updateparam( obj.oscillators{k_oscill}, Const );
        end
    else
        objGlottis.isContact = (glottalArea(k) <= armin);     
%         if isSupOscill    
%             for k_oscill = 1:length(obj.oscillators)
%                 updateparam( obj.oscillators{k_oscill}, Const );
%             end
%         end
    end    
    OralTract.subglottal_pressure = pstmp;
    OralTract.glottal_resistance = sum(objGlottis.resistance)+ ...
        OralTract.Rj(1)+2*(sum(Lg)+OralTract.Lj(1))/T+OralTract.Rcm(1);
    OralTract.glottal_inductance = sum(objGlottis.inductance);   
    OralTract.glottal_bernoulli = sum(objGlottis.bernoulli);

    esmfmatrix( obj );
    try 
        esmfmtxsolver( obj, objGlottis.isContact );
    catch
        warning('Something went wrong. Simulation has ended before total completion');
        break
    end
    updateacoustics( obj, Const );     
  	updatesup( OralTract, Const );
    if sgbool
        pstmp = SubGlottTract.pressure_time(end-1);
    end
    
    %%%%% Radiation
    sumarray = 0;
    
    for krad = 1:length(obj.radiation_position)
        sumarray = sumarray+(obj.waveguide{obj.radiation_position(krad)}.flow_time(end));
    end
    Um = sum(sumarray);
    obj.pressure_radiated(k) = Um-Uk1;
    Uk1 = Um;
    
    if isGlott
        objGlottis.inst_flow = OralTract.glottal_flow_inst;
        objGlottis.supraglottal_pressure = OralTract.supraglottal_pressure;
        if strcmp(objGlottis.model,'smooth')
            appliedforce( objGlottis, Const );
            updatemassposition( objGlottis, Const );         
        else
            updatemassishiflan( objGlottis, Const );   
        end
        
    end    
    if isSupOscill
        for ksup = 1:length(idxSupOscill)
            objSup = obj.oscillators{idxSupOscill(ksup)};
            objwvg = objSup.waveguide;
            updatepressure(objSup, objwvg, Const);
            
            if strcmp(objSup.model,'smooth')
                appliedforce( objSup, Const );
                updatemassposition( objSup, Const );         
            else
                updatemassishiflan( objSup, Const );   
            end
            if objSup.num_oscillators == 1
                objSup.mass_position(1,:) = 0; 
                objSup.mass_position_nm1(1,:) = 0; 
                objSup.mass_position_nm2(1,:) = 0;
            end
            
        end
    end   
    
%     objGlottis.DC_vector(:,k) = objGlottis.DC_flow;
    writeoutputmatrix(obj,k);
    
end

Pout  = obj.pressure_radiated;
obj_out = SpeechAudio('signal', Pout, ...
    'sampling_frequency', sr);
end

function y = powtr( x, a, b, dr )
% y = powtr( x, a, b, dr )
% converts area function to midsagittal distance, or the contrary according
% ot the power transformation

    if nargin < 4; dr = 1; end

    if dr == 1 % midsagittal to area function
        x = 100*x; % x should be in cm
        y = a.*x.^b;
        y = y*1e-4; % goes back to m^2
    else % area to midsagittal distance
        x = 1e4*x; % x should be in cm^2
        y = exp((log(x)-log(a))/b);
        y = y*1e-2; % goes back to m
    end

end

function initnetwork(obj)
%%%%% Initialisation of the derivative and integration terms
% Nx is the number of tubelets of the considered pipe

    nPipe = length(obj.waveguide);
    posrad = nan(nPipe,1);
    nRad = 0;

    for k = 1:nPipe
        wvg_tmp = obj.waveguide{k};
        wvg_tmp.initderiv;    
        wvg_tmp.initacoustics;   
        if wvg_tmp.radiation
            nRad = nRad+1;
            posrad(nRad) = k;
        end
    end
    posrad(isnan(posrad)) = [];
    obj.radiation_position = posrad;
end


function nTubes = computenumtubes(obj)
% nTubes = computenumtubes(obj)
% returns the number of tubes of each waveguide of the VT_Network object

    nPipe = length(obj.waveguide);
    nTubes = zeros(nPipe,1);
    for k = 1:nPipe
        nTubes(k) = size(obj.waveguide{k}.area_function,1);
        obj.waveguide{k}.norder = k;
    end
    obj.number_tubes = nTubes;
end