function fig_handle = freqplot(obj, dyn, norm)
% fig_handle = plot(obj, dyn, norm)
% plot the frequency objects
%
% Input arguments:
% - obj: FreqSig object
% - dyn: dynamic range for z-axis, in dB (default is 100 dB)
% - norm: if true, the max of the time-frequency representation is 0 dB
%
% Output argument:
% - fig_handle: figure object
%
% Benjamin Elie, 2020
%
% See also SpeechAudio, FreqSig
%
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin < 2; dyn = 100; end
if nargin < 3; norm = false; end

set(0,'defaulttextinterpreter','latex')
set(0,'defaultAxesFontSize',20)

Pxx = abs(obj.values).^2;
if norm
    Pxx = Pxx/max(Pxx(:));
end
max_Pxx = max(abs(Pxx(:)));

fig_handle = figure;
plot(obj.frequency_vector, db(Pxx), 'linewidth',2)
set(gca,'ticklabelinterpreter','latex')
ylabel('Spectrum (dB)')
xlabel('Frequency (Hz)')
ylim([ db(max_Pxx)-dyn db(max_Pxx) ])
xlim([0 max(obj.frequency_vector) ])
set(gcf, 'units', 'normalized', 'position', [ 0 0 1 1 ])

end
