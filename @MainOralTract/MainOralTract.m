classdef MainOralTract < VT_Waveguide
% MainOralTract: class herited from VT_Waveguide. Describes the oral tract
% from the glottis to the lips
% 
% Specific properties of MainOralTract objects are:
% - subglottal_pressure_vector: vector containg the pressure values
% upstream the glottis
% - supraglottal_pressure_vector: vector containing the pressure values
% downstream the glottis
% - reynolds_vector: vector containing the reynolds number at each time
% step
% - glottal_flow_vector: vector containing the glottal flow at each time
% step
% - subglottal_pressure: instantaneous pressure upstream the glottis
% - supraglottal_pressure: instantaneous pressure downstream the glottis
% - glottal_bernouilli: instantaneous Bernoulli resistance
% - glottal_resistance: instantaneous glottal resistance (viscous term)
% - glottal_inductance: instantaneous glottal inductante (unsteasy term)     
% - glottal_flow_inst: instantaenous glottal flow through the oscillating
% part of the glottis
% - chink_flow_inst: instantaenous glottal flow through the chink
% - reynolds_inst: instantaneous reynolds number       
% - Qug: instantaneous derivative term

% The radiation is true (radiation at the lips)
%
% Specific methods of MainOralTract objects are
% - computereynolds: compute the Reynolds number
% - updatesup: compute the supraglottal pressure at the current time step
%
% Benjamin Elie, 2020
%
% See also VT_Waveguide, VT_Network, GlottalChink, SubGlottalTract
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    
    properties        
        %%%% Output acoustic matrix
        subglottal_pressure_vector = [];
        supraglottal_pressure_vector = [];
        reynolds_vector = [];    
        glottal_flow_vector = [];   
        velopharyngeal_port = [];
        velum_area = [];
    end
    
    properties (Hidden = true)
        %%%% Instantaneous acoustics
        subglottal_pressure = [];
        supraglottal_pressure = [];
        glottal_bernoulli = [];
        glottal_resistance = [];
        glottal_inductance = [];        
        glottal_flow_inst = [];
        chink_flow_inst = 0;
        reynolds_inst = [];        
        Qug = 0;
    end
    
    methods
        % Constructor method
        function obj = MainOralTract(varargin)
            for k = 1:2:length(varargin)
                field = varargin{k};
                argvalue = varargin{k+1};
                if isprop(obj,field)
                    obj.(field) = argvalue;
                end % if isprop
            end % for k ...
            obj.radiation = true;
            obj.parent_wvg = [];
            obj.parent_point = [];
        end % end function
        
        % Method to compute the Reynolds number in the main oral tract
        Reyn = computereynolds( obj, Udc, Const )
        
        psup = updatesup(obj, Const)
    end
end

