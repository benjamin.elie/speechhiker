function Reyn = computereynolds( obj, Udc, Const )
% Reyn = computereynolds( obj, Udc, Const )
% Compute the Reynolds number inside the constriction of the waveguide
% defined by a, l.
% 
% Input arguments:
% - obj: MainOralTract object
% - Udc : volume velocity
% - Const: some physical constants and parameters
%
% Output argument:
% - Reyn: Reynolds number
% 
% Benjamin Elie, 2020
%
% See also VT_Waveguide, MainOralTract, esmfsynthesis, VT_Network
    
% Copyright (c) 2020, Benjamin Elie
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of  nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

co_mu = Const.mu;
rho = Const.rho;

if isempty(obj.actualVT)
    obj.actualVT = 1:size(obj.area_function,1);
end
if isfield(Const,'k_iter')
    a = obj.area_function(:,Const.k_iter);  
    Ack = min(a(obj.actualVT));
else
    Const.k_iter = 1:size(obj.area_function,2);
    a = obj.area_function;
    Ack = min(a(obj.actualVT,:));
end

% find the constriction geometry
Reyn = 2*rho/co_mu*Udc./sqrt(pi*Ack);
obj.reynolds_inst = Reyn;
obj.reynolds_vector(Const.k_iter) = Reyn;

end